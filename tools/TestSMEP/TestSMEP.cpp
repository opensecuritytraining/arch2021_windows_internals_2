#include <windows.h>
#include <stdio.h>
#include <ktmw32.h>

#pragma comment(lib,"KtmW32.lib")

#define BUFFER_LEN 200

int main(int argc, char* argv[])
{
	printf("main()\n");

	// Make the memory executable
	char* lpBuffer = (char*)VirtualAlloc(NULL, BUFFER_LEN, MEM_RESERVE | MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	printf("Allocated buffer at 0x%p\n", lpBuffer);
	memset(lpBuffer, 0x90, BUFFER_LEN); // Set NOPs
	lpBuffer[5] = (char)0xCC;	// int3 (breakpoint)

	// Make sure no page fault ever occurs due to not backed by physical memory
	VirtualLock(lpBuffer, BUFFER_LEN);

	
	printf("Hit a key to call NtCreateTransaction()...\n");
	getchar();

	// Execute syscall to trap to kernel mode
	CreateTransaction(NULL, 0, 0, 0, 0, 0, NULL);

	printf("Done here!\n");
}