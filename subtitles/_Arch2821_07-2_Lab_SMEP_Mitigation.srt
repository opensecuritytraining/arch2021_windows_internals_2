1
00:00:00,080 --> 00:00:04,560
The goal of this lab is to test

2
00:00:02,639 --> 00:00:06,480
different mitigations on the Windows

3
00:00:04,560 --> 00:00:10,000
kernel. More specifically, we're going to

4
00:00:06,480 --> 00:00:13,120
test that SMEP works as expected, and

5
00:00:10,000 --> 00:00:15,200
that when we disable it, we can bypass it.

6
00:00:13,120 --> 00:00:16,480
The code that you're going to use is in

7
00:00:15,200 --> 00:00:18,560
the

8
00:00:16,480 --> 00:00:19,520
tools\TestSMEP folder. The first thing you're

9
00:00:18,560 --> 00:00:22,000
going to do

10
00:00:19,520 --> 00:00:24,560
is to make sure SMEP is enabled on your

11
00:00:22,000 --> 00:00:26,160
target Windows 10 machine. For that,

12
00:00:24,560 --> 00:00:28,000
you're going to run the TestSMEP.exe

13
00:00:26,160 --> 00:00:30,160
executable and

14
00:00:28,000 --> 00:00:33,440
modify the control flow, once you're in

15
00:00:30,160 --> 00:00:36,239
the kernel mode to try to execute some

16
00:00:33,440 --> 00:00:37,360
memory from userland and confirm it

17
00:00:36,239 --> 00:00:40,719
triggers

18
00:00:37,360 --> 00:00:43,920
the ATTEMPTED_EXECUTE_OF_NOEXECUTE_MEMORY

19
00:00:40,719 --> 00:00:46,320
exception. Next, you're going to rerun

20
00:00:43,920 --> 00:00:48,160
the same executable, but this time once

21
00:00:46,320 --> 00:00:51,840
you are in kernel mode, you're going to

22
00:00:48,160 --> 00:00:54,160
modify the value of the CR4 register in

23
00:00:51,840 --> 00:00:56,559
order to disable SMEP, and then you're

24
00:00:54,160 --> 00:00:59,199
going to confirm that you can execute from

25
00:00:56,559 --> 00:01:01,039
a userland page directly

26
00:00:59,199 --> 00:01:03,280
in kernel mode. Okay,

27
00:01:01,039 --> 00:01:05,360
it's your turn now. So, the first thing

28
00:01:03,280 --> 00:01:08,640
you're going to have to do is to make sure

29
00:01:05,360 --> 00:01:11,119
TestSMEP.exe works on your target VM. As you

30
00:01:08,640 --> 00:01:14,400
can see, it actually

31
00:01:11,119 --> 00:01:16,479
shows a userland buffer allocated and

32
00:01:14,400 --> 00:01:19,200
then it tries to call and the

33
00:01:16,479 --> 00:01:20,880
NtCreateTransaction syscall.

34
00:01:19,200 --> 00:01:22,479
And if you run it several times it's

35
00:01:20,880 --> 00:01:24,960
going to allocate a different

36
00:01:22,479 --> 00:01:26,560
memory address in userland each time.

37
00:01:24,960 --> 00:01:28,479
If you look at the actual code for this

38
00:01:26,560 --> 00:01:31,040
TestSMEP code,

39
00:01:28,479 --> 00:01:32,400
you can see it's allocating some memory

40
00:01:31,040 --> 00:01:34,240
on the heap

41
00:01:32,400 --> 00:01:36,479
that is actually

42
00:01:34,240 --> 00:01:39,439
read/write/executable. It's actually

43
00:01:36,479 --> 00:01:41,680
showing the address of this heap buffer

44
00:01:39,439 --> 00:01:44,159
in userland. And then, we are filling

45
00:01:41,680 --> 00:01:47,040
the actual buffer with

46
00:01:44,159 --> 00:01:48,720
0x90, which is basically the

47
00:01:47,040 --> 00:01:51,600
NOP instruction.

48
00:01:48,720 --> 00:01:54,000
And at index 5 we are actually inserting

49
00:01:51,600 --> 00:01:55,280
a 0xCC value which is a

50
00:01:54,000 --> 00:01:57,119
breakpoint.

51
00:01:55,280 --> 00:02:00,079
Then, there is a call to VirtualLock

52
00:01:57,119 --> 00:02:02,240
to make sure the lpBuffer is always mapped

53
00:02:00,079 --> 00:02:05,040
into memory, that is to say it's always

54
00:02:02,240 --> 00:02:06,640
backed by physical memory. And then

55
00:02:05,040 --> 00:02:07,759
there is a getchar()

56
00:02:06,640 --> 00:02:10,319
call.

57
00:02:07,759 --> 00:02:11,840
The whole idea is to actually wait for

58
00:02:10,319 --> 00:02:14,400
you to hit a key

59
00:02:11,840 --> 00:02:16,879
before calling the syscall.

60
00:02:14,400 --> 00:02:19,680
The syscall here is not

61
00:02:16,879 --> 00:02:21,520
important, it could be any syscall. The only

62
00:02:19,680 --> 00:02:24,239
reason we use CreateTransaction is

63
00:02:21,520 --> 00:02:26,879
because it's a syscall that is rarely used

64
00:02:24,239 --> 00:02:29,440
on a system, so you will actually be able

65
00:02:26,879 --> 00:02:32,239
to set a breakpoint on NtCreateTransaction

66
00:02:29,440 --> 00:02:35,440
in WinDbg

67
00:02:32,239 --> 00:02:38,480
and be sure, when it hits, it comes from

68
00:02:35,440 --> 00:02:40,720
your TestSMEP.exe binary.

69
00:02:38,480 --> 00:02:43,599
This is the exception that should be

70
00:02:40,720 --> 00:02:46,160
triggered when you try to execute

71
00:02:43,599 --> 00:02:49,959
from a heap address in userland, when

72
00:02:46,160 --> 00:02:49,959
you're in kernel mode.

73
00:02:50,480 --> 00:02:56,800
This article will give you information

74
00:02:53,120 --> 00:02:59,760
on where the SMEP feature is stored, more

75
00:02:56,800 --> 00:03:03,360
specifically, into the CR4 register at

76
00:02:59,760 --> 00:03:03,360
bit index 20.

77
00:03:03,760 --> 00:03:09,200
You can also have a look at this blog

78
00:03:05,840 --> 00:03:13,040
that details SMEP features,

79
00:03:09,200 --> 00:03:13,040
and how to bypass it.

