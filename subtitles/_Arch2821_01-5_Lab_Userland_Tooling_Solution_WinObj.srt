1
00:00:00,080 --> 00:00:05,200
Here, we're going to use WinObj to find

2
00:00:02,159 --> 00:00:09,120
the object types that exist in kernel.

3
00:00:05,200 --> 00:00:09,120
So, we need to start it as Administrator.

4
00:00:10,960 --> 00:00:14,799
Then, we can go into ObjectTypes.

5
00:00:15,599 --> 00:00:21,039
So, if we sort them by Name, we'll see

6
00:00:18,080 --> 00:00:24,160
lots of them, now let's go on Vergilius

7
00:00:21,039 --> 00:00:27,800
and try to guess which structure they

8
00:00:24,160 --> 00:00:27,800
are defined in the kernel.

9
00:00:38,559 --> 00:00:43,960
So, for instance, if we look at the Device

10
00:00:40,960 --> 00:00:43,960
one,

11
00:00:46,320 --> 00:00:49,760
we see the _DEVICE_OBJECT.

12
00:00:50,239 --> 00:00:57,879
The _DEVICE_OBJECT

13
00:00:53,280 --> 00:00:57,879
is actually of the type Device.

14
00:00:58,239 --> 00:01:02,399
We can look for the Adapter one,

15
00:01:02,800 --> 00:01:08,960
we can see there is an _ADAPTER_OBJECT,

16
00:01:06,799 --> 00:01:11,040
however it's not defined

17
00:01:08,960 --> 00:01:14,560
because there are no symbols.

18
00:01:11,040 --> 00:01:14,560
If we check in WinDbg

19
00:01:15,439 --> 00:01:18,880
we see there is no such

20
00:01:17,680 --> 00:01:23,439
object,

21
00:01:18,880 --> 00:01:23,439
however, the _DEVICE_OBJECT is defined.

22
00:01:23,680 --> 00:01:30,479
It means the symbols for the actual

23
00:01:26,240 --> 00:01:30,479
_ADAPTER_OBJECT are not public.

24
00:01:31,520 --> 00:01:35,520
Another one we can look for is the

25
00:01:33,360 --> 00:01:39,040
Mutant,

26
00:01:35,520 --> 00:01:39,040
you can see a KMUTANT,

27
00:01:39,840 --> 00:01:43,600
which is of the Mutant type.

28
00:01:43,680 --> 00:01:48,479
You can see several Tm-

29
00:01:46,000 --> 00:01:48,479
something.

30
00:01:48,720 --> 00:01:53,119


31
00:01:50,960 --> 00:01:55,920
If you look for Tm,

32
00:01:53,119 --> 00:01:57,520
you will find _KTM

33
00:01:55,920 --> 00:01:59,840
which stands for Kernel Transaction

34
00:01:57,520 --> 00:02:02,000
Manager.

35
00:01:59,840 --> 00:02:06,479
If you actually want to find all the

36
00:02:02,000 --> 00:02:08,560
objects, you need to look for Transaction.

37
00:02:06,479 --> 00:02:09,679
So, for example

38
00:02:08,560 --> 00:02:10,879
Tm

39
00:02:09,679 --> 00:02:12,560
Tx

40
00:02:10,879 --> 00:02:15,800
is the Transaction,

41
00:02:12,560 --> 00:02:15,800
_KTRANSACTION.

42
00:02:22,879 --> 00:02:27,200
If you can't find the actual structure,

43
00:02:25,280 --> 00:02:29,680
you can use Google.

44
00:02:27,200 --> 00:02:32,000
For example, TmRm,

45
00:02:29,680 --> 00:02:34,720
we don't know what it is.

46
00:02:32,000 --> 00:02:37,840
So, if we go on Google: "TmRm

47
00:02:34,720 --> 00:02:37,840
Windows kernel",

48
00:02:38,000 --> 00:02:43,120
we find it's actually

49
00:02:40,080 --> 00:02:43,120
Resource Manager.

50
00:02:46,319 --> 00:02:50,560
So, it's probably the KRESOURCEMANAGER

51
00:02:48,640 --> 00:02:52,319
structure.

52
00:02:50,560 --> 00:02:54,480
As an exercise,

53
00:02:52,319 --> 00:02:56,640
you can try to find

54
00:02:54,480 --> 00:02:57,920
the structures for the Desktop, the

55
00:02:56,640 --> 00:03:01,760
Directory,

56
00:02:57,920 --> 00:03:01,760
the Driver or the Controller.

