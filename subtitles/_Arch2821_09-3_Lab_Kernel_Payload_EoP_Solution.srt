1
00:00:00,179 --> 00:00:07,680
Okay. So I have my debugger VM attached

2
00:00:04,799 --> 00:00:08,519
to my actual target VM in order to debug

3
00:00:07,680 --> 00:00:11,219
it.

4
00:00:08,519 --> 00:00:13,139
I have started a command prompt as

5
00:00:11,219 --> 00:00:17,460
a regular user. We can see this command

6
00:00:13,139 --> 00:00:19,680
prompt is running with the IEUser user.

7
00:00:17,460 --> 00:00:22,039
And I can confirm it with the "whoami"

8
00:00:19,680 --> 00:00:22,039
command.

9
00:00:22,080 --> 00:00:25,680
So now, what I can do is I can break in

10
00:00:24,300 --> 00:00:28,859
the debugger,

11
00:00:25,680 --> 00:00:32,779
and look for the actual information for

12
00:00:28,859 --> 00:00:32,779
the cmd.exe process.

13
00:00:34,020 --> 00:00:38,059
And so we can see it actually returned

14
00:00:36,360 --> 00:00:41,040
the _EPROCESS

15
00:00:38,059 --> 00:00:43,559
pointer which ends with 0080

16
00:00:41,040 --> 00:00:46,980
in my case. And so what we can do is, we

17
00:00:43,559 --> 00:00:49,860
can just take that address and print it

18
00:00:46,980 --> 00:00:53,780
as an _EPROCESS. And in this case we are

19
00:00:49,860 --> 00:00:53,780
only interested in the Token pointer.

20
00:00:55,739 --> 00:00:59,879
So now, what we can do is we can click

21
00:00:57,899 --> 00:01:02,640
on Token and it's going to actually print

22
00:00:59,879 --> 00:01:05,880
its value. So we can see it holds the

23
00:01:02,640 --> 00:01:09,479
Object, the RefCnt, and the Object

24
00:01:05,880 --> 00:01:11,520
pointer in our case end with the 6064.

25
00:01:09,479 --> 00:01:14,600
So I'm just going to save that

26
00:01:11,520 --> 00:01:14,600
into a text file.

27
00:01:21,720 --> 00:01:25,920
Okay. So now what we need to do is, we

28
00:01:24,659 --> 00:01:28,460
need to do the same for the System

29
00:01:25,920 --> 00:01:28,460
process.

30
00:01:32,040 --> 00:01:35,840
So we get the _EPROCESS address.

31
00:01:37,920 --> 00:01:42,420
And again we can print the actual Token

32
00:01:40,380 --> 00:01:46,340
value for that particular _EPROCESS

33
00:01:42,420 --> 00:01:46,340
associated with the System process.

34
00:01:54,659 --> 00:02:00,720
So what we basically have is, we know

35
00:01:57,899 --> 00:02:03,659
this is the Token associated with the

36
00:02:00,720 --> 00:02:05,100
cmd.exe and we want this pointer to be

37
00:02:03,659 --> 00:02:07,920
replaced by

38
00:02:05,100 --> 00:02:11,700
this pointer, basically. And the _EPROCESS

39
00:02:07,920 --> 00:02:14,879
for cmd.exe is actually this address. So

40
00:02:11,700 --> 00:02:20,780
what we want is, we want to modify this

41
00:02:14,879 --> 00:02:20,780
address + 0x358 so we do "edit qword".

42
00:02:25,620 --> 00:02:31,700
And what we want to write is, we want to

43
00:02:27,900 --> 00:02:31,700
write the Token for the System process.

44
00:02:39,599 --> 00:02:45,980
And so now, if you go back to

45
00:02:42,500 --> 00:02:49,140
printing the actual Token for our

46
00:02:45,980 --> 00:02:52,500
cmd.exe, we can see it matches the one

47
00:02:49,140 --> 00:02:54,480
associated with the System process.

48
00:02:52,500 --> 00:02:56,519
They are different tokens.

49
00:02:54,480 --> 00:03:00,420
Okay. So now what we can do is we can

50
00:02:56,519 --> 00:03:03,060
just hit "go", to continue execution.

51
00:03:00,420 --> 00:03:06,420
And we can go back to our target VM

52
00:03:03,060 --> 00:03:08,040
and type "whoami".

53
00:03:06,420 --> 00:03:10,260
Nice.

54
00:03:08,040 --> 00:03:12,620
So now we can see where our NT AUTHORITY\

55
00:03:10,260 --> 00:03:12,620
SYSTEM.

56
00:03:13,459 --> 00:03:18,599
And interestingly, Process Explorer

57
00:03:16,440 --> 00:03:21,540
doesn't detect it. I think it's because

58
00:03:18,599 --> 00:03:24,319
it assume it hasn't changed. So let's try

59
00:03:21,540 --> 00:03:24,319
to restart it.

60
00:03:32,040 --> 00:03:36,840
Right. So it's unable to open the Token

61
00:03:34,319 --> 00:03:39,300
because it's having higher privileges.

62
00:03:36,840 --> 00:03:41,840
So we're going to restart it with admin

63
00:03:39,300 --> 00:03:41,840
privileges.

64
00:03:48,060 --> 00:03:53,879
And as you can see now, Process Explorer

65
00:03:50,819 --> 00:03:56,180
detects it as being system. Thank you for

66
00:03:53,879 --> 00:03:56,180
watching.

