1
00:00:00,240 --> 00:00:04,259
So, in this part we're going to talk

2
00:00:01,560 --> 00:00:06,660
about kernel memory pools. A pool refers

3
00:00:04,259 --> 00:00:08,639
to a heap in the kernel. We're going to

4
00:00:06,660 --> 00:00:11,059
talk about the different types of pools.

5
00:00:08,639 --> 00:00:14,219
Let's get started. So, for whatever reason

6
00:00:11,059 --> 00:00:16,440
Windows calls their kernel heap a pool.

7
00:00:14,219 --> 00:00:20,400
And it doesn't really meet the general

8
00:00:16,440 --> 00:00:22,439
definition of what a pool type is, which

9
00:00:20,400 --> 00:00:26,160
makes it confusing at first because

10
00:00:22,439 --> 00:00:29,220
basically [generally] a pool is an allocator that

11
00:00:26,160 --> 00:00:32,000
uses some set of dedicated pages and

12
00:00:29,220 --> 00:00:36,059
those pages have fixed size allocations.

13
00:00:32,000 --> 00:00:38,820
So, all allocations of the same size would

14
00:00:36,059 --> 00:00:41,700
be held on the same pages. And different

15
00:00:38,820 --> 00:00:44,940
pages are for different sizes. Whereas

16
00:00:41,700 --> 00:00:46,739
on Windows, any chunk size can all be on

17
00:00:44,940 --> 00:00:49,739
the same page, so in terms of definition,

18
00:00:46,739 --> 00:00:52,680
the kernel pool are more like a generic

19
00:00:49,739 --> 00:00:54,600
heap. So, there are two main heaps, or pool

20
00:00:52,680 --> 00:00:56,219
heaps in the Windows kernel. One of them

21
00:00:54,600 --> 00:00:58,859
is pageable, and the other one is

22
00:00:56,219 --> 00:01:01,920
non-pageable. So, for instance, code that

23
00:00:58,859 --> 00:01:04,080
runs at dispatch level, which is not able

24
00:01:01,920 --> 00:01:05,939
to deal with page faults, has to only

25
00:01:04,080 --> 00:01:08,340
access memory in the non-paged pool,

26
00:01:05,939 --> 00:01:10,560
because by definition, the allocation on

27
00:01:08,340 --> 00:01:13,200
the non-paged pool will never be paged

28
00:01:10,560 --> 00:01:16,140
out, so the allocation will always be

29
00:01:13,200 --> 00:01:18,299
backed with actual memory. And it's

30
00:01:16,140 --> 00:01:21,180
interesting to know that two types of

31
00:01:18,299 --> 00:01:23,460
pools exist. The paged pool and the

32
00:01:21,180 --> 00:01:24,960
non-paged pool. And this is because a lot

33
00:01:23,460 --> 00:01:26,700
of the time, if you're in a scenario

34
00:01:24,960 --> 00:01:28,979
where you want to exploit a heap

35
00:01:26,700 --> 00:01:31,320
corruption vulnerability, like a use-after-free,

36
00:01:28,979 --> 00:01:34,079
people will always make the

37
00:01:31,320 --> 00:01:36,540
distinction, like if it's a use-after-free

38
00:01:34,079 --> 00:01:38,579
on the non-paged pool or if it's a

39
00:01:36,540 --> 00:01:40,500
use-after-free on the paged pool. And this is

40
00:01:38,579 --> 00:01:42,840
because since the paged pool and the

41
00:01:40,500 --> 00:01:44,640
non-paged pool are different heaps, so

42
00:01:42,840 --> 00:01:46,619
they are in different areas in memory. So,

43
00:01:44,640 --> 00:01:49,079
usually you want to replace the freed

44
00:01:46,619 --> 00:01:51,299
chunk, before triggering the use-after-free

45
00:01:49,079 --> 00:01:54,060
but it means you have a different set of

46
00:01:51,299 --> 00:01:56,219
structures of object that you can put on

47
00:01:54,060 --> 00:01:58,500
the non-paged pool or respectively on the

48
00:01:56,219 --> 00:02:00,000
paged pool. So, it will heavily dictate

49
00:01:58,500 --> 00:02:02,520
what kind of interaction you're able to

50
00:02:00,000 --> 00:02:04,920
do with the kernel in order to exploit the

51
00:02:02,520 --> 00:02:07,560
said vulnerability. Also it's worth saying

52
00:02:04,920 --> 00:02:10,259
that every chunk of memory on a pool has

53
00:02:07,560 --> 00:02:12,060
a dedicated POOL_HEADER structure inlined

54
00:02:10,259 --> 00:02:14,099
right in front of its allocation.

55
00:02:12,060 --> 00:02:17,340
Basically the information looks like

56
00:02:14,099 --> 00:02:19,440
this, which is holding pretty standard

57
00:02:17,340 --> 00:02:21,840
information for heap chunks. So, we have

58
00:02:19,440 --> 00:02:24,120
PreviousSize which is the size of

59
00:02:21,840 --> 00:02:26,340
the chunk proceeding in memory. You have

60
00:02:24,120 --> 00:02:28,379
the BlockSize which is the size of the

61
00:02:26,340 --> 00:02:30,599
current chunk, which we are looking at.

62
00:02:28,379 --> 00:02:32,459
Then, you have the PoolType which

63
00:02:30,599 --> 00:02:35,519
basically indicates like non-paged pool

64
00:02:32,459 --> 00:02:37,440
or paged pool as we've just described.

65
00:02:35,519 --> 00:02:39,599
Then, you have the PoolTag which is

66
00:02:37,440 --> 00:02:41,700
basically used to associate like the

67
00:02:39,599 --> 00:02:44,280
subsystem in the kernel that made the

68
00:02:41,700 --> 00:02:47,220
allocation, so you can easily look up

69
00:02:44,280 --> 00:02:50,760
what type of object it is likely to be.

70
00:02:47,220 --> 00:02:53,400
And so fortunately WinDbg has this

71
00:02:50,760 --> 00:02:54,480
"!pool" command and you can give it an

72
00:02:53,400 --> 00:02:57,000
address

73
00:02:54,480 --> 00:02:59,400
on a pool, and it will tell you if it's

74
00:02:57,000 --> 00:03:02,099
an allocated or free chunk, and it will

75
00:02:59,400 --> 00:03:04,379
tell you the page where it's allocated.

76
00:03:02,099 --> 00:03:06,360
And it will tell you all the information such

77
00:03:04,379 --> 00:03:09,659
as the PoolTag which I have just described,

78
00:03:06,360 --> 00:03:11,180
which in this case is NpFr, which tells us

79
00:03:09,659 --> 00:03:14,280
it's the

80
00:03:11,180 --> 00:03:16,980
npfs.sys driver which made the allocation.

81
00:03:14,280 --> 00:03:20,220
So, on top of the already mentioned page

82
00:03:16,980 --> 00:03:22,080
pool and non-paged pool types. There are two

83
00:03:20,220 --> 00:03:25,080
other pools worth mentioning which are

84
00:03:22,080 --> 00:03:27,599
the lookaside lists, and the kernel LFH

85
00:03:25,080 --> 00:03:29,280
like Low Fragmentation Heap. So, the

86
00:03:27,599 --> 00:03:32,819
lookaside lists are used for faster

87
00:03:29,280 --> 00:03:35,159
allocations, usually of small size. And so

88
00:03:32,819 --> 00:03:36,959
sometimes when exploiting bugs in the

89
00:03:35,159 --> 00:03:39,659
kernel, you might encounter a scenario

90
00:03:36,959 --> 00:03:42,180
where you want to force an object to be

91
00:03:39,659 --> 00:03:44,159
freed and this object will be set into a

92
00:03:42,180 --> 00:03:46,620
lookaside list. So, it's good to know

93
00:03:44,159 --> 00:03:48,720
this exists in case you encounter

94
00:03:46,620 --> 00:03:50,099
this scenario. And finally, you have the

95
00:03:48,720 --> 00:03:52,620
kLFH.

96
00:03:50,099 --> 00:03:54,840
And so basically, in userland, there has

97
00:03:52,620 --> 00:03:56,879
been the Low Fragmentation Heap for a

98
00:03:54,840 --> 00:03:59,159
while, and this is a type of heap where

99
00:03:56,879 --> 00:04:01,439
the chunks of the same size are

100
00:03:59,159 --> 00:04:04,019
allocated on dedicated pages. So

101
00:04:01,439 --> 00:04:06,659
Microsoft added a new pool type recently

102
00:04:04,019 --> 00:04:08,340
in the kernel that is called kLFH for

103
00:04:06,659 --> 00:04:11,340
Kernel Low Fragmentation Heap.

104
00:04:08,340 --> 00:04:13,500
And basically the kLFH pool is a real pool

105
00:04:11,340 --> 00:04:16,259
in that different objects of different

106
00:04:13,500 --> 00:04:18,600
types are allocated on separated pages.

107
00:04:16,259 --> 00:04:20,579
And this seems to be quite effective to

108
00:04:18,600 --> 00:04:24,419
make many use-after-free vulnerabilities

109
00:04:20,579 --> 00:04:27,919
hardly exploitable. And so it is like the

110
00:04:24,419 --> 00:04:32,160
actual definition of pool, not Microsoft's

111
00:04:27,919 --> 00:04:35,040
non-pool pools. And so, kLFH exists since

112
00:04:32,160 --> 00:04:36,960
Windows 10 1809, but it was initially

113
00:04:35,040 --> 00:04:39,120
disabled by default and it was only

114
00:04:36,960 --> 00:04:42,320
enabled by default in the following

115
00:04:39,120 --> 00:04:42,320
version 1903.

