1
00:00:00,480 --> 00:00:05,759
Hey. You're here for the instructions of

2
00:00:03,360 --> 00:00:07,020
the next lab. So here, you're going to have

3
00:00:05,759 --> 00:00:10,200
to basically

4
00:00:07,020 --> 00:00:13,200
spawn a command prompt, check that you

5
00:00:10,200 --> 00:00:15,599
are just a regular user, and then with

6
00:00:13,200 --> 00:00:18,060
the debugger attached to that VM,

7
00:00:15,599 --> 00:00:21,840
you're going to basically modify your

8
00:00:18,060 --> 00:00:23,640
cmd.exe privileges to make it more privileged. In

9
00:00:21,840 --> 00:00:27,300
order to do, so you're going to have to find

10
00:00:23,640 --> 00:00:30,539
the cmd.exe EPROCESS structure as well as

11
00:00:27,300 --> 00:00:33,420
the system process' EPROCESS structure.

12
00:00:30,539 --> 00:00:36,540
And then just steal the Token for the

13
00:00:33,420 --> 00:00:40,079
System process, and just apply it to your

14
00:00:36,540 --> 00:00:42,320
cmd.exe EPROCESS Token. Okay. Now it's your

15
00:00:40,079 --> 00:00:42,320
turn.

