1
00:00:00,080 --> 00:00:06,560
So, let's look at the CreateFile() syscall.

2
00:00:03,919 --> 00:00:09,279
So, we're going to look for MSDN

3
00:00:06,560 --> 00:00:09,279
CreateFile.

4
00:00:13,200 --> 00:00:18,480
We see this function is used to open or

5
00:00:15,519 --> 00:00:20,480
create a file or I/O device. This function

6
00:00:18,480 --> 00:00:22,480
returns a HANDLE that can be used to

7
00:00:20,480 --> 00:00:24,800
access the file for other types of

8
00:00:22,480 --> 00:00:24,800
things.

9
00:00:25,519 --> 00:00:29,039
This is the prototype of the function,

10
00:00:27,439 --> 00:00:32,239
all the different parameters are

11
00:00:29,039 --> 00:00:34,559
described after so let's go over them.

12
00:00:32,239 --> 00:00:39,040
lpFileName is basically the file you want

13
00:00:34,559 --> 00:00:39,040
to create or open, it has a max size.

14
00:00:39,760 --> 00:00:44,399
dwDesiredAccess

15
00:00:42,079 --> 00:00:46,800
is the actual access of the file you

16
00:00:44,399 --> 00:00:49,039
want to create or open.

17
00:00:46,800 --> 00:00:51,520
So, usually you want to open or create

18
00:00:49,039 --> 00:00:53,520
the file with read or write access or

19
00:00:51,520 --> 00:00:55,199
both of them.

20
00:00:53,520 --> 00:00:58,079
It says that you cannot request an

21
00:00:55,199 --> 00:00:59,199
access that conflicts with another call

22
00:00:58,079 --> 00:01:01,840
that has been

23
00:00:59,199 --> 00:01:04,479
made to open or create that file with a

24
00:01:01,840 --> 00:01:06,799
share mode that is not allowing you to

25
00:01:04,479 --> 00:01:10,159
do the operation. So, if we look at

26
00:01:06,799 --> 00:01:11,119
dwShareMode, we see it has a way

27
00:01:10,159 --> 00:01:12,479
to

28
00:01:11,119 --> 00:01:15,280
allow another

29
00:01:12,479 --> 00:01:17,759
call to actually read the file

30
00:01:15,280 --> 00:01:19,040
on top of us or write the file. As you

31
00:01:17,759 --> 00:01:21,680
can imagine

32
00:01:19,040 --> 00:01:24,479
this is enforced in the kernel.

33
00:01:21,680 --> 00:01:26,080
Then, there is the lpSecurityAttributes,

34
00:01:24,479 --> 00:01:28,080
which is a pointer to a SECURITY_

35
00:01:26,080 --> 00:01:30,400
ATTRIBUTE structure that can contain

36
00:01:28,080 --> 00:01:32,479
additional security information. If we

37
00:01:30,400 --> 00:01:34,000
look at this structure, we can see there

38
00:01:32,479 --> 00:01:34,960
is a boolean

39
00:01:34,000 --> 00:01:37,600
which is

40
00:01:34,960 --> 00:01:41,119
bInheritHandle, which as you can imagine,

41
00:01:37,600 --> 00:01:43,680
describes if the open file can be

42
00:01:41,119 --> 00:01:46,799
inherited

43
00:01:43,680 --> 00:01:46,799
by sub-processes.

44
00:01:47,200 --> 00:01:53,040
Then, there is the dwCreationDisposition.

45
00:01:50,640 --> 00:01:56,479
For instance, if we look at the OPEN_EXISTING

46
00:01:53,040 --> 00:01:58,719
flag, it says that it tries to open a

47
00:01:56,479 --> 00:02:00,880
file only if it exists,

48
00:01:58,719 --> 00:02:04,399
and if it does not exist, the function

49
00:02:00,880 --> 00:02:06,960
will fail. Then, we can look at dwFlagsAndAttributes,

50
00:02:04,399 --> 00:02:09,840
it says that if we are

51
00:02:06,960 --> 00:02:11,520
actually opening a file that exists,

52
00:02:09,840 --> 00:02:14,319
this

53
00:02:11,520 --> 00:02:18,000
dwFlagsAndAttributes will be ignored,

54
00:02:14,319 --> 00:02:20,319
however, if we actually create a new file

55
00:02:18,000 --> 00:02:23,920
we can set attributes, for instance, we

56
00:02:20,319 --> 00:02:28,319
can set that the file is hidden

57
00:02:23,920 --> 00:02:28,319
or that the file is read-only.

58
00:02:28,800 --> 00:02:33,360
If we go further down, we see lots of

59
00:02:30,560 --> 00:02:33,360
different flags.

60
00:02:35,360 --> 00:02:39,440
There is also a hTemplateFile that can

61
00:02:37,440 --> 00:02:42,160
be used, if you

62
00:02:39,440 --> 00:02:44,560
create a new file and you want specific

63
00:02:42,160 --> 00:02:46,879
attributes to be inherited from the

64
00:02:44,560 --> 00:02:50,319
template file.

65
00:02:46,879 --> 00:02:52,239
For the return value, we see it returns

66
00:02:50,319 --> 00:02:54,560
the actual HANDLE

67
00:02:52,239 --> 00:02:56,800
for the specified file.

68
00:02:54,560 --> 00:02:59,200
And then specific

69
00:02:56,800 --> 00:03:01,040
value which is INVALID_HANDLE_VALUE if

70
00:02:59,200 --> 00:03:02,159
it failed, and then there are lots of

71
00:03:01,040 --> 00:03:04,560
remarks.

72
00:03:02,159 --> 00:03:06,959
If you're more interested in internals

73
00:03:04,560 --> 00:03:11,159
for CreateFile(), you can look at them. So

74
00:03:06,959 --> 00:03:11,159
now let's look at the actual syscall.

75
00:03:22,800 --> 00:03:27,840
If we compare both prototypes,

76
00:03:25,840 --> 00:03:31,599
we can see that a lot of arguments are

77
00:03:27,840 --> 00:03:33,120
actually shared, DesiredAccess

78
00:03:31,599 --> 00:03:34,720
dwShareMode

79
00:03:33,120 --> 00:03:37,440
which is ShareAccess,

80
00:03:34,720 --> 00:03:39,760
lpSecurityAttributes

81
00:03:37,440 --> 00:03:43,680
is not in the actual syscall,

82
00:03:39,760 --> 00:03:46,560
dwCreationDisposition we have CreateDisposition,

83
00:03:43,680 --> 00:03:49,440
dwFlagsAndAttributes, we have

84
00:03:46,560 --> 00:03:51,920
FileAttributes.

85
00:03:49,440 --> 00:03:53,680
However, some of them are different.

86
00:03:51,920 --> 00:03:55,439
In the actual syscall, we see the actual

87
00:03:53,680 --> 00:03:57,840
HANDLE is passed at the first argument

88
00:03:55,439 --> 00:03:59,680
as a pointer so we can imagine that once

89
00:03:57,840 --> 00:04:02,480
it's returned, it's going to be then

90
00:03:59,680 --> 00:04:06,080
returned into the CreateFile() function.

91
00:04:02,480 --> 00:04:07,280
So, let's look at some of the arguments

92
00:04:06,080 --> 00:04:09,519
FileHandle

93
00:04:07,280 --> 00:04:11,760
is a pointer to receive the actual file

94
00:04:09,519 --> 00:04:14,319
HANDLE that was created by the kernel.

95
00:04:11,760 --> 00:04:17,439
DesiredAccess is very similar to the

96
00:04:14,319 --> 00:04:17,439
dwDesiredAccess here.

97
00:04:17,519 --> 00:04:23,759
As you can see, they use different macros

98
00:04:20,160 --> 00:04:26,400
for the actual exposed macros to the

99
00:04:23,759 --> 00:04:29,840
user, and internal macros to the syscall,

100
00:04:26,400 --> 00:04:29,840
but they have the same value in general.

101
00:04:33,759 --> 00:04:37,680
The ObjectAttributes is interesting

102
00:04:35,600 --> 00:04:40,240
because we can see it holds the actual

103
00:04:37,680 --> 00:04:40,240
file name.

104
00:04:41,360 --> 00:04:46,080
So the file name is going to be passed

105
00:04:43,440 --> 00:04:47,440
as the exposed userland API but then,

106
00:04:46,080 --> 00:04:51,360
internally,

107
00:04:47,440 --> 00:04:53,520
it may be saved into ObjectName

108
00:04:51,360 --> 00:04:55,600
into the ObjectAttributes.

109
00:04:53,520 --> 00:04:57,280
This is why there is no filename passed

110
00:04:55,600 --> 00:05:01,520
to the actual syscall, because it's

111
00:04:57,280 --> 00:05:01,520
actually into the ObjectAttributes.

112
00:05:06,080 --> 00:05:11,560
Then, it has additional specific

113
00:05:08,560 --> 00:05:11,560
arguments,

114
00:05:18,320 --> 00:05:22,000
we can see the dwFlagsAndAttributes holds

115
00:05:20,960 --> 00:05:25,000
similar

116
00:05:22,000 --> 00:05:25,000
attributes

117
00:05:25,039 --> 00:05:28,880
to the actual FileAttributes macro[->argument]

118
00:05:29,759 --> 00:05:35,240
and we could compare other arguments as

119
00:05:32,240 --> 00:05:35,240
well.

120
00:05:41,360 --> 00:05:47,400
So, now let's look at ZwCreateFile()

121
00:05:44,160 --> 00:05:47,400
in the kernel.

122
00:05:54,160 --> 00:05:59,360
The first thing to notice is that, at the

123
00:05:56,080 --> 00:06:01,759
very top of the MSDN link, we can see

124
00:05:59,360 --> 00:06:04,560
NtCreateFile() is in Windows / Apps /

125
00:06:01,759 --> 00:06:07,919
Win32, which means it's in userland.

126
00:06:04,560 --> 00:06:09,440
But, ZwCreateFile() is actually in

127
00:06:07,919 --> 00:06:13,360
Windows Drivers / API /

128
00:06:09,440 --> 00:06:13,360
Kernel, so it's actually in kernel land.

129
00:06:13,680 --> 00:06:17,919
If we look at the actual arguments

130
00:06:15,840 --> 00:06:20,800
they are basically the same, which makes

131
00:06:17,919 --> 00:06:23,280
sense, because the syscall will trap the

132
00:06:20,800 --> 00:06:25,280
CPU into the kernel mode, but the arguments

133
00:06:23,280 --> 00:06:27,680
will be the same. If we look at the

134
00:06:25,280 --> 00:06:30,160
actual differences, most of the

135
00:06:27,680 --> 00:06:31,680
descriptions will actually be the same.

136
00:06:30,160 --> 00:06:34,080
However, sometimes there are some

137
00:06:31,680 --> 00:06:37,560
instructions that are more specifics on

138
00:06:34,080 --> 00:06:37,560
one or the other.

139
00:06:49,520 --> 00:06:52,880
If we look at the return value,

140
00:06:53,360 --> 00:06:58,240
ZwCreateFile() return a status success

141
00:06:56,479 --> 00:07:00,840
similarly to the syscall.

142
00:06:58,240 --> 00:07:05,759
Now, let's look at the actual code into

143
00:07:00,840 --> 00:07:05,759
Ghidra. We are looking for CreateFile()

144
00:07:06,160 --> 00:07:11,120
and so we are going to open

145
00:07:08,800 --> 00:07:13,360
CreateFile() into kernelbase.dll which is

146
00:07:11,120 --> 00:07:16,160
the exposed one for the user,

147
00:07:13,360 --> 00:07:17,840
then go into the actual syscall into ntdll.dll,

148
00:07:16,160 --> 00:07:19,919
and then the

149
00:07:17,840 --> 00:07:21,919
implementation into the kernel.

150
00:07:19,919 --> 00:07:23,759
Let's start with the actual exposed API

151
00:07:21,919 --> 00:07:26,560
for the user.

152
00:07:23,759 --> 00:07:27,599
I have actually renamed some stuff

153
00:07:26,560 --> 00:07:29,520
already,

154
00:07:27,599 --> 00:07:32,960
even though it's not perfect, it's going

155
00:07:29,520 --> 00:07:34,479
to give us insights on what is happening.

156
00:07:32,960 --> 00:07:36,880
So, for CreateFile(),

157
00:07:34,479 --> 00:07:39,599
we see the argument from MSDN that I

158
00:07:36,880 --> 00:07:39,599
have already named,

159
00:07:40,479 --> 00:07:45,479
and we see that an internal function is

160
00:07:42,479 --> 00:07:45,479
called,

161
00:07:47,120 --> 00:07:50,560
if we look at the internal function,

162
00:07:51,919 --> 00:07:54,639
we see that

163
00:07:56,400 --> 00:08:00,400
the FileName is converted to Unicode,

164
00:08:02,319 --> 00:08:10,400
and then it is stored into ObjectName

165
00:08:06,160 --> 00:08:10,400
for the ObjectAttributes structure.

166
00:08:11,039 --> 00:08:17,840
The length makes sense, 0x30 bytes,

167
00:08:14,160 --> 00:08:17,840
as we can see in Vergilius,

168
00:08:22,080 --> 00:08:26,240
the actual length of the Object

169
00:08:23,840 --> 00:08:27,919
Attribute is 0x30 bytes. So, it's setting

170
00:08:26,240 --> 00:08:29,759
the length to 0x30

171
00:08:27,919 --> 00:08:32,759
and then the ObjectName to the File

172
00:08:29,759 --> 00:08:32,759
Name.

173
00:08:41,360 --> 00:08:44,560
At the end,

174
00:08:42,479 --> 00:08:48,080
we see it's actually calling NtCreateFile()

175
00:08:44,560 --> 00:08:50,160
from ntdll.dll. The first argument

176
00:08:48,080 --> 00:08:52,240
is a pointer to the FileHandle to be returned

177
00:08:50,160 --> 00:08:54,959
by the syscall.

178
00:08:52,240 --> 00:08:56,640
Now, let's look at the actual syscall for

179
00:08:54,959 --> 00:08:59,800
NtCreateFile()

180
00:08:56,640 --> 00:08:59,800
in ntdll.dll.

181
00:09:00,720 --> 00:09:05,920
Here, it makes more sense to look at the

182
00:09:02,959 --> 00:09:05,920
actual assembly.

183
00:09:06,720 --> 00:09:12,240
We can see what happens is 0x55 is

184
00:09:10,160 --> 00:09:15,519
stored into EAX,

185
00:09:12,240 --> 00:09:17,839
and then the syscall instruction is called.

186
00:09:15,519 --> 00:09:20,080
Now, let's look at the actual syscall

187
00:09:17,839 --> 00:09:20,839
implementation into the kernel for

188
00:09:20,080 --> 00:09:23,440
NtCreateFile().

189
00:09:20,839 --> 00:09:27,279
We can see it calls an internal

190
00:09:23,440 --> 00:09:27,279
function called IopCreateFile().

191
00:09:27,680 --> 00:09:32,080
If we look at this function,

192
00:09:29,680 --> 00:09:34,720
we see the first thing it does, it sets a

193
00:09:32,080 --> 00:09:37,120
local variable called PreviousMode.

194
00:09:34,720 --> 00:09:40,080
PreviousMode is used to

195
00:09:37,120 --> 00:09:42,240
define if the call was made from kernel

196
00:09:40,080 --> 00:09:43,920
mode or from user mode.

197
00:09:42,240 --> 00:09:49,200
0 indicates it's been called from

198
00:09:43,920 --> 00:09:48,080
kernel mode. It actually

199
00:09:46,320 --> 00:09:50,800
sets PreviousMode

200
00:09:48,080 --> 00:09:54,800
to a specific version

201
00:09:50,800 --> 00:09:54,800
of it into an internal structure.

202
00:09:55,839 --> 00:09:59,519
After that, PreviousMode is tested

203
00:09:58,160 --> 00:10:02,000
against 0.

204
00:09:59,519 --> 00:10:03,440
If it's non-zero, it means it was called

205
00:10:02,000 --> 00:10:05,600
from user mode,

206
00:10:03,440 --> 00:10:06,880
so it does a bunch of additional checks

207
00:10:05,600 --> 00:10:09,200
to make sure

208
00:10:06,880 --> 00:10:11,120
we call the function from user mode with

209
00:10:09,200 --> 00:10:13,519
the right permissions.

210
00:10:11,120 --> 00:10:16,839
And it goes into an error if the test

211
00:10:13,519 --> 00:10:16,839
didn't pass.

212
00:10:21,120 --> 00:10:27,440
A lot of code is executed

213
00:10:24,480 --> 00:10:28,399
and at some points, it's going to call

214
00:10:27,440 --> 00:10:30,640


215
00:10:28,399 --> 00:10:34,160
ObOpenObjectByNameEx.

216
00:10:30,640 --> 00:10:36,640
This function is basically used to

217
00:10:34,160 --> 00:10:38,399
retrieve a HANDLE associated with the

218
00:10:36,640 --> 00:10:39,760
File object.

219
00:10:38,399 --> 00:10:42,160
As you can imagine, it's going to be

220
00:10:39,760 --> 00:10:44,160
returned to user mode since the HANDLE

221
00:10:42,160 --> 00:10:45,200
is what the user is going to be dealing

222
00:10:44,160 --> 00:10:48,079
with.

223
00:10:45,200 --> 00:10:50,160
Now, as an additional exercise,

224
00:10:48,079 --> 00:10:52,720
you could try to replicate what you've

225
00:10:50,160 --> 00:10:55,279
seen with another syscall.

226
00:10:52,720 --> 00:10:57,519
So taking for instance

227
00:10:55,279 --> 00:11:00,399
NtCreateTransactionManager()

228
00:10:57,519 --> 00:11:02,720
looking that up on the MSDN both for the

229
00:11:00,399 --> 00:11:05,279
user API, and the actual syscall, and then

230
00:11:02,720 --> 00:11:07,839
reverse entering it in your own

231
00:11:05,279 --> 00:11:10,560
disassembler. Another example would be

232
00:11:07,839 --> 00:11:10,560
CreateThread(),

233
00:11:10,720 --> 00:11:16,880
which again has its own user API, or its

234
00:11:14,720 --> 00:11:18,399
own kernel syscall.

235
00:11:16,880 --> 00:11:20,480
But, feel free to choose whatever you

236
00:11:18,399 --> 00:11:24,079
want reverse engineer

237
00:11:20,480 --> 00:11:24,079
to see if you can replicate that.

