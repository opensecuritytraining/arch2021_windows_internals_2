1
00:00:00,240 --> 00:00:02,239
In this lab, we're going to experiment


2
00:00:02,240 --> 00:00:06,399
with previously shown techniques.

3
00:00:04,960 --> 00:00:09,440
The first thing you're going to have to

4
00:00:06,399 --> 00:00:11,599
do, is to go on the j00ru website and

5
00:00:09,440 --> 00:00:14,080
find a specific syscall, and more

6
00:00:11,599 --> 00:00:16,720
specifically the actual syscall number

7
00:00:14,080 --> 00:00:19,359
associated with a syscall. Then, you're

8
00:00:16,720 --> 00:00:21,680
going to have to look at the ntdll code for

9
00:00:19,359 --> 00:00:24,720
that particular Windows 10 version, and

10
00:00:21,680 --> 00:00:26,960
make sure it matches what j00ru said it

11
00:00:24,720 --> 00:00:30,000
was on their website. You can start by

12
00:00:26,960 --> 00:00:31,760
doing that for NtCreateProcess().

13
00:00:30,000 --> 00:00:34,000
Then, you're going to have to go on Vergilius

14
00:00:31,760 --> 00:00:35,200
project website and

15
00:00:34,000 --> 00:00:37,120
browse the

16
00:00:35,200 --> 00:00:39,520
_ETHREAD structure and make sure you

17
00:00:37,120 --> 00:00:42,719
understand how the _KTHREAD structure is

18
00:00:39,520 --> 00:00:44,960
embedded into the _ETHREAD structure. Also,

19
00:00:42,719 --> 00:00:48,160
in the actual _KTHREAD structure, you'll

20
00:00:44,960 --> 00:00:50,640
try to find where there is a pointer to a

21
00:00:48,160 --> 00:00:52,879
_KPROCESS. And then once you are in

22
00:00:50,640 --> 00:00:54,960
_KPROCESS, you're going to have to see where

23
00:00:52,879 --> 00:00:56,239
it is used. Most specifically, you're

24
00:00:54,960 --> 00:00:58,320
going to have to find the _EPROCESS

25
00:00:56,239 --> 00:01:01,039
structure. Then, it's going to be time to

26
00:00:58,320 --> 00:01:03,359
analyze different IRQLs into your

27
00:01:01,039 --> 00:01:06,159
debugger. So, you're going to first start

28
00:01:03,359 --> 00:01:09,200
by analyzing the KeGetCurrentIrql()

29
00:01:06,159 --> 00:01:11,119
function, to understand in what register

30
00:01:09,200 --> 00:01:12,880
is saved the IRQL.

31
00:01:11,119 --> 00:01:15,759
Then, we are going to look at one

32
00:01:12,880 --> 00:01:18,479
function that actually modifies the IRQL.

33
00:01:15,759 --> 00:01:19,759
this function is called KeAcquireFastMutex.

34
00:01:18,479 --> 00:01:21,439


35
00:01:19,759 --> 00:01:23,280
Because you know in what register the

36
00:01:21,439 --> 00:01:25,840
IRQL is saved, you'll basically set a

37
00:01:23,280 --> 00:01:29,520
breakpoint before the IRQL is

38
00:01:25,840 --> 00:01:31,280
changed, and see the actual change of IRQL

39
00:01:29,520 --> 00:01:35,680
happening by stepping over that

40
00:01:31,280 --> 00:01:35,680
instruction. Okay, now it's your time!

