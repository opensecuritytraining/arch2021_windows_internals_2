1
00:00:00,399 --> 00:00:05,040
The goal of this lab is to understand

2
00:00:02,560 --> 00:00:07,120
how a syscall is called from userland to

3
00:00:05,040 --> 00:00:10,080
reach the kernel. The idea is going to be

4
00:00:07,120 --> 00:00:12,719
to reverse engineer the Windows API that

5
00:00:10,080 --> 00:00:16,880
is exposed to userland, then go to the

6
00:00:12,719 --> 00:00:18,880
actual syscall in ntdll, and finally see

7
00:00:16,880 --> 00:00:20,880
how it's called and the actual code in

8
00:00:18,880 --> 00:00:23,279
kernel mode. To do that, we're going to

9
00:00:20,880 --> 00:00:24,800
use MSDN which is the Microsoft

10
00:00:23,279 --> 00:00:26,320
documentation,

11
00:00:24,800 --> 00:00:28,160
and look at the different function

12
00:00:26,320 --> 00:00:30,400
prototypes and compare the different

13
00:00:28,160 --> 00:00:33,040
arguments. After that, we are going to

14
00:00:30,400 --> 00:00:36,000
reverse engineer the Windows API the

15
00:00:33,040 --> 00:00:37,520
actual syscall wrapper in ntdll,

16
00:00:36,000 --> 00:00:39,840
as well as the

17
00:00:37,520 --> 00:00:41,920
syscall implementation in the kernel. You

18
00:00:39,840 --> 00:00:43,840
can start doing that on the actual

19
00:00:41,920 --> 00:00:46,320
CreateFile() function, looking at

20
00:00:43,840 --> 00:00:48,239
NtCreateFile() for the wrapper, and

21
00:00:46,320 --> 00:00:51,840
ZwCreateFile() in the kernel.

22
00:00:48,239 --> 00:00:51,840
Now, it's your turn.

