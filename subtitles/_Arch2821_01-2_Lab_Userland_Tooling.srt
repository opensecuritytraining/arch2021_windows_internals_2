1
00:00:00,240 --> 00:00:04,160
Now, it is going to be time for you to do

2
00:00:02,720 --> 00:00:06,160
the work. The first thing you're going to

3
00:00:04,160 --> 00:00:08,639
have to do is to go on Vergilius

4
00:00:06,160 --> 00:00:10,480
website, and start poking around with the

5
00:00:08,639 --> 00:00:13,440
actual structures. You're going to have

6
00:00:10,480 --> 00:00:14,880
to find one structure, and then see if

7
00:00:13,440 --> 00:00:16,640
there is any difference between

8
00:00:14,880 --> 00:00:18,720
different version of Windows 10, for

9
00:00:16,640 --> 00:00:19,840
instance, you can look at the EPROCESS

10
00:00:18,720 --> 00:00:21,840
structure.

11
00:00:19,840 --> 00:00:23,840
Then, you are going to use

12
00:00:21,840 --> 00:00:25,920
Process Explorer to list the different processes

13
00:00:23,840 --> 00:00:27,680
that are running on your system, and then,

14
00:00:25,920 --> 00:00:29,599
for a given process, list all the

15
00:00:27,680 --> 00:00:32,079
different threads. You will be able to

16
00:00:29,599 --> 00:00:35,680
list the sessions associated with these

17
00:00:32,079 --> 00:00:38,160
processes, and also all the open HANDLEs

18
00:00:35,680 --> 00:00:40,879
for different objects. Finally, you'll

19
00:00:38,160 --> 00:00:43,280
have to use WinObj SysInternals tool to

20
00:00:40,879 --> 00:00:45,680
find new object types that you don't

21
00:00:43,280 --> 00:00:48,719
know and then find their associated

22
00:00:45,680 --> 00:00:51,440
kernel structures on Vergilius or in the

23
00:00:48,719 --> 00:00:53,760
WinDbg debugger. Okay, now it's your

24
00:00:51,440 --> 00:00:53,760
turn.

