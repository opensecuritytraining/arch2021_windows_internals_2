﻿1
00:00:00,480 --> 00:00:04,880
In this lab, we are going to analyze a

2
00:00:02,800 --> 00:00:07,440
vulnerability that was patched in June

3
00:00:04,880 --> 00:00:09,920
2021, and so you're going to have to use

4
00:00:07,440 --> 00:00:12,240
a target virtual machine that is before

5
00:00:09,920 --> 00:00:15,519
June 2021 to make sure it's vulnerable

6
00:00:12,240 --> 00:00:17,600
to this information disclosure vulnerability.

7
00:00:15,519 --> 00:00:20,560
So, we're going to run this executable,

8
00:00:17,600 --> 00:00:23,279
this exploit, on the actual VM, we're

9
00:00:20,560 --> 00:00:24,400
going to leak some _EPROCESS structures

10
00:00:23,279 --> 00:00:26,880
and then,

11
00:00:24,400 --> 00:00:28,800
with WinDbg, we're going to make sure

12
00:00:26,880 --> 00:00:31,039
these _EPROCESS structures are actually

13
00:00:28,800 --> 00:00:33,200
real structures and

14
00:00:31,039 --> 00:00:38,239
really we are leaking some

15
00:00:33,200 --> 00:00:38,239
kernel pointers. Okay, now it's your turn.

