﻿1
00:00:00,080 --> 00:00:04,799
So, the first thing we want to do is we

2
00:00:01,520 --> 00:00:07,759
want to see if we can match a syscall

3
00:00:04,799 --> 00:00:10,880
number from j00ru's website with the

4
00:00:07,759 --> 00:00:15,160
actual code from ntdll. So, for instance,

5
00:00:10,880 --> 00:00:15,160
let's look at NtCreateProcess.

6
00:00:18,560 --> 00:00:22,480
In our case, we're interested in Windows 10

7
00:00:20,240 --> 00:00:22,480


8
00:00:24,160 --> 00:00:29,960


9
00:00:25,840 --> 00:00:29,960
and the 7th column.

10
00:00:39,840 --> 00:00:46,640
So, NtCreateProcess for Windows 10

11
00:00:42,960 --> 00:00:48,559
1809 should be 0xB4.

12
00:00:46,640 --> 00:00:50,559
Now, let's break into the debugger and

13
00:00:48,559 --> 00:00:54,120
print the actual code for ntdll

14
00:00:50,559 --> 00:00:54,120
NtCreateProcess.

15
00:00:59,760 --> 00:01:04,000
It could be it's not actually showing

16
00:01:01,920 --> 00:01:05,119
because you're not actually debugging a

17
00:01:04,000 --> 00:01:06,560
userland [process].

18
00:01:05,119 --> 00:01:09,200
So, you're going to have to

19
00:01:06,560 --> 00:01:13,960
context switch to an actual process.

20
00:01:09,200 --> 00:01:13,960
Let's contact switch to lsass.

21
00:01:26,560 --> 00:01:31,520
We continue execution

22
00:01:28,320 --> 00:01:31,520
to reach that process.

23
00:01:33,200 --> 00:01:40,119
As we can see now, we're into lsass.

24
00:01:36,400 --> 00:01:40,119
You can reload the symbols.

25
00:01:48,399 --> 00:01:54,560
Now, we should be able to show the actual

26
00:01:51,040 --> 00:01:56,320
NtCreateProcess.

27
00:01:54,560 --> 00:01:58,560
As you can see,

28
00:01:56,320 --> 00:02:00,240
this is 0xB4,

29
00:01:58,560 --> 00:02:01,280
similarly to what was

30
00:02:00,240 --> 00:02:02,880
actually

31
00:02:01,280 --> 00:02:05,439
defined into

32
00:02:02,880 --> 00:02:09,280
j00ru's website. Now, let's take another

33
00:02:05,439 --> 00:02:12,399
one for instance NtCreateProcessEx,

34
00:02:09,280 --> 00:02:12,399
it should be 0x4D.

35
00:02:18,879 --> 00:02:25,440
Again, as you can see

36
00:02:21,760 --> 00:02:25,440
the syscall number is 0x4D.

37
00:02:27,200 --> 00:02:29,280
Okay,

38
00:02:28,319 --> 00:02:31,360
now

39
00:02:29,280 --> 00:02:33,840
take whatever syscall you want

40
00:02:31,360 --> 00:02:39,360
and check yourself that there is a match

41
00:02:33,840 --> 00:02:39,360
between j00ru's website and ntdll code.

