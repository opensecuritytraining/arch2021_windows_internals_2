﻿1
00:00:00,240 --> 00:00:05,040
So, if we look at the

2
00:00:02,240 --> 00:00:06,319
ExAcquireFastMutex function on react

3
00:00:05,040 --> 00:00:09,359
OS,

4
00:00:06,319 --> 00:00:09,359
we find this link.

5
00:00:09,760 --> 00:00:14,920
From here, we can look for the function,

6
00:00:13,280 --> 00:00:17,199
and then click on the actual

7
00:00:14,920 --> 00:00:18,720
function. It will show the actual code

8
00:00:17,199 --> 00:00:21,039
and we can show the full function by

9
00:00:18,720 --> 00:00:23,359
clicking here.

10
00:00:21,039 --> 00:00:25,359
So, now we have the code for ExAcquire

11
00:00:23,359 --> 00:00:27,359
FastMutex. We can see at the beginning

12
00:00:25,359 --> 00:00:28,400
that there is an ASSERT() which is not

13
00:00:27,359 --> 00:00:30,160
going to be

14
00:00:28,400 --> 00:00:33,840
in the actual release [build] but that actually

15
00:00:30,160 --> 00:00:37,920
checks that the IRQL is below APC_LEVEL

16
00:00:33,840 --> 00:00:40,800
by calling KeGetCurrentIrql.

17
00:00:37,920 --> 00:00:43,200
This function will actually retrieve the

18
00:00:40,800 --> 00:00:44,800
IRQL from a register.

19
00:00:43,200 --> 00:00:48,399
Also, this function

20
00:00:44,800 --> 00:00:50,000
actually raises the IRQL to APC_LEVEL

21
00:00:48,399 --> 00:00:52,000
before doing the rest.

22
00:00:50,000 --> 00:00:54,559
So, let's see how it's implemented into

23
00:00:52,000 --> 00:00:56,559
the actual assembly. Another thing worth

24
00:00:54,559 --> 00:00:59,600
mentioning is the documentation of

25
00:00:56,559 --> 00:01:02,399
ExAcquireFastMutex.

26
00:00:59,600 --> 00:01:06,320
We see on the MSDN that the callers need

27
00:01:02,399 --> 00:01:07,119
to be running IRQL under APC_LEVEL. And

28
00:01:06,320 --> 00:01:10,080
again,

29
00:01:07,119 --> 00:01:13,040
the documentation says that the IRQL is

30
00:01:10,080 --> 00:01:15,040
changed to APC_LEVEL. This confirms what

31
00:01:13,040 --> 00:01:16,560
ReactOS was telling us.

32
00:01:15,040 --> 00:01:19,680
Now, let's look at

33
00:01:16,560 --> 00:01:19,680
the code in WindDbg.

34
00:01:19,920 --> 00:01:25,680
Let's disassemble the

35
00:01:22,400 --> 00:01:27,920
Nt!KeGetCurrentIrql function.

36
00:01:25,680 --> 00:01:30,560
We can see this function is really small

37
00:01:27,920 --> 00:01:32,720
and all it does is it retrieves the CR8

38
00:01:30,560 --> 00:01:35,920
register and saves it

39
00:01:32,720 --> 00:01:40,560
into RAX to return it to the caller. So,

40
00:01:35,920 --> 00:01:42,320
basically CR8 holds the IRQL information.

41
00:01:40,560 --> 00:01:44,560
If we check the IRQL for our current

42
00:01:42,320 --> 00:01:48,079
thread,

43
00:01:44,560 --> 00:01:48,079
we see it's 13.

44
00:01:51,040 --> 00:01:56,000
If we check the actual CR8 value, it

45
00:01:53,439 --> 00:01:58,159
holds not only 13 but also other

46
00:01:56,000 --> 00:01:59,600
information. We can confirm the CR8

47
00:01:58,159 --> 00:02:01,439
register is used

48
00:01:59,600 --> 00:02:05,479
for the IRQL,

49
00:02:01,439 --> 00:02:05,479
by looking at blogs.

50
00:02:08,879 --> 00:02:13,200
Now, let's look at ExAcquireFastMutex.

51
00:02:16,080 --> 00:02:21,160
We set a breakpoint on that function.

52
00:02:22,319 --> 00:02:26,400
Once we hit the breakpoint, we check the

53
00:02:24,000 --> 00:02:30,480
IRQL.

54
00:02:26,400 --> 00:02:30,480
We can see we are at the LOW_LEVEL.

55
00:02:33,599 --> 00:02:38,080
So, now let's analyze the source code in

56
00:02:35,680 --> 00:02:38,080
Ghidra.

57
00:02:41,840 --> 00:02:45,440
The ExAcquireFastMutex

58
00:02:46,160 --> 00:02:50,640
is this. We are interested in

59
00:02:48,560 --> 00:02:52,720
finding where CR8 is used because we

60
00:02:50,640 --> 00:02:53,519
know it's going to be how you can change

61
00:02:52,720 --> 00:02:56,319
the

62
00:02:53,519 --> 00:02:56,319
IRQL.

63
00:02:58,959 --> 00:03:03,200
We see CR8

64
00:03:00,959 --> 00:03:05,280
is actually set here.

65
00:03:03,200 --> 00:03:07,120
So, here I'm going to set a breakpoint at

66
00:03:05,280 --> 00:03:08,080
that particular address

67
00:03:07,120 --> 00:03:10,640
into

68
00:03:08,080 --> 00:03:10,640
WinDbg.

69
00:03:11,599 --> 00:03:15,760
For this, I'm just going to use ret-sync

70
00:03:13,920 --> 00:03:19,400
to ease the process, but you can find the

71
00:03:15,760 --> 00:03:19,400
address otherwise.

72
00:03:36,959 --> 00:03:42,920
As you can see, I've set a breakpoint

73
00:03:38,959 --> 00:03:42,920
just before changing CR8.

74
00:03:47,440 --> 00:03:50,080
Now, as you can see,

75
00:03:49,120 --> 00:03:53,519
I'm still

76
00:03:50,080 --> 00:03:53,519
with IRQL LOW_LEVEL.

77
00:03:58,840 --> 00:04:06,080
RBX holds the old IRQL which is 0 for

78
00:04:02,799 --> 00:04:10,080
LOW_LEVEL, the new one we're going to set

79
00:04:06,080 --> 00:04:10,080
is 1 for APC_LEVEL.

80
00:04:10,799 --> 00:04:17,759
After stepping over, I check the IRQL.

81
00:04:14,720 --> 00:04:19,759
As you can see, now we are

82
00:04:17,759 --> 00:04:23,199
under APC_LEVEL.

83
00:04:19,759 --> 00:04:26,320
So, what we just saw is the KeRaiseIrql

84
00:04:23,199 --> 00:04:29,680
function that is inline.

85
00:04:26,320 --> 00:04:34,000
It actually raised the IRQL from

86
00:04:29,680 --> 00:04:34,000
LOW_LEVEL to APC_LEVEL.

