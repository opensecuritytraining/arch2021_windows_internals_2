1
00:00:00,120 --> 00:00:05,339
Hi everyone, in this video we're going to

2
00:00:02,879 --> 00:00:07,859
talk about kernel payloads, which is what

3
00:00:05,339 --> 00:00:09,540
can we do once we exploit a kernel

4
00:00:07,859 --> 00:00:12,240
vulnerability? We'll look at two

5
00:00:09,540 --> 00:00:14,400
different types of payloads, one which

6
00:00:12,240 --> 00:00:16,500
assumes you have code execution and you

7
00:00:14,400 --> 00:00:19,020
want to migrate to userland, and the

8
00:00:16,500 --> 00:00:22,080
other one which is just a data-only

9
00:00:19,020 --> 00:00:24,779
attack which consists in patching kernel

10
00:00:22,080 --> 00:00:27,420
structures in order to elevate a given

11
00:00:24,779 --> 00:00:30,779
process* to the System privileges. Okay,

12
00:00:27,420 --> 00:00:34,020
let's get started. Typically, when we have

13
00:00:30,779 --> 00:00:35,899
kernel privileges we can't really start a

14
00:00:34,020 --> 00:00:38,579
new process with high privileges

15
00:00:35,899 --> 00:00:41,640
directly from the kernel. But because we

16
00:00:38,579 --> 00:00:43,680
have the highest privileges possible in the

17
00:00:41,640 --> 00:00:46,200
kernel, basically with the highest

18
00:00:43,680 --> 00:00:49,800
privileges we can do less privileged

19
00:00:46,200 --> 00:00:53,280
actions obviously. And so, the idea is to

20
00:00:49,800 --> 00:00:57,360
migrate and execute a userland payload

21
00:00:53,280 --> 00:01:00,600
with the NT AUTHORITY\SYSTEM privileges.

22
00:00:57,360 --> 00:01:03,239
And so, typically, you have two ways to

23
00:01:00,600 --> 00:01:05,339
execute code in high privileged userland

24
00:01:03,239 --> 00:01:07,500
processes. If you already have kernel

25
00:01:05,339 --> 00:01:11,100
mode code execution, you can typically

26
00:01:07,500 --> 00:01:13,860
call APIs or use certain tricks, in order

27
00:01:11,100 --> 00:01:16,680
to then inject code into existing userland

28
00:01:13,860 --> 00:01:19,140
processes with high privileges. And

29
00:01:16,680 --> 00:01:22,320
the other method is to actually just do

30
00:01:19,140 --> 00:01:25,439
what we call data-only attacks, which

31
00:01:22,320 --> 00:01:27,720
basically doesn't even require any

32
00:01:25,439 --> 00:01:31,020
code execution into the kernel. All you

33
00:01:27,720 --> 00:01:33,240
usually need is an arbitrary read/write

34
00:01:31,020 --> 00:01:35,040
primitive into the kernel memory and

35
00:01:33,240 --> 00:01:38,119
then you can manipulate the kernel

36
00:01:35,040 --> 00:01:41,340
structures such as the tokens, security

37
00:01:38,119 --> 00:01:44,640
identifiers, or privileges, in order to

38
00:01:41,340 --> 00:01:46,860
give high privileges to your own process. The

39
00:01:44,640 --> 00:01:49,259
advantage of the former method is that

40
00:01:46,860 --> 00:01:52,259
it applies to both local and remote

41
00:01:49,259 --> 00:01:55,560
scenarios, whereas the latter method

42
00:01:52,259 --> 00:01:58,079
only applies to local exploitation. And

43
00:01:55,560 --> 00:02:00,600
so with recent mitigations in Windows 10

44
00:01:58,079 --> 00:02:03,000
and later, it's getting harder and harder

45
00:02:00,600 --> 00:02:06,119
to get code execution into the kernel. And

46
00:02:03,000 --> 00:02:08,280
so usually what you get first is an

47
00:02:06,119 --> 00:02:10,739
arbitrary read/write primitive, anyway.

48
00:02:08,280 --> 00:02:13,680
And so instead of then trying to get

49
00:02:10,739 --> 00:02:16,560
execution into the kernel, in the

50
00:02:13,680 --> 00:02:18,780
local privilege escalation case, what

51
00:02:16,560 --> 00:02:21,599
most attackers do is that they just

52
00:02:18,780 --> 00:02:24,000
manipulate tokens in order to elevate

53
00:02:21,599 --> 00:02:26,940
an existing process they control

54
00:02:24,000 --> 00:02:29,280
in userland since it's just fairly

55
00:02:26,940 --> 00:02:31,920
straightforward. And so now we are going

56
00:02:29,280 --> 00:02:34,140
to talk about data-only attacks and

57
00:02:31,920 --> 00:02:36,060
elevation of process payload more

58
00:02:34,140 --> 00:02:38,340
specifically. And we're going to detail

59
00:02:36,060 --> 00:02:40,739
three methods to actually achieve that

60
00:02:38,340 --> 00:02:43,140
goal which consists of manipulating

61
00:02:40,739 --> 00:02:46,500
certain structures into the kernel

62
00:02:43,140 --> 00:02:48,540
versus others. The first method, once you

63
00:02:46,500 --> 00:02:51,840
have like a kernel read/write primitive

64
00:02:48,540 --> 00:02:54,599
is that you want to find a pointer to a

65
00:02:51,840 --> 00:02:56,940
privileged Token in kernel memory, like the

66
00:02:54,599 --> 00:02:59,940
System Token, and patch your target

67
00:02:56,940 --> 00:03:03,360
process structures so that it actually

68
00:02:59,940 --> 00:03:05,819
uses that System Token pointer, instead

69
00:03:03,360 --> 00:03:08,760
of the original unprivileged Token

70
00:03:05,819 --> 00:03:11,760
pointer. And usually, the way that it works

71
00:03:08,760 --> 00:03:15,599
is you know that some existing high

72
00:03:11,760 --> 00:03:18,659
privilege process like lsass.exe or the

73
00:03:15,599 --> 00:03:21,360
System process has a reference in its

74
00:03:18,659 --> 00:03:24,480
_EPROCESS structure to the Token that

75
00:03:21,360 --> 00:03:27,360
you want, and you just find that Token

76
00:03:24,480 --> 00:03:29,700
pointer and you patch your own target

77
00:03:27,360 --> 00:03:32,519
_EPROCESS structure to use that

78
00:03:29,700 --> 00:03:36,360
privileged Token. So the first step would

79
00:03:32,519 --> 00:03:38,819
be to find an _EPROCESS structure of any

80
00:03:36,360 --> 00:03:41,340
process and then you can basically just

81
00:03:38,819 --> 00:03:45,000
walk the linked list of _EPROCESS

82
00:03:41,340 --> 00:03:47,220
structures until you find a privileged

83
00:03:45,000 --> 00:03:49,319
process. And then you just grab the

84
00:03:47,220 --> 00:03:51,420
pointer to the Token. Then, you continue

85
00:03:49,319 --> 00:03:54,239
working the linked list of _EPROCESS

86
00:03:51,420 --> 00:03:56,700
structures until you find your target

87
00:03:54,239 --> 00:03:58,980
process. And finally, you patch your

88
00:03:56,700 --> 00:04:02,280
target _EPROCESS to hold the privileged

89
00:03:58,980 --> 00:04:04,440
Token pointer you previously stole. In this

90
00:04:02,280 --> 00:04:08,040
example, we will assume we have some

91
00:04:04,440 --> 00:04:10,860
unprivileged command prompt cmd.exe, and

92
00:04:08,040 --> 00:04:12,959
if you type something like "whoami" you

93
00:04:10,860 --> 00:04:15,720
can see the Token associated with this

94
00:04:12,959 --> 00:04:19,079
process is just from a regular user. In

95
00:04:15,720 --> 00:04:21,120
our case it's the IEUser. And if we run

96
00:04:19,079 --> 00:04:23,820
our exploit from this command prompt,

97
00:04:21,120 --> 00:04:25,979
the goal would be to elevate this

98
00:04:23,820 --> 00:04:28,560
particular command prompt privileges to

99
00:04:25,979 --> 00:04:31,020
the NT AUTHORITY\SYSTEM user so that

100
00:04:28,560 --> 00:04:33,660
when you type "whoami" you can see you

101
00:04:31,020 --> 00:04:36,060
have System privileges. And so basically, we

102
00:04:33,660 --> 00:04:38,160
are going to show how to patch things in

103
00:04:36,060 --> 00:04:42,479
the debugger in order to effectively

104
00:04:38,160 --> 00:04:45,479
elevate our cmd.exe to system. And so,

105
00:04:42,479 --> 00:04:48,780
the first thing we do is, we dump out the

106
00:04:45,479 --> 00:04:51,120
process information of our cmd.exe using

107
00:04:48,780 --> 00:04:54,900
WinDbg to get the _EPROCESS address.

108
00:04:51,120 --> 00:04:57,479
From the _EPROCESS, we can get the Token

109
00:04:54,900 --> 00:05:01,740
structure pointer which is effectively

110
00:04:57,479 --> 00:05:04,979
an _EX_FAST_REF structure which is just a

111
00:05:01,740 --> 00:05:08,100
special type that is a reference counted

112
00:05:04,979 --> 00:05:11,040
object which has an Object field. And so

113
00:05:08,100 --> 00:05:14,340
in the Token case, the Object field

114
00:05:11,040 --> 00:05:18,180
points to the actual Token object. And so,

115
00:05:14,340 --> 00:05:20,060
we see that our cmd.exe Token pointer

116
00:05:18,180 --> 00:05:23,400
ends with

117
00:05:20,060 --> 00:05:26,100
7069. And then, you can do the same for

118
00:05:23,400 --> 00:05:29,220
the System process and find the Token

119
00:05:26,100 --> 00:05:32,759
pointer which is referenced by the

120
00:05:29,220 --> 00:05:33,780
object field of the _EX_FAST_REF structure

121
00:05:32,759 --> 00:05:36,120
as well.

122
00:05:33,780 --> 00:05:41,160
And so, we see that the System process

123
00:05:36,120 --> 00:05:43,440
Token pointer ends with the 0x044. This

124
00:05:41,160 --> 00:05:46,259
slide just shows how you would manually

125
00:05:43,440 --> 00:05:49,139
find the _EPROCESS pointers for your

126
00:05:46,259 --> 00:05:51,780
target cmd.exe process and for the System

127
00:05:49,139 --> 00:05:53,940
process assuming you already have a

128
00:05:51,780 --> 00:05:56,940
pointer inside the linked list of

129
00:05:53,940 --> 00:05:59,940
_EPROCESS. And so basically, it shows that

130
00:05:56,940 --> 00:06:02,280
the linked list is done using the

131
00:05:59,940 --> 00:06:05,759
LIST_ENTRY structure, which is very common

132
00:06:02,280 --> 00:06:09,840
on Windows. And this LIST_ENTRY is at

133
00:06:05,759 --> 00:06:12,660
offset 0x2e8 on that particular

134
00:06:09,840 --> 00:06:15,600
Windows version. And so, using the "dl"

135
00:06:12,660 --> 00:06:18,000
command we can actually see all the

136
00:06:15,600 --> 00:06:21,660
elements of the linked lists and we can

137
00:06:18,000 --> 00:06:24,660
find both the _EPROCESS for cmd.exe and

138
00:06:21,660 --> 00:06:27,180
the _EPROCESS for the System process. So

139
00:06:24,660 --> 00:06:31,440
inside the _EPROCESS structure, we said

140
00:06:27,180 --> 00:06:34,860
the offset to the linked list is 0x2e8,

141
00:06:31,440 --> 00:06:37,639
and so another field you need is the

142
00:06:34,860 --> 00:06:41,340
Token pointer and so it is at offset

143
00:06:37,639 --> 00:06:44,280
0x358. And so the idea here is that we are

144
00:06:41,340 --> 00:06:47,460
just dumping out the original Token

145
00:06:44,280 --> 00:06:50,819
pointer using the "dq" command, and we do

146
00:06:47,460 --> 00:06:55,319
that for the cmd.exe process. And we see

147
00:06:50,819 --> 00:06:58,560
it ends with 0x7069. And then we are

148
00:06:55,319 --> 00:07:01,800
editing this Token pointer with the "eq"

149
00:06:58,560 --> 00:07:04,080
command in order to replace it with the

150
00:07:01,800 --> 00:07:06,960
one from the System process ending

151
00:07:04,080 --> 00:07:10,860
with 0x044. And then we can just

152
00:07:06,960 --> 00:07:14,160
confirm by re-reading it with "dq" that it

153
00:07:10,860 --> 00:07:17,699
now points to the modified value that

154
00:07:14,160 --> 00:07:19,860
we've just written using "eq". And then we

155
00:07:17,699 --> 00:07:22,500
can press "go" in the debugger after

156
00:07:19,860 --> 00:07:25,139
making these types of modification and

157
00:07:22,500 --> 00:07:28,620
then just use our command prompt again.

158
00:07:25,139 --> 00:07:31,139
And if you type "whoami" again, you can

159
00:07:28,620 --> 00:07:35,340
see that there is now a reference that

160
00:07:31,139 --> 00:07:39,180
cmd.exe is running at the NT AUTHORITY\

161
00:07:35,340 --> 00:07:40,919
SYSTEM user. And so to summarize, assuming

162
00:07:39,180 --> 00:07:43,500
you have an arbitrary read/write

163
00:07:40,919 --> 00:07:45,660
primitive, all you need to do is walking

164
00:07:43,500 --> 00:07:48,360
the linked list and so in the previous

165
00:07:45,660 --> 00:07:50,639
example the system process was like the

166
00:07:48,360 --> 00:07:54,180
7th entry so you're basically doing

167
00:07:50,639 --> 00:07:56,099
7 reads of a linked list to reach

168
00:07:54,180 --> 00:07:59,940
the _EPROCESS for the System process.

169
00:07:56,099 --> 00:08:02,699
Then, you can read out a pointer to the

170
00:07:59,940 --> 00:08:05,520
Token of the System process. And finally

171
00:08:02,699 --> 00:08:08,819
you can write this pointer into your

172
00:08:05,520 --> 00:08:11,280
_EPROCESS for the cmd.exe process. And so,

173
00:08:08,819 --> 00:08:15,120
you basically have like 10 steps

174
00:08:11,280 --> 00:08:17,819
maximum. Almost all of them are reads and

175
00:08:15,120 --> 00:08:20,639
at the end you do one write and so it's

176
00:08:17,819 --> 00:08:22,620
always the same for every exploit you

177
00:08:20,639 --> 00:08:24,300
would work on. And so once you get

178
00:08:22,620 --> 00:08:26,220
that kind of arbitrary read/write

179
00:08:24,300 --> 00:08:28,560
primitive, and once you've got your

180
00:08:26,220 --> 00:08:31,500
theory down of something like this in

181
00:08:28,560 --> 00:08:34,500
the debugger it's really trivial to add

182
00:08:31,500 --> 00:08:37,260
in a real world exploit. It is worth

183
00:08:34,500 --> 00:08:40,740
noting that there are some other options

184
00:08:37,260 --> 00:08:43,380
than patching a Token pointer directly,

185
00:08:40,740 --> 00:08:46,200
because security tools and stuff could

186
00:08:43,380 --> 00:08:49,140
detect this kind of thing. I want to

187
00:08:46,200 --> 00:08:52,080
guess that Kaspersky must have hooks or

188
00:08:49,140 --> 00:08:53,940
like a regular sort of kernel monitor

189
00:08:52,080 --> 00:08:56,279
that is checking for this type of

190
00:08:53,940 --> 00:08:58,680
behavior and then dumping like

191
00:08:56,279 --> 00:09:01,320
process memory or something, because they

192
00:08:58,680 --> 00:09:03,540
seem to catch a ton of zero days being

193
00:09:01,320 --> 00:09:07,019
exploited in-the-wild, and a lot of

194
00:09:03,540 --> 00:09:09,120
exploits just seem to steal tokens in

195
00:09:07,019 --> 00:09:11,519
order to elevate privileges. Presumably,

196
00:09:09,120 --> 00:09:14,760
they have some software that is running

197
00:09:11,519 --> 00:09:17,160
in the kernel that just takes a snapshot

198
00:09:14,760 --> 00:09:20,640
of all of the processes and the pointers

199
00:09:17,160 --> 00:09:23,700
to the tokens. And, if all of a sudden, the

200
00:09:20,640 --> 00:09:26,519
Token pointer changes versus it's just

201
00:09:23,700 --> 00:09:28,800
having some new permission changes that

202
00:09:26,519 --> 00:09:32,339
would make sense for it to have, the

203
00:09:28,800 --> 00:09:35,580
security tool will flag it and if it

204
00:09:32,339 --> 00:09:37,620
specifically pointed to a Token that is

205
00:09:35,580 --> 00:09:39,959
associated with something like this

206
00:09:37,620 --> 00:09:42,180
System process, that would not be normal.

207
00:09:39,959 --> 00:09:44,940
I would guess that it would never

208
00:09:42,180 --> 00:09:49,080
normally happen that a userland process

209
00:09:44,940 --> 00:09:52,080
would legitimately have a pointer to the

210
00:09:49,080 --> 00:09:54,360
exact same Token structure that is

211
00:09:52,080 --> 00:09:56,220
referenced by the System process, so it

212
00:09:54,360 --> 00:09:58,380
would just be like something that you

213
00:09:56,220 --> 00:10:01,200
would flag and then just immediately

214
00:09:58,380 --> 00:10:04,440
dump the process memory to get a copy of

215
00:10:01,200 --> 00:10:06,600
the exploit or whatever. So one way that

216
00:10:04,440 --> 00:10:08,160
some people would get around that is to

217
00:10:06,600 --> 00:10:10,620
just modify the actual security

218
00:10:08,160 --> 00:10:13,740
descriptors associated with the process

219
00:10:10,620 --> 00:10:16,320
to give the Token the same privileges as

220
00:10:13,740 --> 00:10:17,519
typical Administrator users. I mean I

221
00:10:16,320 --> 00:10:20,940
would guess that certain security

222
00:10:17,519 --> 00:10:24,540
projects could detect that too, but it is

223
00:10:20,940 --> 00:10:27,839
the eternal cat and mouse game. And so,

224
00:10:24,540 --> 00:10:30,360
it may be stealthier to avoid patching a

225
00:10:27,839 --> 00:10:33,540
Token pointer directly. So the way it

226
00:10:30,360 --> 00:10:35,640
works is that the Token itself has a

227
00:10:33,540 --> 00:10:39,779
reference to the actual owner Security

228
00:10:35,640 --> 00:10:42,300
Identifier, like the SID. So when we are

229
00:10:39,779 --> 00:10:44,160
parsing the System _EPROCESS, instead

230
00:10:42,300 --> 00:10:46,440
of cloning the actual System Token

231
00:10:44,160 --> 00:10:48,980
pointer, we can actually parse the System

232
00:10:46,440 --> 00:10:52,079
Token structure and find the Security

233
00:10:48,980 --> 00:10:54,540
Identifiers for the owner and the groups

234
00:10:52,079 --> 00:10:58,320
which in this case are the Security

235
00:10:54,540 --> 00:11:01,140
Identifier for the Local System user and

236
00:10:58,320 --> 00:11:03,959
the Administrators group. And so we can

237
00:11:01,140 --> 00:11:07,079
just change the SID for the owner and

238
00:11:03,959 --> 00:11:09,959
the group in our cmd.exe Token to be

239
00:11:07,079 --> 00:11:11,940
this Local System and Administrators one.

240
00:11:09,959 --> 00:11:14,220
And so, this is what it looks like in the

241
00:11:11,940 --> 00:11:17,100
debugger. Using the the !token

242
00:11:14,220 --> 00:11:19,640
command and passing the Token pointers,

243
00:11:17,100 --> 00:11:23,100
we list all the Security Descriptors

244
00:11:19,640 --> 00:11:25,380
stored in the cmd.exe Token, as well as

245
00:11:23,100 --> 00:11:27,600
the System process Token. And so

246
00:11:25,380 --> 00:11:30,180
typically, we would want to just replace

247
00:11:27,600 --> 00:11:32,820
some of the Security Descriptors from

248
00:11:30,180 --> 00:11:34,500
the cmd.exe process with some of the

249
00:11:32,820 --> 00:11:37,920
Security Descriptors from the System

250
00:11:34,500 --> 00:11:40,620
process, which again might bypass certain

251
00:11:37,920 --> 00:11:42,899
security software. But generally, in our

252
00:11:40,620 --> 00:11:46,079
experience we don't actually need to

253
00:11:42,899 --> 00:11:49,560
worry about doing that. The last method

254
00:11:46,079 --> 00:11:52,019
to do data-only attacks to elevate our

255
00:11:49,560 --> 00:11:55,200
process is to just modify the actual

256
00:11:52,019 --> 00:11:57,779
Token itself to give it all of the

257
00:11:55,200 --> 00:12:00,779
privileges which would let our process

258
00:11:57,779 --> 00:12:03,779
access other processes memory. Adding new

259
00:12:00,779 --> 00:12:06,420
privileges is just a matter of adjusting

260
00:12:03,779 --> 00:12:08,820
a bitmap in the Token to add the

261
00:12:06,420 --> 00:12:12,240
privileges. And so typically, you would add

262
00:12:08,820 --> 00:12:14,459
the SeDebugPrivilege capability, so you

263
00:12:12,240 --> 00:12:17,279
can read other processes memory like

264
00:12:14,459 --> 00:12:19,980
lsass, so you can dump hashes or

265
00:12:17,279 --> 00:12:22,980
credentials, pivot to other systems and

266
00:12:19,980 --> 00:12:26,220
so on. This slide just shows that a

267
00:12:22,980 --> 00:12:29,820
typical cmd.exe is missing all the super

268
00:12:26,220 --> 00:12:32,640
powerful privileges like SeDebugPrivilege.

269
00:12:29,820 --> 00:12:36,240
And so you could just patch this if you

270
00:12:32,640 --> 00:12:39,180
wanted into your cmd.exe privileges. But

271
00:12:36,240 --> 00:12:41,279
again, in our experience we don't need to

272
00:12:39,180 --> 00:12:46,160
worry about doing that and patching the

273
00:12:41,279 --> 00:12:46,160
actual Token pointer is enough in general.

