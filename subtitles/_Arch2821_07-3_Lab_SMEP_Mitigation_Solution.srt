1
00:00:00,480 --> 00:00:05,279
So, we want to make sure that SMEP is

2
00:00:02,800 --> 00:00:07,919
enabled and enforced correctly. So, let's execute the TestSMEP binary.

4
00:00:07,919 --> 00:00:13,120
Let's take note of the actual address

5
00:00:10,800 --> 00:00:16,800
on the heap that is in userland and that

6
00:00:13,120 --> 00:00:18,160
is marked as executable.

7
00:00:16,800 --> 00:00:20,800
Now, let's go on the

8
00:00:18,160 --> 00:00:22,720
debugger side.

9
00:00:20,800 --> 00:00:27,080
We're going to set a breakpoint on the

10
00:00:22,720 --> 00:00:27,080
NtCreateTransaction function.

11
00:00:38,960 --> 00:00:42,960
Now, we go back to the actual

12
00:00:40,879 --> 00:00:46,559
binary execution and we're going to hit a

13
00:00:42,960 --> 00:00:46,559
key to actually trigger the syscall.

14
00:00:47,440 --> 00:00:51,520
I've done it. As you can see we don't see

15
00:00:49,280 --> 00:00:52,879
the cursor anymore because the

16
00:00:51,520 --> 00:00:55,520
VM

17
00:00:52,879 --> 00:00:55,520
is hanging.

18
00:00:56,079 --> 00:00:59,840
Also we see the breakpoint hit.

19
00:01:11,200 --> 00:01:14,720
We can see we

20
00:01:12,640 --> 00:01:19,200
reached the NtCreateTransaction

21
00:01:14,720 --> 00:01:19,200
syscall implementation from TestSMEP.

22
00:01:19,360 --> 00:01:26,240
This is the actual memory in userland.

23
00:01:23,759 --> 00:01:28,720
We can see all the NOPs, as well as the

24
00:01:26,240 --> 00:01:28,720
int 3.

25
00:01:28,960 --> 00:01:37,560
We can show it as assembly. Now, let's

26
00:01:32,479 --> 00:01:37,560
look at the value of the CR4 register.

27
00:01:41,119 --> 00:01:46,880
If we count the number of bytes, we have 8,

28
00:01:44,320 --> 00:01:50,799
16,

29
00:01:46,880 --> 00:01:52,960
17, 18, 19, 20.

30
00:01:50,799 --> 00:01:56,000
So, the higher one bit

31
00:01:52,960 --> 00:01:58,880
is actually the one for SMEP. This

32
00:01:56,000 --> 00:02:02,240
corresponds to the one here.

33
00:01:58,880 --> 00:02:04,960
So, if we set CR4 to just this value,

34
00:02:02,240 --> 00:02:07,960
it will actually unset the SMEP

35
00:02:04,960 --> 00:02:07,960
feature.

36
00:02:19,040 --> 00:02:25,120
Now, let's set the instruction pointer to

37
00:02:22,000 --> 00:02:25,120
the heap address.

38
00:02:32,560 --> 00:02:36,360
Now, if you continue execution,

39
00:02:38,000 --> 00:02:40,800
let's see what happens.

40
00:02:43,599 --> 00:02:50,640
We can see we're executing our userland

41
00:02:46,640 --> 00:02:53,519
area from kernel mode,

42
00:02:50,640 --> 00:02:56,080
so SMEP was successfully disabled.

43
00:02:53,519 --> 00:02:58,159
Now, let's reset the instruction pointer to

44
00:02:56,080 --> 00:03:01,159
the beginning of the heap area in userland.

45
00:02:58,159 --> 00:03:01,159


46
00:03:04,720 --> 00:03:10,080
Also, let's restore CR4 to its original

47
00:03:07,760 --> 00:03:10,080
value.

48
00:03:16,720 --> 00:03:21,519
Here, we have re-enabled SMEP. Let's see

49
00:03:18,640 --> 00:03:24,480
what happens in the debugger if we

50
00:03:21,519 --> 00:03:24,480
continue execution.

51
00:03:26,080 --> 00:03:33,040
As you can see, an exception is triggered

52
00:03:28,560 --> 00:03:34,640
more specifically the 0xfc exception.

53
00:03:33,040 --> 00:03:37,120
If we look at the actual

54
00:03:34,640 --> 00:03:40,400
MSDN documentation, it means we are

55
00:03:37,120 --> 00:03:42,319
trying to execute non-executable memory,

56
00:03:40,400 --> 00:03:43,440
which means SMEP was successfully

57
00:03:42,319 --> 00:03:45,920
enforced.

58
00:03:43,440 --> 00:03:48,480
As an exercise, you could actually now

59
00:03:45,920 --> 00:03:51,680
try to check other mitigations using the

60
00:03:48,480 --> 00:03:51,680
same kind of techniques.

