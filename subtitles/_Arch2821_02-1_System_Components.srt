1
00:00:00,000 --> 00:00:02,800
Hi everyone, in this part we're going to

2
00:00:02,000 --> 00:00:05,359
see

3
00:00:02,800 --> 00:00:07,680
the interactions between the userland

4
00:00:05,359 --> 00:00:09,679
and the kernel land. How the syscalls are

5
00:00:07,680 --> 00:00:11,840
called, also we're going to see

6
00:00:09,679 --> 00:00:13,440
the different components: the executive

7
00:00:11,840 --> 00:00:15,280
component as well as the kernel

8
00:00:13,440 --> 00:00:17,760
component and differences.

9
00:00:15,280 --> 00:00:20,240
Okay, let's get started. So, this is a very

10
00:00:17,760 --> 00:00:22,640
simplified view of the separation

11
00:00:20,240 --> 00:00:24,240
between user mode and kernel mode which

12
00:00:22,640 --> 00:00:27,039
people refer to

13
00:00:24,240 --> 00:00:29,760
as ring3 and ring0.

14
00:00:27,039 --> 00:00:32,640
But basically, in user mode, you have a ton of

15
00:00:29,760 --> 00:00:34,559
services, user applications and some

16
00:00:32,640 --> 00:00:37,200
stuff associated with the Windowing

17
00:00:34,559 --> 00:00:39,760
environment. What these

18
00:00:37,200 --> 00:00:43,000
programs end up doing

19
00:00:39,760 --> 00:00:44,680
actually goes into System DLLs, like

20
00:00:43,000 --> 00:00:47,520
ntdll.dll or

21
00:00:44,680 --> 00:00:49,840
user32.dll and eventually they'll cross

22
00:00:47,520 --> 00:00:52,239
over into the kernel, typically through

23
00:00:49,840 --> 00:00:54,559
system calls. So, the main interface you

24
00:00:52,239 --> 00:00:57,039
interact with from userland is, like, an

25
00:00:54,559 --> 00:00:59,039
interface called Executive layer. And so,

26
00:00:57,039 --> 00:01:02,000
when you see something like _EPROCESS

27
00:00:59,039 --> 00:01:04,559
structure or _ETHREAD structure, the E

28
00:01:02,000 --> 00:01:06,799
stands for Executive, and it is the high

29
00:01:04,559 --> 00:01:09,280
level, more complicated view of things.

30
00:01:06,799 --> 00:01:12,240
And the kernel itself deals with the

31
00:01:09,280 --> 00:01:14,479
lower level side of things, because

32
00:01:12,240 --> 00:01:16,080
it's more interested in things

33
00:01:14,479 --> 00:01:18,880
like the scheduling, the memory

34
00:01:16,080 --> 00:01:21,840
management and it will typically deal

35
00:01:18,880 --> 00:01:24,479
with more simplified structures like

36
00:01:21,840 --> 00:01:27,119
_KPROCESS or _KTHREAD and so

37
00:01:24,479 --> 00:01:29,280
the K in _KPROCESS stands for

38
00:01:27,119 --> 00:01:31,840
Kernel. So, typically,

39
00:01:29,280 --> 00:01:34,000
an "E" structure will actually have one of

40
00:01:31,840 --> 00:01:36,799
its member the first member being a

41
00:01:34,000 --> 00:01:39,040
_KPROCESS and so the same object can be

42
00:01:36,799 --> 00:01:41,280
used from either the Executive part or

43
00:01:39,040 --> 00:01:42,880
the Kernel part but just when it's

44
00:01:41,280 --> 00:01:45,439
actually in the kernel part it will just

45
00:01:42,880 --> 00:01:48,079
ignore the complexity of the _EPROCESS

46
00:01:45,439 --> 00:01:50,640
part. So, the Executive layer actually

47
00:01:48,079 --> 00:01:52,560
abstracts things like access to the

48
00:01:50,640 --> 00:01:55,040
device drivers. So, anytime you'll be

49
00:01:52,560 --> 00:01:57,840
working on Windows, you'll see a bunch of

50
00:01:55,040 --> 00:02:00,240
default System processes. The one most

51
00:01:57,840 --> 00:02:02,479
people are familiar with, in general, is

52
00:02:00,240 --> 00:02:05,360
lsass, mainly because it's the one you

53
00:02:02,479 --> 00:02:07,840
target with a tool like Mimikats to

54
00:02:05,360 --> 00:02:09,759
scrape credentials or hashes, and if you

55
00:02:07,840 --> 00:02:12,080
look at the processes shown by Process

56
00:02:09,759 --> 00:02:14,640
Explorer, you'll see

57
00:02:12,080 --> 00:02:17,440
also kernel threads and themselves that

58
00:02:14,640 --> 00:02:19,760
are like System services but they won't

59
00:02:17,440 --> 00:02:20,879
necessarily have a corresponding userland

60
00:02:19,760 --> 00:02:23,360
process.

61
00:02:20,879 --> 00:02:25,599
They will always be running in kernel. So

62
00:02:23,360 --> 00:02:28,080
generally, knowing about some of these

63
00:02:25,599 --> 00:02:29,680
processes is useful from a kernel

64
00:02:28,080 --> 00:02:31,760
exploitation perspective because

65
00:02:29,680 --> 00:02:34,640
typically they will always be running

66
00:02:31,760 --> 00:02:36,800
with special privileges. So, if you end up

67
00:02:34,640 --> 00:02:38,959
attacking the kernel and you want to

68
00:02:36,800 --> 00:02:41,360
pull out some kind of System level Token,

69
00:02:38,959 --> 00:02:43,920
like a Token with highest privileges, you

70
00:02:41,360 --> 00:02:45,680
know that, for instance, lsass will always

71
00:02:43,920 --> 00:02:47,680
have this kind of Token. So, if you can

72
00:02:45,680 --> 00:02:49,760
find an _EPROCESS structure, whatever

73
00:02:47,680 --> 00:02:52,080
process it is, and then work through the

74
00:02:49,760 --> 00:02:54,560
linked list of processes, and find the

75
00:02:52,080 --> 00:02:57,040
_EPROCESS associated with lsass,

76
00:02:54,560 --> 00:03:00,080
inevitably, you can find a System Token.

77
00:02:57,040 --> 00:03:03,120
So ntdll.dll is basically the equivalent of

78
00:03:00,080 --> 00:03:05,920
libC on Linux, but this time for Windows.

79
00:03:03,120 --> 00:03:09,120
And so, a lot of it is basically

80
00:03:05,920 --> 00:03:11,680
about abstracting access to system calls.

81
00:03:09,120 --> 00:03:14,159
So, according to Microsoft, system calls

82
00:03:11,680 --> 00:03:16,720
are volatile and can change and they

83
00:03:14,159 --> 00:03:19,360
often do, like the numbers associated

84
00:03:16,720 --> 00:03:21,519
with the system calls often change, the

85
00:03:19,360 --> 00:03:23,440
argument can change sometimes as well

86
00:03:21,519 --> 00:03:26,319
and this is why they say you have to

87
00:03:23,440 --> 00:03:29,040
call the APIs documented on the MSDN.

88
00:03:26,319 --> 00:03:31,840
So, ntdll is implementing the system

89
00:03:29,040 --> 00:03:33,760
calls themselves starting with Nt. So,

90
00:03:31,840 --> 00:03:37,680
typically the functions you would call

91
00:03:33,760 --> 00:03:41,360
are from other DLLs like user32.dll

92
00:03:37,680 --> 00:03:43,440
or kernel32.dll. And so ntdll on top of

93
00:03:41,360 --> 00:03:45,519
implementing the system calls is also

94
00:03:43,440 --> 00:03:47,519
responsible for managing the heap, it

95
00:03:45,519 --> 00:03:48,959
contains a lot of C runtime functions

96
00:03:47,519 --> 00:03:49,760
like memcpy

97
00:03:48,959 --> 00:03:52,080
or

98
00:03:49,760 --> 00:03:55,200
a lot of Microsoft typical functions to

99
00:03:52,080 --> 00:03:57,120
convert strings, etc. But generally, if

100
00:03:55,200 --> 00:03:59,360
you're interested in Windows kernel

101
00:03:57,120 --> 00:04:01,680
exploitation, you'll want to reverse

102
00:03:59,360 --> 00:04:04,080
engineer how the system calls are

103
00:04:01,680 --> 00:04:06,400
implemented exactly, how are they

104
00:04:04,080 --> 00:04:07,439
implemented into ntdll, and this is

105
00:04:06,400 --> 00:04:10,080
because

106
00:04:07,439 --> 00:04:12,720
sometimes you'll want to trigger a kernel

107
00:04:10,080 --> 00:04:15,040
vulnerability, and so for that you'll need to

108
00:04:12,720 --> 00:04:18,079
call a system call but the wrapper in

109
00:04:15,040 --> 00:04:20,400
ntdll might have some sanity checks that

110
00:04:18,079 --> 00:04:22,800
prevent you from reaching the kernel

111
00:04:20,400 --> 00:04:25,199
side, and so in order to reach the kernel

112
00:04:22,800 --> 00:04:27,440
bug you'll have to basically

113
00:04:25,199 --> 00:04:29,680
call the system yourself by using

114
00:04:27,440 --> 00:04:32,720
the actual assembly instructions instead

115
00:04:29,680 --> 00:04:35,120
of actually going through the normal API

116
00:04:32,720 --> 00:04:37,199
and through ntdll. So this is typically

117
00:04:35,120 --> 00:04:39,520
how you would talk to a driver from userland.

118
00:04:37,199 --> 00:04:41,919
You would start by calling the

119
00:04:39,520 --> 00:04:44,240
ReadFile function from kernel32, the one

120
00:04:41,919 --> 00:04:47,520
that is actually documented on the MSDN,

121
00:04:44,240 --> 00:04:49,680
and that will call into ntdll, the actual

122
00:04:47,520 --> 00:04:53,040
wrapper for the syscall, the one starting

123
00:04:49,680 --> 00:04:55,199
with Nt, and that function will actually

124
00:04:53,040 --> 00:04:59,120
end up calling the assembly instruction

125
00:04:55,199 --> 00:05:01,440
to do the syscall, which traps you into the

126
00:04:59,120 --> 00:05:03,199
more privileged mode, which calls the

127
00:05:01,440 --> 00:05:04,880
kernel side. And then depending on the

128
00:05:03,199 --> 00:05:07,120
file type you are looking at, it will

129
00:05:04,880 --> 00:05:09,600
know if it is associated to a specific

130
00:05:07,120 --> 00:05:12,160
driver and it will invoke that driver

131
00:05:09,600 --> 00:05:14,400
logic. So, for instance the actual ReadFile

132
00:05:12,160 --> 00:05:17,520
implementation. So, sometimes you are

133
00:05:14,400 --> 00:05:20,720
reversing code and it is unclear if you

134
00:05:17,520 --> 00:05:22,479
are looking at the Executive part or the

135
00:05:20,720 --> 00:05:25,520
Kernel part but it's not always

136
00:05:22,479 --> 00:05:28,000
important, However, it's good to know that

137
00:05:25,520 --> 00:05:30,320
the two layers exist, as sometimes,

138
00:05:28,000 --> 00:05:32,000
Microsoft refers to one or the other. So

139
00:05:30,320 --> 00:05:34,160
you can think of the Executive layer as

140
00:05:32,000 --> 00:05:37,840
the part that manages the processes, the

141
00:05:34,160 --> 00:05:41,199
threads, the system configuration or the

142
00:05:37,840 --> 00:05:43,840
I/Os in general. And the Kernel layer is

143
00:05:41,199 --> 00:05:46,400
more about abstracting the hardware

144
00:05:43,840 --> 00:05:47,919
layer and this is an example where

145
00:05:46,400 --> 00:05:49,680
knowing everything

146
00:05:47,919 --> 00:05:51,199
is not that important because I don't

147
00:05:49,680 --> 00:05:53,759
even remember what is the difference

148
00:05:51,199 --> 00:05:55,280
between a Control object or a Dispatch

149
00:05:53,759 --> 00:05:57,120
object. So

150
00:05:55,280 --> 00:05:59,600
it's like we know all this information

151
00:05:57,120 --> 00:06:02,479
exists somewhere and it's documented and

152
00:05:59,600 --> 00:06:04,080
you can read about all the internals but

153
00:06:02,479 --> 00:06:06,319
until you specifically need that

154
00:06:04,080 --> 00:06:08,319
information, it is not that important.

155
00:06:06,319 --> 00:06:10,880
It's more important to know that you

156
00:06:08,319 --> 00:06:13,120
need to dive into new concepts when you

157
00:06:10,880 --> 00:06:15,680
need them. So, there is something called

158
00:06:13,120 --> 00:06:17,199
the HAL, the Hardware Abstraction Layer,

159
00:06:15,680 --> 00:06:18,160
and you'll hear about it a lot when

160
00:06:17,199 --> 00:06:19,600
you're doing Windows kernel

161
00:06:18,160 --> 00:06:22,479
exploitation. But, basically every

162
00:06:19,600 --> 00:06:25,039
computer has specific hardware, things on

163
00:06:22,479 --> 00:06:27,120
the PCI bus or whatever that are

164
00:06:25,039 --> 00:06:29,440
different and they need to be abstracted

165
00:06:27,120 --> 00:06:32,080
somehow, so parts of the kernel can call

166
00:06:29,440 --> 00:06:34,479
fairly common APIs and there is

167
00:06:32,080 --> 00:06:36,800
this HAL layer that does all the

168
00:06:34,479 --> 00:06:38,639
hardware specific stuff. So,

169
00:06:36,800 --> 00:06:40,560
interestingly, the HAL is fairly well

170
00:06:38,639 --> 00:06:42,479
documented, and the main reason is

171
00:06:40,560 --> 00:06:45,120
because it's been abused for kernel

172
00:06:42,479 --> 00:06:47,440
exploitation, and so from a Windows kernel

173
00:06:45,120 --> 00:06:48,720
exploitation perspective, the HAL

174
00:06:47,440 --> 00:06:51,840
is basically

175
00:06:48,720 --> 00:06:54,319
dealing with a region of memory, and that

176
00:06:51,840 --> 00:06:55,680
region of memory contains a lot of

177
00:06:54,319 --> 00:06:58,319
interesting pointers, like function

178
00:06:55,680 --> 00:07:01,520
pointers, or pointers to interesting

179
00:06:58,319 --> 00:07:04,080
structures. And until recently, it was all

180
00:07:01,520 --> 00:07:06,080
at a static address, so typically exploit

181
00:07:04,080 --> 00:07:07,599
would abuse these structures.

182
00:07:06,080 --> 00:07:10,319
So if you're doing kernel exploitation,

183
00:07:07,599 --> 00:07:11,919
you can think of the HAL as a heap memory

184
00:07:10,319 --> 00:07:14,080
region that you might be able to abuse

185
00:07:11,919 --> 00:07:16,240
to get better primitives, but the actual

186
00:07:14,080 --> 00:07:18,400
HAL features and how it abstracts the

187
00:07:16,240 --> 00:07:20,560
hardware is generally not that

188
00:07:18,400 --> 00:07:22,240
interesting, and it is more like a rabbit

189
00:07:20,560 --> 00:07:25,840
hole, if you don't need to go that down

190
00:07:22,240 --> 00:07:27,680
that route. Win32k, is the main driver for

191
00:07:25,840 --> 00:07:29,440
handling all the Windowing functionality

192
00:07:27,680 --> 00:07:30,639
when you're interacting with the

193
00:07:29,440 --> 00:07:32,479
Desktop, it's probably the most

194
00:07:30,639 --> 00:07:34,240
complicated part of the Windows kernel

195
00:07:32,479 --> 00:07:36,240
which means it has probably the most

196
00:07:34,240 --> 00:07:38,319
vulnerabilities. And interestingly, over

197
00:07:36,240 --> 00:07:41,440
time, like thousands of vulnerabilities have

198
00:07:38,319 --> 00:07:43,599
been found in win32k.sys, and still

199
00:07:41,440 --> 00:07:46,000
people are finding bugs in that

200
00:07:43,599 --> 00:07:48,560
component these days, however,

201
00:07:46,000 --> 00:07:50,240
because it has been heavily targeted, a

202
00:07:48,560 --> 00:07:52,160
lot of the sandboxes

203
00:07:50,240 --> 00:07:54,720
implemented now

204
00:07:52,160 --> 00:07:57,199
lock it down and prevent you from

205
00:07:54,720 --> 00:08:01,039
calling the system calls that dispatch

206
00:07:57,199 --> 00:08:03,280
into win32k. So, for instance a browser

207
00:08:01,039 --> 00:08:04,720
sandbox like Chrome or Edge or Firefox

208
00:08:03,280 --> 00:08:07,680
or whatever, they would

209
00:08:04,720 --> 00:08:10,400
basically, at startup, they would, once all

210
00:08:07,680 --> 00:08:12,879
the GUI and window components are initialized,

211
00:08:10,400 --> 00:08:16,080
they will basically filter, so you can't

212
00:08:12,879 --> 00:08:18,479
call any of the win32k

213
00:08:16,080 --> 00:08:20,879
syscalls, or at least reduce the

214
00:08:18,479 --> 00:08:22,800
number to a really small number. So,

215
00:08:20,879 --> 00:08:25,520
interestingly even though you can still

216
00:08:22,800 --> 00:08:27,280
find bugs in win32k, you won't be able to

217
00:08:25,520 --> 00:08:29,440
use them in practice for like useful

218
00:08:27,280 --> 00:08:31,360
scenario with something like after you

219
00:08:29,440 --> 00:08:33,519
get code execution into a browser, and you

220
00:08:31,360 --> 00:08:35,519
want to target the Windows kernel to

221
00:08:33,519 --> 00:08:37,839
get out of the sandbox, you won't be able

222
00:08:35,519 --> 00:08:39,760
to target win32k anymore. So, you will

223
00:08:37,839 --> 00:08:41,839
have to target other components into the

224
00:08:39,760 --> 00:08:44,000
kernel. And this is the main reason why

225
00:08:41,839 --> 00:08:45,839
attackers won't look for bugs in win32k

226
00:08:44,000 --> 00:08:47,040
anymore. So, this is an example of you

227
00:08:45,839 --> 00:08:50,399
trying to interact with the Windows

228
00:08:47,040 --> 00:08:52,800
subsystem. So, for instance the win32k

229
00:08:50,399 --> 00:08:54,959
driver, by doing some Windowing

230
00:08:52,800 --> 00:08:57,440
functionality. And how it typically works

231
00:08:54,959 --> 00:09:00,240
is very similar to what we saw before

232
00:08:57,440 --> 00:09:03,120
for I/Os, but basically you would call the

233
00:09:00,240 --> 00:09:06,000
well-known documented API in

234
00:09:03,120 --> 00:09:08,480
user32, and trigger some system call

235
00:09:06,000 --> 00:09:11,279
wrapper from ntdll, and then it calls the

236
00:09:08,480 --> 00:09:12,880
actual instructions to trap into kernel

237
00:09:11,279 --> 00:09:15,839
mode. And the system call will be

238
00:09:12,880 --> 00:09:19,519
associated with the win32k driver itself,

239
00:09:15,839 --> 00:09:22,000
as win32k has its huge set

240
00:09:19,519 --> 00:09:24,720
of system calls that it supports and

241
00:09:22,000 --> 00:09:26,320
implements. And a lot of the time, which

242
00:09:24,720 --> 00:09:29,040
is kind of interesting and weird

243
00:09:26,320 --> 00:09:31,920
functionality, win32k.sys

244
00:09:29,040 --> 00:09:35,600
will issue this callbacks

245
00:09:31,920 --> 00:09:38,320
into userland DLLs which leads to 

246
00:09:35,600 --> 00:09:40,240
all sorts of crazy use-after-free bugs and

247
00:09:38,320 --> 00:09:42,320
problems, and

248
00:09:40,240 --> 00:09:44,160
it's kind of interesting and good to

249
00:09:42,320 --> 00:09:46,880
know because if you're actually

250
00:09:44,160 --> 00:09:48,720
debugging the Windows kernel, at first it

251
00:09:46,880 --> 00:09:51,360
can be a little bit confusing when you

252
00:09:48,720 --> 00:09:53,360
look at backtrace, and you see that it

253
00:09:51,360 --> 00:09:54,560
goes from user DLLs

254
00:09:53,360 --> 00:09:56,959
into

255
00:09:54,560 --> 00:09:59,760
win32k kernel drivers

256
00:09:56,959 --> 00:10:04,079
into the kernel and then back into

257
00:09:59,760 --> 00:10:04,079
calling into user DLLs again.

