1
00:00:00,399 --> 00:00:04,000
Okay, let's start Process Explorer.

2
00:00:10,880 --> 00:00:13,360
At first,

3
00:00:12,000 --> 00:00:15,679
you can see that

4
00:00:13,360 --> 00:00:16,480
not many columns are actually

5
00:00:15,679 --> 00:00:18,560
shown

6
00:00:16,480 --> 00:00:20,560
for all the different processes.

7
00:00:18,560 --> 00:00:22,160
So you can add them

8
00:00:20,560 --> 00:00:23,600
manually, for instance, adding the

9
00:00:22,160 --> 00:00:25,359
User Name,

10
00:00:23,600 --> 00:00:29,720
the Protection,

11
00:00:25,359 --> 00:00:29,720
Integrity Level, and Session.

12
00:00:39,040 --> 00:00:45,559
The next thing you want to do is to

13
00:00:40,879 --> 00:00:45,559
actually show the lower panel,

14
00:00:52,480 --> 00:00:57,199
so, for instance, we can see some

15
00:00:54,320 --> 00:01:01,280
processes are running on the system

16
00:00:57,199 --> 00:01:03,600
in session 0, they are system services.

17
00:01:01,280 --> 00:01:08,199
All the processes are running with our

18
00:01:03,600 --> 00:01:08,199
IEUser in session 1.

19
00:01:17,520 --> 00:01:21,759
For instance,

20
00:01:18,880 --> 00:01:24,479
cmd[.exe] here is running in session 1 with

21
00:01:21,759 --> 00:01:27,280
Medium integrity, Firefox is running as

22
00:01:24,479 --> 00:01:30,960
Medium for the main process,

23
00:01:27,280 --> 00:01:33,040
and then renderer processes are running

24
00:01:30,960 --> 00:01:35,680
and they're Low integrity level because

25
00:01:33,040 --> 00:01:37,360
they are running in the sandbox.

26
00:01:35,680 --> 00:01:41,640
Now let's look at the different threads

27
00:01:37,360 --> 00:01:41,640
for that particular Firefox process.

28
00:01:43,680 --> 00:01:47,360
We can see it has lots of threads.

29
00:01:51,680 --> 00:01:55,840
If we look at the lsass[.exe] one,

30
00:02:11,440 --> 00:02:15,040
we can see where they were started.

31
00:02:17,520 --> 00:02:22,800
Now, let's look at the different objects

32
00:02:19,360 --> 00:02:25,040
that are open and the HANDLEs.

33
00:02:22,800 --> 00:02:27,040
If you go in View,

34
00:02:25,040 --> 00:02:29,440
Lower Pane View, you can select either

35
00:02:27,040 --> 00:02:32,239
the DLLs or Handles.

36
00:02:29,440 --> 00:02:34,840
The DLLs will show the loaded DLLs in

37
00:02:32,239 --> 00:02:39,519
that particular process,

38
00:02:34,840 --> 00:02:39,519
so you want to select Handles.

39
00:02:39,840 --> 00:02:43,120
Here, you can select additional columns

40
00:02:41,440 --> 00:02:45,040
as well,

41
00:02:43,120 --> 00:02:48,360
to see the Handle Value

42
00:02:45,040 --> 00:02:48,360
or the [Access] Mask.

43
00:02:51,519 --> 00:02:56,080
So, we can see lsass has lots of

44
00:02:53,360 --> 00:02:57,680
different objects being open,

45
00:02:56,080 --> 00:02:58,879
we have Events,

46
00:02:57,680 --> 00:03:00,239
Directories,

47
00:02:58,879 --> 00:03:05,080
Files,

48
00:03:00,239 --> 00:03:05,080
Keys, which are the Registry keys,

49
00:03:05,680 --> 00:03:10,159
Processes,

50
00:03:07,680 --> 00:03:10,159
Mutants,

51
00:03:11,840 --> 00:03:14,840
Tokens.

