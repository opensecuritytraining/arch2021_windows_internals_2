﻿1
00:00:00,000 --> 00:00:05,040
Hi everyone, in this part, we're going to

2
00:00:02,580 --> 00:00:06,779
talk about processes and threads. More

3
00:00:05,040 --> 00:00:09,240
specifically, we're going to look into

4
00:00:06,779 --> 00:00:11,880
the structures that give information on

5
00:00:09,240 --> 00:00:13,980
processes and thread, both in userlandd

6
00:00:11,880 --> 00:00:17,220
and kernel land. Then, we're going to see

7
00:00:13,980 --> 00:00:19,140
how we access thread information as

8
00:00:17,220 --> 00:00:22,439
well as process information using

9
00:00:19,140 --> 00:00:25,019
a special register which is GS. Okay,

10
00:00:22,439 --> 00:00:27,720
let's get started! So, first, we're going

11
00:00:25,019 --> 00:00:30,599
to look into the processes both in user

12
00:00:27,720 --> 00:00:32,880
land with the PEB and in kernel land, with the

13
00:00:30,599 --> 00:00:35,100
_EPROCESS. So, in kernel memory the

14
00:00:32,880 --> 00:00:37,020
_EPROCESS structure is used to store a

15
00:00:35,100 --> 00:00:40,500
ton of information. It contains the

16
00:00:37,020 --> 00:00:42,420
_KPROCESS structure at index 0, it has a

17
00:00:40,500 --> 00:00:45,059
linked list of all the other processes

18
00:00:42,420 --> 00:00:47,100
on the system. It has a lock that can be

19
00:00:45,059 --> 00:00:48,899
used for actually modifying fields in

20
00:00:47,100 --> 00:00:50,700
the _EPROCESS structure itself so

21
00:00:48,899 --> 00:00:53,520
different threads don't do it at the

22
00:00:50,700 --> 00:00:56,640
same time. So, the main ones that you

23
00:00:53,520 --> 00:00:59,100
typically run into are the Pcb field

24
00:00:56,640 --> 00:01:01,140
which is the _KPROCESS itself, which has

25
00:00:59,100 --> 00:01:02,879
interesting data, then you have the

26
00:01:01,140 --> 00:01:04,439
linked list of the other processes on

27
00:01:02,879 --> 00:01:06,960
the system because, a lot of the time,

28
00:01:04,439 --> 00:01:08,460
what will happen is you'll find the

29
00:01:06,960 --> 00:01:10,860
address of the _EPROCESS structure in

30
00:01:08,460 --> 00:01:12,960
memory with some kind of kernel leak or

31
00:01:10,860 --> 00:01:15,479
something. But, let's say that you want to

32
00:01:12,960 --> 00:01:18,240
find a privileged process. So the idea is

33
00:01:15,479 --> 00:01:20,460
you can walk the actual ActiveProcessLinks

34
00:01:18,240 --> 00:01:24,180
to find other processes' structures.

35
00:01:20,460 --> 00:01:26,460
And so the [Unique]ProcessId, the PID, is useful,

36
00:01:24,180 --> 00:01:28,259
because the main threads of execution

37
00:01:26,460 --> 00:01:31,200
for the kernel are actually in the

38
00:01:28,259 --> 00:01:33,659
System process and it has the PID 4,

39
00:01:31,200 --> 00:01:35,880
which is static across all Windows

40
00:01:33,659 --> 00:01:37,439
versions. So, in general you don't know

41
00:01:35,880 --> 00:01:39,659
the PID for the other processes on the

42
00:01:37,439 --> 00:01:41,700
system but you always know the PID 4

43
00:01:39,659 --> 00:01:43,860
for the System process. So if you want to

44
00:01:41,700 --> 00:01:46,619
find other System tokens, you can just

45
00:01:43,860 --> 00:01:49,799
browse the _EPROCESS list, and look for the

46
00:01:46,619 --> 00:01:52,920
_EPROCESS that has a UniqueProcessId

47
00:01:49,799 --> 00:01:55,140
equal to 4, and then you get a Token

48
00:01:52,920 --> 00:01:57,240
pointer that is associated with the

49
00:01:55,140 --> 00:01:58,560
System process. And so the Token

50
00:01:57,240 --> 00:02:00,780
represents the actual privileges

51
00:01:58,560 --> 00:02:02,939
associated with that process. So, in user

52
00:02:00,780 --> 00:02:05,040
land, a process will typically have

53
00:02:02,939 --> 00:02:07,320
its own complicated structure that is

54
00:02:05,040 --> 00:02:10,319
not always super well documented by

55
00:02:07,320 --> 00:02:12,540
Microsoft, and it's called PEB for Process

56
00:02:10,319 --> 00:02:14,700
Environment Block. So, you can use it to

57
00:02:12,540 --> 00:02:16,860
introspect a lot of information about

58
00:02:14,700 --> 00:02:19,200
the userland process but it's not as

59
00:02:16,860 --> 00:02:21,540
complete as the kernel side. Typically, it

60
00:02:19,200 --> 00:02:24,360
holds information such as the list of

61
00:02:21,540 --> 00:02:26,879
loaded modules like user32.dll,

62
00:02:24,360 --> 00:02:29,040
Ntdll.dll, etc. And it will

63
00:02:26,879 --> 00:02:32,400
contain the Thread Local Storage also

64
00:02:29,040 --> 00:02:35,040
known as TLS which is thread specific

65
00:02:32,400 --> 00:02:37,260
data. And it will also hold where the

66
00:02:35,040 --> 00:02:39,599
different heaps start. So, you can find

67
00:02:37,260 --> 00:02:42,599
the head of the linked list of processes

68
00:02:39,599 --> 00:02:44,760
in the kernel with the PsActiveProcessHead

69
00:02:42,599 --> 00:02:47,940
symbol. And then you can dump the

70
00:02:44,760 --> 00:02:50,519
list of processes using the "dl"

71
00:02:47,940 --> 00:02:53,340
command, which stands for "display linked

72
00:02:50,519 --> 00:02:55,800
list". And then you could display all the

73
00:02:53,340 --> 00:02:58,980
different EPROCESS structures one by one

74
00:02:55,800 --> 00:03:00,840
using the "dt" command or directly using

75
00:02:58,980 --> 00:03:03,060
the "!process" command to display

76
00:03:00,840 --> 00:03:05,519
information about them. So, we use the

77
00:03:03,060 --> 00:03:07,080
PsIdleProcess symbol to get the _EPROCESS

78
00:03:05,519 --> 00:03:10,080
address associated with the idle

79
00:03:07,080 --> 00:03:11,940
process which is the process which does

80
00:03:10,080 --> 00:03:14,400
nothing. And then we display that

81
00:03:11,940 --> 00:03:16,140
_EPROCESS structure using using the "dt"

82
00:03:14,400 --> 00:03:18,120
command. We see that the idle process

83
00:03:16,140 --> 00:03:19,200
doesn't have an actual PID, which is

84
00:03:18,120 --> 00:03:21,239
interesting because all the other

85
00:03:19,200 --> 00:03:22,680
processes on the system do have one. So,

86
00:03:21,239 --> 00:03:24,599
here we do the same for the actual

87
00:03:22,680 --> 00:03:26,580
system process, and as I mentioned

88
00:03:24,599 --> 00:03:29,940
earlier, we can see it has the PID 4

89
00:03:26,580 --> 00:03:31,680
hardcoded. So, now let's let's look into

90
00:03:29,940 --> 00:03:33,299
the threads. As you can see, there are

91
00:03:31,680 --> 00:03:35,099
similar structures. So instead of the

92
00:03:33,299 --> 00:03:37,739
_EPROCESS, we have the _ETHREAD and

93
00:03:35,099 --> 00:03:39,780
instead of the _PEB we have the _TEB. So, in

94
00:03:37,739 --> 00:03:41,519
the _ETHREAD structure in the kernel,

95
00:03:39,780 --> 00:03:43,739
the start address can sometimes be

96
00:03:41,519 --> 00:03:46,200
useful, because you can see where in user

97
00:03:43,739 --> 00:03:47,940
land or in the kernel a given thread

98
00:03:46,200 --> 00:03:49,799
started executing, which can help

99
00:03:47,940 --> 00:03:52,260
sometimes with reversing. And then

100
00:03:49,799 --> 00:03:53,840
depending on what privileges a thread is

101
00:03:52,260 --> 00:03:56,640
running with, there will be Token

102
00:03:53,840 --> 00:03:58,560
impersonation associated with the thread

103
00:03:56,640 --> 00:04:00,299
itself that could be different from the

104
00:03:58,560 --> 00:04:02,640
process Token information, because

105
00:04:00,299 --> 00:04:04,920
different threads can be running with

106
00:04:02,640 --> 00:04:08,340
different privileges. And finally, inside

107
00:04:04,920 --> 00:04:11,099
the _KTHREAD.Tcb member, you'll find a

108
00:04:08,340 --> 00:04:13,019
pointer to the process owning the thread,

109
00:04:11,099 --> 00:04:15,720
which is useful to then locate other

110
00:04:13,019 --> 00:04:18,299
processes on the system for instance. So,

111
00:04:15,720 --> 00:04:20,100
in userland, the TEB, also known as

112
00:04:18,299 --> 00:04:22,740
Thread Environment Block, holds

113
00:04:20,100 --> 00:04:25,080
information for the thread that are

114
00:04:22,740 --> 00:04:27,000
accessible in userland. Typically it

115
00:04:25,080 --> 00:04:29,639
holds information such as the stack

116
00:04:27,000 --> 00:04:32,340
boundaries, since each thread has its

117
00:04:29,639 --> 00:04:34,620
own stack, and contains a pointer to the

118
00:04:32,340 --> 00:04:36,600
PEB for the process that owns that

119
00:04:34,620 --> 00:04:39,660
particular thread. So, in 64-bit on

120
00:04:36,600 --> 00:04:41,880
Windows, the GS segment selector in user

121
00:04:39,660 --> 00:04:44,940
land points to the TEB. And in kernel

122
00:04:41,880 --> 00:04:47,460
mode, the GS register will point to the

123
00:04:44,940 --> 00:04:49,500
specific kernel structure that tracks the

124
00:04:47,460 --> 00:04:51,360
process itself and that holds a bunch of

125
00:04:49,500 --> 00:04:53,639
input information about the process

126
00:04:51,360 --> 00:04:57,060
state, register states,

127
00:04:53,639 --> 00:05:00,240
etc. And that structure is called KPCR

128
00:04:57,060 --> 00:05:02,039
for Kernel Processor Control Region. The

129
00:05:00,240 --> 00:05:04,560
main point is that when you are

130
00:05:02,039 --> 00:05:07,259
reversing the Windows kernel, you'll often

131
00:05:04,560 --> 00:05:10,979
see something like "mov register"

132
00:05:07,259 --> 00:05:12,720
and then dereference "GS:offset". This

133
00:05:10,979 --> 00:05:15,360
basically represents an access to some

134
00:05:12,720 --> 00:05:18,419
given offset from the GS segment

135
00:05:15,360 --> 00:05:20,039
selector, and that instruction indicates

136
00:05:18,419 --> 00:05:21,840
to save that value into a temporary

137
00:05:20,039 --> 00:05:25,080
register. At first, this can be quite

138
00:05:21,840 --> 00:05:27,360
confusing, but if you know GS points to

139
00:05:25,080 --> 00:05:30,300
the actual KPCR structure, then you can

140
00:05:27,360 --> 00:05:32,699
see what offsets it accesses, and you can

141
00:05:30,300 --> 00:05:35,100
match them to the actual KPCR structure

142
00:05:32,699 --> 00:05:36,780
as documented by Vergilius, for instance,

143
00:05:35,100 --> 00:05:38,759
to know what other types it is

144
00:05:36,780 --> 00:05:41,280
retrieving. So, for instance because we

145
00:05:38,759 --> 00:05:46,039
can't really retrieve where GS points to,

146
00:05:41,280 --> 00:05:46,039
there is an actual field named "Self"

147
00:05:46,620 --> 00:05:52,199
and this "Self" field holds the address of

148
00:05:50,280 --> 00:05:54,419
where the KPCR is stored, which is

149
00:05:52,199 --> 00:05:56,639
effectively the address referenced by the

150
00:05:54,419 --> 00:05:58,199
GS segment selector. And this is useful

151
00:05:56,639 --> 00:06:00,780
when some other codes in the kernel

152
00:05:58,199 --> 00:06:02,699
expects to work on a pointer to a KPCR

153
00:06:00,780 --> 00:06:07,680
structure. Another very important

154
00:06:02,699 --> 00:06:09,840
field is the Pcrb, which type is KPCRB

155
00:06:07,680 --> 00:06:11,759
which stands for Kernel Processor

156
00:06:09,840 --> 00:06:13,520
Control Block. So, it basically

157
00:06:11,759 --> 00:06:16,380
represents some block information

158
00:06:13,520 --> 00:06:18,840
instead of region information. But

159
00:06:16,380 --> 00:06:21,360
basically, it is an inlined structure. So,

160
00:06:18,840 --> 00:06:23,880
you will end up with accesses inside

161
00:06:21,360 --> 00:06:26,940
that structure. So, for instance, for that

162
00:06:23,880 --> 00:06:27,919
Windows version where the Prcb is at

163
00:06:26,940 --> 00:06:30,720
offset 0x180,

164
00:06:27,919 --> 00:06:34,380
you'll end up with an access at

165
00:06:30,720 --> 00:06:36,300
different offsets with value above 0x180

166
00:06:34,380 --> 00:06:39,080
to access different fields inside the

167
00:06:36,300 --> 00:06:41,639
Prcb. So, for instance, we see that the

168
00:06:39,080 --> 00:06:44,220
KPRCB has a pointer to the current

169
00:06:41,639 --> 00:06:47,880
thread at offset 0x8.

170
00:06:44,220 --> 00:06:51,240
And we just saw that the KPRCB was at

171
00:06:47,880 --> 00:06:53,900
offset at 0x180 from the beginning of the

172
00:06:51,240 --> 00:06:53,900
KPCR.

173
00:06:54,180 --> 00:07:01,919
So, if we do a little bit of math we have

174
00:06:57,080 --> 00:07:04,199
0x180 + 0x8 = 0x188. So, basically to

175
00:07:01,919 --> 00:07:06,560
directly access the current thread we

176
00:07:04,199 --> 00:07:10,440
would basically have an access at offset

177
00:07:06,560 --> 00:07:12,539
0x188 from the GS register.

178
00:07:10,440 --> 00:07:15,479
So, here, we know it's actually retrieving

179
00:07:12,539 --> 00:07:19,259
the _KTHREAD pointer and then saving

180
00:07:15,479 --> 00:07:21,960
that into an offset on the stack.

181
00:07:19,259 --> 00:07:23,160
But, originally, if you see some code like

182
00:07:21,960 --> 00:07:25,680
this,

183
00:07:23,160 --> 00:07:28,440
or this, it can be a little bit confusing

184
00:07:25,680 --> 00:07:31,500
at first. But once you recall the GS

185
00:07:28,440 --> 00:07:33,240
selector point to the KPCR, everything is

186
00:07:31,500 --> 00:07:36,840
easy and you can find out what the code

187
00:07:33,240 --> 00:07:39,300
is doing. In practice IDA Pro, HexRays

188
00:07:36,840 --> 00:07:41,460
decompiler, is quite nice because it will

189
00:07:39,300 --> 00:07:43,919
actually show you a function call

190
00:07:41,460 --> 00:07:45,780
KeGetCurrentThread which is like a

191
00:07:43,919 --> 00:07:48,240
macro in the Windows kernel, instead of

192
00:07:45,780 --> 00:07:50,940
the GS access, and it will do that

193
00:07:48,240 --> 00:07:53,400
automatically. As far as I know, Ghidra is

194
00:07:50,940 --> 00:07:55,620
not as clever, and it won't allow to do

195
00:07:53,400 --> 00:07:58,020
that automatically, yet. This is why it's

196
00:07:55,620 --> 00:08:00,240
important to know what GS is pointing to.

197
00:07:58,020 --> 00:08:02,639
So, this is just a little diagram to show

198
00:08:00,240 --> 00:08:04,500
the relationships between structures. And

199
00:08:02,639 --> 00:08:07,199
remember, this is basically what we want

200
00:08:04,500 --> 00:08:09,180
to think about when we actually think

201
00:08:07,199 --> 00:08:10,440
about the kernel. The kernel is just a

202
00:08:09,180 --> 00:08:12,419
series of structures with different

203
00:08:10,440 --> 00:08:15,360
relationships. So, typically, you would

204
00:08:12,419 --> 00:08:17,400
have a KTHREAD for your given thread

205
00:08:15,360 --> 00:08:19,680
and somewhere inside that structure,

206
00:08:17,400 --> 00:08:21,720
there is an embedded KPROCESS pointer.

207
00:08:19,680 --> 00:08:23,639
And so we set KPROCESS and EPROCESS

208
00:08:21,720 --> 00:08:25,680
pointers are basically the same,

209
00:08:23,639 --> 00:08:28,379
since the _KPROCESS is the first member

210
00:08:25,680 --> 00:08:30,840
of the _EPROCESS structure, and so then

211
00:08:28,379 --> 00:08:33,240
the _EPROCESS holds a linked list of

212
00:08:30,840 --> 00:08:34,800
pointer to the _EPROCESS structures for

213
00:08:33,240 --> 00:08:36,919
all the other processes running on the

214
00:08:34,800 --> 00:08:36,919
system.

