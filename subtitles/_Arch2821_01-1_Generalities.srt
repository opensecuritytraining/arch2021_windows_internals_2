1
00:00:00,160 --> 00:00:04,799
So, in this part, we're going to

2
00:00:02,159 --> 00:00:07,600
increase our mental model, to understand

3
00:00:04,799 --> 00:00:09,280
how a kernel works, and more

4
00:00:07,600 --> 00:00:11,599
specifically, how the Windows kernel

5
00:00:09,280 --> 00:00:13,360
works. So, in order to have a good mental

6
00:00:11,599 --> 00:00:16,240
model, we need to understand core

7
00:00:13,360 --> 00:00:18,560
concepts, like how the Windows APIs are

8
00:00:16,240 --> 00:00:21,439
called, and more specifically how the

9
00:00:18,560 --> 00:00:23,439
syscalls are called in order to go from

10
00:00:21,439 --> 00:00:24,560
userland to kernel land. We're going to

11
00:00:23,439 --> 00:00:27,840
understand

12
00:00:24,560 --> 00:00:30,240
how the different objects are in memory.

13
00:00:27,840 --> 00:00:32,079
And when we talk about objects, they are

14
00:00:30,240 --> 00:00:34,399
not actually C++ objects, they're

15
00:00:32,079 --> 00:00:36,480
actually structures, but that's how they

16
00:00:34,399 --> 00:00:38,399
name their structure, they call them

17
00:00:36,480 --> 00:00:41,840
objects. So, we have objects like

18
00:00:38,399 --> 00:00:44,000
processes, threads, that are executing in

19
00:00:41,840 --> 00:00:46,559
memory. We're going to also see how the

20
00:00:44,000 --> 00:00:49,280
virtual memory works to understand how

21
00:00:46,559 --> 00:00:50,960
each process manages its own memory in

22
00:00:49,280 --> 00:00:52,960
userland, and how the memory is

23
00:00:50,960 --> 00:00:54,559
managed in kernel land. And finally, we're

24
00:00:52,960 --> 00:00:56,879
going to see what are the different

25
00:00:54,559 --> 00:00:58,800
sessions, how they manage different

26
00:00:56,879 --> 00:01:00,559
processes into sessions. Let's get

27
00:00:58,800 --> 00:01:02,800
started. So, if you want to define what a

28
00:01:00,559 --> 00:01:03,680
kernel is, basically

29
00:01:02,800 --> 00:01:04,720
it's

30
00:01:03,680 --> 00:01:06,880
mainly

31
00:01:04,720 --> 00:01:10,000
a piece of code that is running

32
00:01:06,880 --> 00:01:12,560
somewhere into memory and that goal is

33
00:01:10,000 --> 00:01:14,080
to actually manage the actual processes

34
00:01:12,560 --> 00:01:16,640
that are running on the system, and to

35
00:01:14,080 --> 00:01:18,960
make sure they have separated memory

36
00:01:16,640 --> 00:01:21,520
space, and to manage the actual

37
00:01:18,960 --> 00:01:24,720
privileges between these processes.

38
00:01:21,520 --> 00:01:26,640
People usually find kernel intimidating,

39
00:01:24,720 --> 00:01:29,520
because they think it's complex, but if

40
00:01:26,640 --> 00:01:31,759
you think of it as another userland

41
00:01:29,520 --> 00:01:34,720
process that actually does

42
00:01:31,759 --> 00:01:36,880
that work to enforce security barriers

43
00:01:34,720 --> 00:01:39,119
between processes, and schedule other

44
00:01:36,880 --> 00:01:41,439
processes, it's basically not very

45
00:01:39,119 --> 00:01:43,600
complex. For instance, it's actually

46
00:01:41,439 --> 00:01:45,840
simpler than a browser. And most of the

47
00:01:43,600 --> 00:01:48,079
time, you don't have to care about the

48
00:01:45,840 --> 00:01:49,759
complexity behind everything that the

49
00:01:48,079 --> 00:01:52,320
kernel is doing and you can just

50
00:01:49,759 --> 00:01:54,560
abstract it to what I've just

51
00:01:52,320 --> 00:01:56,079
said, to have a good mental model of what

52
00:01:54,560 --> 00:01:58,560
a kernel is doing. So, there are two

53
00:01:56,079 --> 00:02:00,240
interesting projects, if you want to

54
00:01:58,560 --> 00:02:02,399
understand the Windows internals.

55
00:02:00,240 --> 00:02:04,560
The first one is Vergilius project, this is

56
00:02:02,399 --> 00:02:06,240
basically a website that

57
00:02:04,560 --> 00:02:07,840
allows you to

58
00:02:06,240 --> 00:02:10,239
parse the different structures

59
00:02:07,840 --> 00:02:13,280
associated with Windows kernel structures

60
00:02:10,239 --> 00:02:15,040
and you can see relationships between

61
00:02:13,280 --> 00:02:17,360
the different fields of the structures

62
00:02:15,040 --> 00:02:20,879
and browse all the structures. The

63
00:02:17,360 --> 00:02:22,400
idea is the Windows kernel relies on many

64
00:02:20,879 --> 00:02:24,319
objects that are represented by

65
00:02:22,400 --> 00:02:26,959
structures, so you can parse them all and

66
00:02:24,319 --> 00:02:28,959
understand relationships. Also, it allows

67
00:02:26,959 --> 00:02:30,800
you to compare the different operating

68
00:02:28,959 --> 00:02:33,440
system versions, because the structures have

69
00:02:30,800 --> 00:02:36,239
evolved over time. Another interesting

70
00:02:33,440 --> 00:02:38,480
project is ReactOS. So basically, every

71
00:02:36,239 --> 00:02:41,120
time you do some reversing, you want to

72
00:02:38,480 --> 00:02:42,800
analyze new stuff, and that there are new

73
00:02:41,120 --> 00:02:43,599
stuff you don't know anything about, and

74
00:02:42,800 --> 00:02:45,920
so

75
00:02:43,599 --> 00:02:48,720
ReactOS is very nice, because it gives an

76
00:02:45,920 --> 00:02:51,519
approximation of lots of functions that

77
00:02:48,720 --> 00:02:55,200
have been reversed over time in Windows,

78
00:02:51,519 --> 00:02:57,440
and even though ReactOS is based on NT

79
00:02:55,200 --> 00:03:00,400
which is quite old now most of it is

80
00:02:57,440 --> 00:03:02,400
still valid for modern operating systems,

81
00:03:00,400 --> 00:03:04,239
like I would say 90%

82
00:03:02,400 --> 00:03:06,959
of the logic won't be different with

83
00:03:04,239 --> 00:03:09,120
like Windows 7 for instance. So,

84
00:03:06,959 --> 00:03:11,920
ReactOS is basically a

85
00:03:09,120 --> 00:03:13,680
re-implementation as open-source of the

86
00:03:11,920 --> 00:03:16,319
Windows logic, that has been reversed

87
00:03:13,680 --> 00:03:19,280
over time. And even if you don't rely on

88
00:03:16,319 --> 00:03:20,720
it for the actual decompiled functions,

89
00:03:19,280 --> 00:03:23,599
having all the

90
00:03:20,720 --> 00:03:26,640
names and the general logic is really

91
00:03:23,599 --> 00:03:30,560
helpful to reverse engineer new Windows

92
00:03:26,640 --> 00:03:32,879
versions. So, Windows is based on

93
00:03:30,560 --> 00:03:36,480
lots of functions and usually the logic

94
00:03:32,879 --> 00:03:39,120
can be thought as a core set of APIs

95
00:03:36,480 --> 00:03:41,519
that are used internally to do a bunch

96
00:03:39,120 --> 00:03:44,400
of stuff, and if you are familiar with

97
00:03:41,519 --> 00:03:47,280
Linux, you can think of that as something

98
00:03:44,400 --> 00:03:49,519
similar to libc on Linux. So, basically,

99
00:03:47,280 --> 00:03:53,200
you would have functions

100
00:03:49,519 --> 00:03:56,239
implemented in like ntdll.dll or kernelbase.dll

101
00:03:53,200 --> 00:03:58,480
or kernel32.dll like the general DLLs on

102
00:03:56,239 --> 00:04:00,400
Windows, that you that are publicly

103
00:03:58,480 --> 00:04:02,959
documented, and that you would call from

104
00:04:00,400 --> 00:04:04,480
userland so for instance CreateFile.

105
00:04:02,959 --> 00:04:06,400
And CreateFile would be

106
00:04:04,480 --> 00:04:08,959
wrapping

107
00:04:06,400 --> 00:04:11,439
a bunch of checks in userland

108
00:04:08,959 --> 00:04:13,439
and then it would call a syscall wrapper

109
00:04:11,439 --> 00:04:15,680
called, "NtCreateFile", another function

110
00:04:13,439 --> 00:04:19,280
in userland, and this function would

111
00:04:15,680 --> 00:04:21,680
basically call the syscall to then context

112
00:04:19,280 --> 00:04:23,919
switch to kernel mode. And so on the

113
00:04:21,680 --> 00:04:26,800
kernel side you would have the actual

114
00:04:23,919 --> 00:04:29,600
syscall with the same name so if you have

115
00:04:26,800 --> 00:04:31,360
an NtCreateFile in ntdll that is in

116
00:04:29,600 --> 00:04:33,440
userland, you would

117
00:04:31,360 --> 00:04:35,280
actually have an equivalent NtCreateFile

118
00:04:33,440 --> 00:04:36,800
in kernel land which is the syscall

119
00:04:35,280 --> 00:04:38,000
part called from userland. And so

120
00:04:36,800 --> 00:04:39,600
generally the logic, the way it's

121
00:04:38,000 --> 00:04:41,840
implemented into the kernel, is that the

122
00:04:39,600 --> 00:04:44,960
NtCreateFile will actually end up

123
00:04:41,840 --> 00:04:47,840
calling ZwCreateFile. And the idea

124
00:04:44,960 --> 00:04:50,880
behind that logic is that the kernel

125
00:04:47,840 --> 00:04:53,120
sometimes want to call ZwCreateFile

126
00:04:50,880 --> 00:04:54,960
from kernel mode, but you still want to

127
00:04:53,120 --> 00:04:58,000
be able to call it from user mode. So the

128
00:04:54,960 --> 00:05:00,320
idea is there will be some checks that

129
00:04:58,000 --> 00:05:02,080
are made to make sure everything is sane,

130
00:05:00,320 --> 00:05:04,400
due to the context switch from user to

131
00:05:02,080 --> 00:05:06,639
kernel and all these checks would be in

132
00:05:04,400 --> 00:05:09,199
NtCreateFile in kernel, but if the

133
00:05:06,639 --> 00:05:10,960
kernel actually calls the syscall from

134
00:05:09,199 --> 00:05:12,400
kernel mode it won't need to do all that

135
00:05:10,960 --> 00:05:14,800
kind of checks and it would

136
00:05:12,400 --> 00:05:17,120
just call ZwCreateFile. But at the end

137
00:05:14,800 --> 00:05:19,120
of the day, even if you call it from user

138
00:05:17,120 --> 00:05:21,280
mode, you would end up calling the actual

139
00:05:19,120 --> 00:05:23,520
syscall ZwCreateFile and it would just

140
00:05:21,280 --> 00:05:26,880
merge to the same code. But if you think of

141
00:05:23,520 --> 00:05:29,120
Windows APIs as actual functions you can

142
00:05:26,880 --> 00:05:32,160
call from a program, you can think of

143
00:05:29,120 --> 00:05:34,320
them as some parts implemented in user

144
00:05:32,160 --> 00:05:36,479
land and some other parts implemented in

145
00:05:34,320 --> 00:05:38,560
the kernel. The last thing you have in kernel

146
00:05:36,479 --> 00:05:40,880
is that you have APIs that you can't

147
00:05:38,560 --> 00:05:43,039
actually call from user mode and the

148
00:05:40,880 --> 00:05:45,199
idea is something like ExAllocatePoolWithTag

149
00:05:43,039 --> 00:05:47,840
is like a function to allocate

150
00:05:45,199 --> 00:05:49,840
memory in kernel so it's possible you

151
00:05:47,840 --> 00:05:52,240
call a function from user mode that will

152
00:05:49,840 --> 00:05:53,520
actually end up calling this ExAllocatePoolWithTag

153
00:05:52,240 --> 00:05:55,520
but that's going to be

154
00:05:53,520 --> 00:05:57,520
part of other function being called.

155
00:05:55,520 --> 00:05:59,759
You won't be actually able to call

156
00:05:57,520 --> 00:06:01,759
ExAllocatePoolWithTag as it is. It would

157
00:05:59,759 --> 00:06:03,600
just be a side effect of you calling

158
00:06:01,759 --> 00:06:06,080
something from user mode. So what is a

159
00:06:03,600 --> 00:06:08,319
process, so you can think of a process as

160
00:06:06,080 --> 00:06:11,039
some program with like a private address

161
00:06:08,319 --> 00:06:14,960
space enforced by the kernel so it has

162
00:06:11,039 --> 00:06:17,360
its own privileges and other processes can't

163
00:06:14,960 --> 00:06:20,400
interact with it in general. And so this

164
00:06:17,360 --> 00:06:23,440
process has an executable program

165
00:06:20,400 --> 00:06:26,400
when it started, and it also has a list

166
00:06:23,440 --> 00:06:28,160
of open HANDLEs to what we call system

167
00:06:26,400 --> 00:06:30,880
objects, which are basically

168
00:06:28,160 --> 00:06:33,440
structures into the kernel, and

169
00:06:30,880 --> 00:06:35,919
the process has an associated access

170
00:06:33,440 --> 00:06:38,240
Token which gives the permission for

171
00:06:35,919 --> 00:06:40,240
that particular process associated with

172
00:06:38,240 --> 00:06:42,800
the user, for instance, that is actually

173
00:06:40,240 --> 00:06:46,000
running the process. As you would expect,

174
00:06:42,800 --> 00:06:49,520
the process has a PID, like a process ID,

175
00:06:46,000 --> 00:06:52,479
which uniquely identifies it. And a process

176
00:06:49,520 --> 00:06:54,319
has at least one thread being executed,

177
00:06:52,479 --> 00:06:56,720
and the idea is the thread tracks the

178
00:06:54,319 --> 00:06:58,080
state of the process during the

179
00:06:56,720 --> 00:07:00,880
execution. And from the kernel's

180
00:06:58,080 --> 00:07:03,440
perspective, the process is tracked using

181
00:07:00,880 --> 00:07:05,840
an _EPROCESS structure,

182
00:07:03,440 --> 00:07:08,240
that you would typically find on

183
00:07:05,840 --> 00:07:10,080
Vergilius. So, this is the output of

184
00:07:08,240 --> 00:07:11,680
Process Explorer. So,

185
00:07:10,080 --> 00:07:13,440
I guess most of you are familiar with

186
00:07:11,680 --> 00:07:15,599
this kind of output, where you see the

187
00:07:13,440 --> 00:07:17,360
different processes running on the

188
00:07:15,599 --> 00:07:18,639
operating system, and so people have

189
00:07:17,360 --> 00:07:20,720
generally a good understanding of the

190
00:07:18,639 --> 00:07:23,039
userland part, but they think the kernel

191
00:07:20,720 --> 00:07:25,440
is too complicated, but actually the

192
00:07:23,039 --> 00:07:28,160
relation between what you see here, and

193
00:07:25,440 --> 00:07:30,560
what the kernel is doing, is really close,

194
00:07:28,160 --> 00:07:32,080
because you can think of the userland

195
00:07:30,560 --> 00:07:34,479
part

196
00:07:32,080 --> 00:07:36,960
as a shadow of the actual kernel

197
00:07:34,479 --> 00:07:39,360
structures. And so the kernel is tracking

198
00:07:36,960 --> 00:07:41,440
all the different processes

199
00:07:39,360 --> 00:07:43,440
using _EPROCESS structures, and what

200
00:07:41,440 --> 00:07:45,599
you're seeing in this slide is

201
00:07:43,440 --> 00:07:47,039
just some part of what the kernel is

202
00:07:45,599 --> 00:07:48,800
managing. And so, if you think of the

203
00:07:47,039 --> 00:07:51,120
kernel as something that just tracks

204
00:07:48,800 --> 00:07:53,440
kernel structures, it's relatively easy.

205
00:07:51,120 --> 00:07:55,919
So, the user mode world is largely

206
00:07:53,440 --> 00:07:57,599
just a shadow of kernel structures, and so

207
00:07:55,919 --> 00:07:59,520
the information you see in userland

208
00:07:57,599 --> 00:08:01,360
is just part of what the kernel is

209
00:07:59,520 --> 00:08:03,039
actually. And so you are exposed to lots

210
00:08:01,360 --> 00:08:05,199
of information in userland but,

211
00:08:03,039 --> 00:08:07,599
actually, this is just something that is

212
00:08:05,199 --> 00:08:09,520
also stored in kernel into structures,

213
00:08:07,599 --> 00:08:11,599
and when you are calling certain APIs,

214
00:08:09,520 --> 00:08:13,840
you are just pulling some information

215
00:08:11,599 --> 00:08:15,759
out from the structures, and getting all

216
00:08:13,840 --> 00:08:18,879
these information in userland. So, if

217
00:08:15,759 --> 00:08:20,879
you think of the user world as just this,

218
00:08:18,879 --> 00:08:22,160
it helps with the mental model. And so,

219
00:08:20,879 --> 00:08:23,680
when you're actually trying to

220
00:08:22,160 --> 00:08:25,520
understand what the kernel is doing,

221
00:08:23,680 --> 00:08:27,599
it's helpful to understand some of these

222
00:08:25,520 --> 00:08:29,599
structures. And, just understanding some

223
00:08:27,599 --> 00:08:31,759
of these structures is actually useful

224
00:08:29,599 --> 00:08:33,360
to get a very good view of what the kernel

225
00:08:31,759 --> 00:08:35,519
is, and generally, you don't need to

226
00:08:33,360 --> 00:08:36,959
understand all the relationships between

227
00:08:35,519 --> 00:08:38,800
all these different structures, just

228
00:08:36,959 --> 00:08:40,719
approaching certain structures is good

229
00:08:38,800 --> 00:08:43,440
enough. So, what is a thread? As we said

230
00:08:40,719 --> 00:08:45,680
earlier, any process has at least one

231
00:08:43,440 --> 00:08:48,480
thread of execution, and so a given

232
00:08:45,680 --> 00:08:50,480
thread always has an associated process.

233
00:08:48,480 --> 00:08:52,560
And a thread is effectively some

234
00:08:50,480 --> 00:08:55,839
structure in the kernel to track its

235
00:08:52,560 --> 00:08:58,080
context, and to be able to restore the

236
00:08:55,839 --> 00:09:00,480
state of this thread during scheduling

237
00:08:58,080 --> 00:09:03,200
by the kernel. And so a thread has two

238
00:09:00,480 --> 00:09:05,040
stacks obviously because for managing

239
00:09:03,200 --> 00:09:07,519
different privileges context to make

240
00:09:05,040 --> 00:09:09,920
sure we can't abuse the stack that would

241
00:09:07,519 --> 00:09:11,279
then be used in kernel when there is a

242
00:09:09,920 --> 00:09:14,000
context switch from userland to

243
00:09:11,279 --> 00:09:16,160
kernel, the thread will have its

244
00:09:14,000 --> 00:09:18,480
stack point to the actual kernel one.

245
00:09:16,160 --> 00:09:20,880
There is also an interesting structure

246
00:09:18,480 --> 00:09:23,360
called TLS for thread local storage. This

247
00:09:20,880 --> 00:09:25,200
is basically a private area related to a

248
00:09:23,360 --> 00:09:27,360
given thread it's not shared with any

249
00:09:25,200 --> 00:09:29,600
other threads. It's not necessarily

250
00:09:27,360 --> 00:09:31,839
always that you can encounter TLS during

251
00:09:29,600 --> 00:09:34,080
analysis, but it's good to know it exists.

252
00:09:31,839 --> 00:09:36,480
And so, any thread will have a thread

253
00:09:34,080 --> 00:09:38,959
ID associated with it. So, in the

254
00:09:36,480 --> 00:09:41,120
kernel a thread will be tracked by an

255
00:09:38,959 --> 00:09:42,959
_ETHREAD structure. And so most of the

256
00:09:41,120 --> 00:09:44,800
time when you're executing your program

257
00:09:42,959 --> 00:09:47,200
in userland, most of the time will be

258
00:09:44,800 --> 00:09:50,080
spent between your own code in userland.

259
00:09:47,200 --> 00:09:52,480
But if you call some APIs that

260
00:09:50,080 --> 00:09:55,600
end up doing a syscall, eventually, it will

261
00:09:52,480 --> 00:09:58,000
actually run some code in kernel mode for

262
00:09:55,600 --> 00:10:00,720
a little bit, before it comes back to the

263
00:09:58,000 --> 00:10:03,040
user mode. So, this is the view of the

264
00:10:00,720 --> 00:10:05,279
different threads in Process Explorer. As

265
00:10:03,040 --> 00:10:06,800
you can see, each one has a different

266
00:10:05,279 --> 00:10:09,120
thread ID, and we can see the start

267
00:10:06,800 --> 00:10:12,000
address of the thread. So, virtual memory

268
00:10:09,120 --> 00:10:15,839
is just a way of abstracting access to

269
00:10:12,000 --> 00:10:18,160
physical RAM or memory map files using

270
00:10:15,839 --> 00:10:20,800
some kind of linear address space per

271
00:10:18,160 --> 00:10:23,440
process. Because it would be too chaotic

272
00:10:20,800 --> 00:10:24,560
to use physical addresses, and so

273
00:10:23,440 --> 00:10:25,760
basically

274
00:10:24,560 --> 00:10:28,800
one can be

275
00:10:25,760 --> 00:10:29,680
virtualized is either the RAM, like the

276
00:10:28,800 --> 00:10:32,000
actual

277
00:10:29,680 --> 00:10:33,360
fast memory, the memory mapped files

278
00:10:32,000 --> 00:10:35,760
which are

279
00:10:33,360 --> 00:10:39,040
files on disk, that are actually mapped

280
00:10:35,760 --> 00:10:40,000
in memory for fast lookup and change, and

281
00:10:39,040 --> 00:10:42,079
then

282
00:10:40,000 --> 00:10:44,959
some swap file, which is basically if

283
00:10:42,079 --> 00:10:47,279
there isn't any enough RAM, then the

284
00:10:44,959 --> 00:10:50,240
disk will actually be used and a file on

285
00:10:47,279 --> 00:10:52,800
disk will actually be used to get more

286
00:10:50,240 --> 00:10:55,279
accessible memory. And so userland

287
00:10:52,800 --> 00:10:57,680
processes like to have a consistent

288
00:10:55,279 --> 00:10:59,360
environment to work with, which is why

289
00:10:57,680 --> 00:11:01,200
virtual memory is used. So in

290
00:10:59,360 --> 00:11:04,399
practice, a lot of virtual memory

291
00:11:01,200 --> 00:11:07,200
technically is there but it might not be

292
00:11:04,399 --> 00:11:09,120
backed by anything in the moment, and so

293
00:11:07,200 --> 00:11:12,000
virtual memory is just an abstracted

294
00:11:09,120 --> 00:11:14,399
idea, and there is not actually memory

295
00:11:12,000 --> 00:11:17,519
there sometimes, when it's unmapped. So, if

296
00:11:14,399 --> 00:11:20,000
you're used to 32-bit, the main thing

297
00:11:17,519 --> 00:11:22,480
that might come to a surprise is that, on

298
00:11:20,000 --> 00:11:25,600
64-bit, they don't actually use the full

299
00:11:22,480 --> 00:11:29,600
64-bit address space. And so everyone is

300
00:11:25,600 --> 00:11:32,560
for now using the 48-bit addresses, known

301
00:11:29,600 --> 00:11:35,040
as canonical address ranges. And so,

302
00:11:32,560 --> 00:11:37,200
it's like a standardized range. And so, on

303
00:11:35,040 --> 00:11:39,279
32-bit you would be looking at the lower

304
00:11:37,200 --> 00:11:41,519
two gigabytes of address space for user

305
00:11:39,279 --> 00:11:43,279
space, and two [upper] gigabytes of address space

306
00:11:41,519 --> 00:11:46,000
for kernel space, which is easy to

307
00:11:43,279 --> 00:11:48,720
remember. But on 64-bit, we are dealing

308
00:11:46,000 --> 00:11:50,560
with a slightly more confusing addresses.

309
00:11:48,720 --> 00:11:53,760
But the easy way

310
00:11:50,560 --> 00:11:56,399
to know if the upper bits are set, then

311
00:11:53,760 --> 00:11:58,079
it's actually the kernel address space.

312
00:11:56,399 --> 00:12:01,040
So if these bits are set, it is the kernel

313
00:11:58,079 --> 00:12:04,320
address space. And if you see all zeros,

314
00:12:01,040 --> 00:12:07,120
it's userspace, which is kind of useful

315
00:12:04,320 --> 00:12:09,120
for instance if you're looking for kernel

316
00:12:07,120 --> 00:12:10,959
addresses leaking to userland, you can

317
00:12:09,120 --> 00:12:12,399
just look at your process memory, and

318
00:12:10,959 --> 00:12:14,399
look for bytes

319
00:12:12,399 --> 00:12:17,760
values starting with like that.

320
00:12:14,399 --> 00:12:21,360
FFF in 64-bit values, which is kind of

321
00:12:17,760 --> 00:12:24,560
how people like Alex Ionescu or j00ru

322
00:12:21,360 --> 00:12:27,360
find such vulnerabilities. And so, most

323
00:12:24,560 --> 00:12:32,000
memory in general is tracked by

324
00:12:27,360 --> 00:12:34,959
4K chunks, called pages, and each page has

325
00:12:32,000 --> 00:12:37,920
memory protections typically read, write,

326
00:12:34,959 --> 00:12:40,160
execute, which dictates what you can do

327
00:12:37,920 --> 00:12:42,959
with these pages and sometimes

328
00:12:40,160 --> 00:12:45,279
you have large pages of one megabyte. So,

329
00:12:42,959 --> 00:12:47,200
in this screenshot, we can see

330
00:12:45,279 --> 00:12:49,760
two different

331
00:12:47,200 --> 00:12:52,399
executables that are actually debugged

332
00:12:49,760 --> 00:12:54,320
with x64dbg, and so when you are looking

333
00:12:52,399 --> 00:12:56,800
at processes under a debugger, typically in

334
00:12:54,320 --> 00:12:59,920
userland, you'll notice they have the

335
00:12:56,800 --> 00:13:02,000
same exact address spaces, but obviously

336
00:12:59,920 --> 00:13:03,920
these processes can't access the other

337
00:13:02,000 --> 00:13:06,399
process memory, so this is just an

338
00:13:03,920 --> 00:13:08,399
abstract view of physical memory. And so,

339
00:13:06,399 --> 00:13:10,959
all these addresses are managed by kernel

340
00:13:08,399 --> 00:13:13,680
structures that are just page tables. And

341
00:13:10,959 --> 00:13:15,200
again, people find these confusing

342
00:13:13,680 --> 00:13:17,360
but again they are just structures that

343
00:13:15,200 --> 00:13:20,720
are very well documented. So, this is

344
00:13:17,360 --> 00:13:22,160
a system internals tool called VMMap,

345
00:13:20,720 --> 00:13:25,120
where you can basically see the

346
00:13:22,160 --> 00:13:27,519
different kind of memory, but for

347
00:13:25,120 --> 00:13:30,560
here, what we're interested in, are things

348
00:13:27,519 --> 00:13:32,240
like mapped files, that you can see are

349
00:13:30,560 --> 00:13:35,760
referenced, but in general it doesn't

350
00:13:32,240 --> 00:13:38,160
help much with what the kernel memory is

351
00:13:35,760 --> 00:13:39,199
because it's all userland here.

352
00:13:38,160 --> 00:13:41,199
So,

353
00:13:39,199 --> 00:13:43,040
we said it already, two different

354
00:13:41,199 --> 00:13:44,000
processes use the same user address

355
00:13:43,040 --> 00:13:45,760
range

356
00:13:44,000 --> 00:13:48,959
and the illusion is created by the

357
00:13:45,760 --> 00:13:51,920
kernel. But, there is a recent thing in

358
00:13:48,959 --> 00:13:55,519
Windows, since the Meltdown and Spectre

359
00:13:51,920 --> 00:13:57,920
patches were added, and you may see it in

360
00:13:55,519 --> 00:14:00,560
WinDbg when you see function

361
00:13:57,920 --> 00:14:02,320
names containing the term "shadow" when

362
00:14:00,560 --> 00:14:04,320
for instance you look at the syscalls

363
00:14:02,320 --> 00:14:06,160
entry points. And the reason for that, is

364
00:14:04,320 --> 00:14:08,639
because now, there are actually multiple

365
00:14:06,160 --> 00:14:11,920
set of page tables and so

366
00:14:08,639 --> 00:14:14,399
userland has a set of page tables,

367
00:14:11,920 --> 00:14:17,279
and the kernel addresses associated with

368
00:14:14,399 --> 00:14:20,079
this page tables are very limited,

369
00:14:17,279 --> 00:14:22,560
whereas before, all of the kernel address

370
00:14:20,079 --> 00:14:25,600
space was accessible through a process

371
00:14:22,560 --> 00:14:27,839
userland page tables, it is just that when

372
00:14:25,600 --> 00:14:30,160
you were executing in userland, you

373
00:14:27,839 --> 00:14:31,920
would not have permissions to access

374
00:14:30,160 --> 00:14:34,800
them, so you wouldn't be able to access

375
00:14:31,920 --> 00:14:37,920
the kernel address space. But this led to

376
00:14:34,800 --> 00:14:40,480
side channel attacks through speculative

377
00:14:37,920 --> 00:14:42,800
execution, with things like Meltdown and

378
00:14:40,480 --> 00:14:44,320
Spectre. But what they do now, is that

379
00:14:42,800 --> 00:14:47,040
they have a limited number of page

380
00:14:44,320 --> 00:14:49,680
tables loaded when the thread is in

381
00:14:47,040 --> 00:14:52,079
user mode, which means kernel addresses

382
00:14:49,680 --> 00:14:54,399
can't be accessed because page table

383
00:14:52,079 --> 00:14:57,279
entries don't exist at all which avoids

384
00:14:54,399 --> 00:14:59,519
the Meltdown and Spectre problems. Okay so

385
00:14:57,279 --> 00:15:01,920
the concept of sessions.

386
00:14:59,519 --> 00:15:04,079
So basically when a user logs in, the

387
00:15:01,920 --> 00:15:05,920
user will have a session, and all the

388
00:15:04,079 --> 00:15:08,959
processes will be

389
00:15:05,920 --> 00:15:11,040
associated with that session and so

390
00:15:08,959 --> 00:15:13,440
typically these processes can't interact

391
00:15:11,040 --> 00:15:16,399
with other processes in other sessions,

392
00:15:13,440 --> 00:15:18,880
and the Windows kernel itself can

393
00:15:16,399 --> 00:15:21,360
introspect all the sessions and do

394
00:15:18,880 --> 00:15:24,000
whatever it wants, and typically you have

395
00:15:21,360 --> 00:15:26,720
a special session which is

396
00:15:24,000 --> 00:15:28,639
session 0, which is reserved for System

397
00:15:26,720 --> 00:15:30,160
services. So,

398
00:15:28,639 --> 00:15:31,920
when you think of the relationship

399
00:15:30,160 --> 00:15:34,000
between userland and kernel, and you

400
00:15:31,920 --> 00:15:36,480
start thinking of the kernel as a

401
00:15:34,000 --> 00:15:38,720
relatively simple program that manages

402
00:15:36,480 --> 00:15:40,399
userland, you can basically think of

403
00:15:38,720 --> 00:15:43,360
the hypervisor

404
00:15:40,399 --> 00:15:45,839
as a relatively simple program that

405
00:15:43,360 --> 00:15:48,160
manages the kernel. It is a very similar

406
00:15:45,839 --> 00:15:50,240
mental model, but with a slightly

407
00:15:48,160 --> 00:15:52,079
different level of abstraction, and so

408
00:15:50,240 --> 00:15:54,560
the main reason why Windows kernel

409
00:15:52,079 --> 00:15:58,000
exploitation is useful for attackers is

410
00:15:54,560 --> 00:16:00,000
that the kernel has read/write access to

411
00:15:58,000 --> 00:16:02,160
all userland, like it is able to

412
00:16:00,000 --> 00:16:05,120
access all processes,

413
00:16:02,160 --> 00:16:07,839
and most CPU instructions.

414
00:16:05,120 --> 00:16:09,519
So, if you get kernel privileges, you can

415
00:16:07,839 --> 00:16:12,000
basically do whatever you want on the

416
00:16:09,519 --> 00:16:14,160
system, aside from the hypervisor, if

417
00:16:12,000 --> 00:16:16,160
there is one. The only thing that is

418
00:16:14,160 --> 00:16:18,000
worth mentioning that we'll detail later

419
00:16:16,160 --> 00:16:19,440
in mitigation, is that the kernel

420
00:16:18,000 --> 00:16:22,639
typically can't

421
00:16:19,440 --> 00:16:25,120
execute code in userland, due to the

422
00:16:22,639 --> 00:16:28,320
SMEP mitigation, and that even though, by

423
00:16:25,120 --> 00:16:32,560
default, a user process

424
00:16:28,320 --> 00:16:35,519
can't access another process user space,

425
00:16:32,560 --> 00:16:38,079
obviously the kernel allows to do so by

426
00:16:35,519 --> 00:16:38,880
using like Windows APIs and going

427
00:16:38,079 --> 00:16:40,880
through

428
00:16:38,880 --> 00:16:43,600
kernel services. So, if you think about

429
00:16:40,880 --> 00:16:46,720
the kernel as a bunch of objects,

430
00:16:43,600 --> 00:16:49,600
Microsoft wants people to think about

431
00:16:46,720 --> 00:16:52,160
objects are opaque structures, because

432
00:16:49,600 --> 00:16:53,920
you just call APIs that deal with these

433
00:16:52,160 --> 00:16:56,560
object, and you don't really need to

434
00:16:53,920 --> 00:16:59,360
understand what is done internally, but

435
00:16:56,560 --> 00:17:01,519
actually, there are exported symbols that

436
00:16:59,360 --> 00:17:03,440
you can have when debugging. So you can

437
00:17:01,519 --> 00:17:06,799
actually see most of these objects'

438
00:17:03,440 --> 00:17:08,160
internals in practice. So, in memory

439
00:17:06,799 --> 00:17:11,120
when you allocate

440
00:17:08,160 --> 00:17:12,319
a typical object type, like a process or

441
00:17:11,120 --> 00:17:14,720
thread, or

442
00:17:12,319 --> 00:17:15,839
a file, in memory the kernel will

443
00:17:14,720 --> 00:17:18,640
actually

444
00:17:15,839 --> 00:17:20,640
allocate an Object structure

445
00:17:18,640 --> 00:17:22,480
in front or the actual

446
00:17:20,640 --> 00:17:24,319
object type you're allocating, so you

447
00:17:22,480 --> 00:17:26,480
would typically have an Object structure

448
00:17:24,319 --> 00:17:28,319
in front of an _ETHREAD structure, for

449
00:17:26,480 --> 00:17:30,880
instance, if you're tracking a thread.

450
00:17:28,319 --> 00:17:33,360
So, the kernel can track specifics like

451
00:17:30,880 --> 00:17:36,080
what process that object is associated

452
00:17:33,360 --> 00:17:37,600
with, it can track memory usage, and stuff

453
00:17:36,080 --> 00:17:39,120
like that. So you have this

454
00:17:37,600 --> 00:17:41,600
sysinternals

455
00:17:39,120 --> 00:17:43,520
tool, called WinObj, which basically

456
00:17:41,600 --> 00:17:46,240
allows you to list all the different

457
00:17:43,520 --> 00:17:49,200
object types that exist, and so typically

458
00:17:46,240 --> 00:17:51,840
you would see the Process type,

459
00:17:49,200 --> 00:17:53,919
the Thread type, or the Token type, we

460
00:17:51,840 --> 00:17:54,720
talked about already, but you can also

461
00:17:53,919 --> 00:17:57,919
see

462
00:17:54,720 --> 00:18:02,080
more fancy types like TmEn,

463
00:17:57,919 --> 00:18:04,799
TmRm, Tm-whatever, which are basically

464
00:18:02,080 --> 00:18:08,720
Transaction Manager-related objects, and

465
00:18:04,799 --> 00:18:10,480
so you wouldn't use this WinObj tool

466
00:18:08,720 --> 00:18:12,480
all the time, but it's nice so you can

467
00:18:10,480 --> 00:18:14,799
list all the different object types that

468
00:18:12,480 --> 00:18:16,799
exist. So, most people that do userland

469
00:18:14,799 --> 00:18:19,440
stuff are more familiar with like,

470
00:18:16,799 --> 00:18:21,440
HANDLEs, it's basically the same as

471
00:18:19,440 --> 00:18:22,559
file descriptors on Linux, and so

472
00:18:21,440 --> 00:18:25,679
typically

473
00:18:22,559 --> 00:18:27,679
userland can't know the addresses of the

474
00:18:25,679 --> 00:18:30,400
objects in kernel memory. So, they are

475
00:18:27,679 --> 00:18:32,400
abstracted through a per process HANDLE

476
00:18:30,400 --> 00:18:35,360
table, and so, the userland

477
00:18:32,400 --> 00:18:38,400
process gets an identifier, like an

478
00:18:35,360 --> 00:18:40,400
integer, or also known as HANDLE,

479
00:18:38,400 --> 00:18:42,400
associated with the object that it is

480
00:18:40,400 --> 00:18:46,160
dealing with. And so, when it wants to

481
00:18:42,400 --> 00:18:49,360
call certain APIs, it gives this

482
00:18:46,160 --> 00:18:51,200
identifier to the kernel,

483
00:18:49,360 --> 00:18:53,600
and the kernel knows,

484
00:18:51,200 --> 00:18:55,919
like does the translation to actually

485
00:18:53,600 --> 00:18:56,720
access the object that corresponds to

486
00:18:55,919 --> 00:18:58,960
that

487
00:18:56,720 --> 00:19:00,960
HANDLE. So, what ends up happening, is

488
00:18:58,960 --> 00:19:02,880
you can have one object on the kernel

489
00:19:00,960 --> 00:19:04,400
side that is actually accessed by many

490
00:19:02,880 --> 00:19:06,720
processes in userland, and the

491
00:19:04,400 --> 00:19:08,720
processes will have their own HANDLE to

492
00:19:06,720 --> 00:19:11,200
access that object, and the HANDLEs will

493
00:19:08,720 --> 00:19:14,320
typically have their own permissions

494
00:19:11,200 --> 00:19:16,400
associated with the process. So,

495
00:19:14,320 --> 00:19:19,360
that will actually dictate what the

496
00:19:16,400 --> 00:19:20,880
process can do with that particular object.

497
00:19:19,360 --> 00:19:23,760
So, the kernel can

498
00:19:20,880 --> 00:19:25,679
either allow it or deny it. So,

499
00:19:23,760 --> 00:19:29,520
because of the way it

500
00:19:25,679 --> 00:19:32,320
works, the kernel basically tracks

501
00:19:29,520 --> 00:19:34,240
how many times an object is accessed

502
00:19:32,320 --> 00:19:36,880
with like reference counting, and it

503
00:19:34,240 --> 00:19:40,480
bumps a ref count each time a new

504
00:19:36,880 --> 00:19:42,799
process accesses the same object, and so

505
00:19:40,480 --> 00:19:44,799
then the kernel can actually delete the

506
00:19:42,799 --> 00:19:47,200
object when no one else is actually

507
00:19:44,799 --> 00:19:49,840
using it anymore. So, typically

508
00:19:47,200 --> 00:19:52,160
if you see functions like ObOpenObjectByName

509
00:19:49,840 --> 00:19:53,919
or ObOpenObjectByPointer, you don't need to

510
00:19:52,160 --> 00:19:56,320
know internally what they do, but

511
00:19:53,919 --> 00:19:58,240
basically when you're reversing, all you

512
00:19:56,320 --> 00:20:00,960
have to understand is that some HANDLE

513
00:19:58,240 --> 00:20:03,039
is created for an object, and the HANDLE

514
00:20:00,960 --> 00:20:05,360
is probably going to be returned to the

515
00:20:03,039 --> 00:20:07,440
caller, and potentially to userland at

516
00:20:05,360 --> 00:20:10,480
some point, so, it can be

517
00:20:07,440 --> 00:20:13,440
reused by the program later. And so this

518
00:20:10,480 --> 00:20:16,480
is Process Explorer so you can see

519
00:20:13,440 --> 00:20:19,919
for a given process, in this case lsass,

520
00:20:16,480 --> 00:20:22,640
you can see all the open HANDLEs, and the

521
00:20:19,919 --> 00:20:24,400
actual object types associated with these

522
00:20:22,640 --> 00:20:27,039
open HANDLEs, all of that for a given

523
00:20:24,400 --> 00:20:29,520
process. So we can see process

524
00:20:27,039 --> 00:20:33,360
types, thread types, Token types and the

525
00:20:29,520 --> 00:20:36,840
HANDLEs would be like 1884, 7216,

526
00:20:33,360 --> 00:20:36,840
etc.

