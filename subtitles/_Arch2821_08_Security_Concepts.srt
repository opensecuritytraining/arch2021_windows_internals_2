1
00:00:00,120 --> 00:00:06,180
Hello, in this video, we're going to look

2
00:00:03,480 --> 00:00:09,179
at all the concepts related to how

3
00:00:06,180 --> 00:00:11,940
different processes are either being

4
00:00:09,179 --> 00:00:13,799
allowed or denied to access certain

5
00:00:11,940 --> 00:00:16,680
objects. We are going to talk about

6
00:00:13,799 --> 00:00:18,539
tokens associated with threads and processes,

7
00:00:16,680 --> 00:00:21,240
as well as security descriptors

8
00:00:18,539 --> 00:00:23,760
associated with objects, and all the

9
00:00:21,240 --> 00:00:26,939
access control lists and privileges

10
00:00:23,760 --> 00:00:29,580
around these concepts. Okay, let's get

11
00:00:26,939 --> 00:00:31,560
started. In general, most permissions on

12
00:00:29,580 --> 00:00:34,980
Windows are based around security

13
00:00:31,560 --> 00:00:38,219
properties associated with tokens and

14
00:00:34,980 --> 00:00:41,579
objects' security descriptors. And so, each

15
00:00:38,219 --> 00:00:44,520
thread has a default Token that dictates

16
00:00:41,579 --> 00:00:47,820
what it can do or not do on the system.

17
00:00:44,520 --> 00:00:50,340
And each object on the system has an

18
00:00:47,820 --> 00:00:53,579
associated default security descriptor

19
00:00:50,340 --> 00:00:55,980
that dictates the type of action that

20
00:00:53,579 --> 00:00:58,320
can be done by other threads on that

21
00:00:55,980 --> 00:01:00,600
particular object. And so, typically a

22
00:00:58,320 --> 00:01:02,820
thread that creates particular objects

23
00:01:00,600 --> 00:01:04,860
will be able to manipulate these objects,

24
00:01:02,820 --> 00:01:07,020
but won't be able to access certain

25
00:01:04,860 --> 00:01:09,060
objects that are created by other

26
00:01:07,020 --> 00:01:11,100
threads. And so the component, that is

27
00:01:09,060 --> 00:01:13,979
responsible to check that thread only

28
00:01:11,100 --> 00:01:15,900
access the objects that they are allowed

29
00:01:13,979 --> 00:01:18,420
to access, is called the security

30
00:01:15,900 --> 00:01:20,820
reference monitor. Typically, the access

31
00:01:18,420 --> 00:01:23,520
check will happen when calling

32
00:01:20,820 --> 00:01:26,520
functions like CreateProcess,

33
00:01:23,520 --> 00:01:29,759
CreateFile that are called when you try to

34
00:01:26,520 --> 00:01:32,159
open a specific object, before you can do

35
00:01:29,759 --> 00:01:35,040
anything with it. And so the access check

36
00:01:32,159 --> 00:01:38,100
is done at that time. And so, now we're

37
00:01:35,040 --> 00:01:40,820
going to detail a bit more tokens owned

38
00:01:38,100 --> 00:01:44,280
by threads and security descriptors

39
00:01:40,820 --> 00:01:47,460
associated with objects. WinDbg includes

40
00:01:44,280 --> 00:01:49,860
the !token command that can be used

41
00:01:47,460 --> 00:01:52,020
to analyze Token properties associated

42
00:01:49,860 --> 00:01:55,320
with your current thread. We can see the

43
00:01:52,020 --> 00:01:57,600
Token holds information on the user and

44
00:01:55,320 --> 00:01:59,759
group associated with the process. And

45
00:01:57,600 --> 00:02:02,040
we'll see later that the information is

46
00:01:59,759 --> 00:02:06,000
stored in what we call a security

47
00:02:02,040 --> 00:02:12,560
identifier and it is of the format S-,

48
00:02:06,000 --> 00:02:12,560
so for instance here we have S-

49
00:02:13,160 --> 00:02:17,959
1-5-32-544 for the actual user group, and

50
00:02:16,140 --> 00:02:21,000
we have
,
51
00:02:17,959 --> 00:02:22,800
S-1-5.18 for the user. And we can see on

52
00:02:21,000 --> 00:02:25,739
top of that that there are some

53
00:02:22,800 --> 00:02:28,560
privileges that give you the ability to

54
00:02:25,739 --> 00:02:31,500
create new tokens, assign tokens to other

55
00:02:28,560 --> 00:02:34,920
processes and so on. Basically, all the

56
00:02:31,500 --> 00:02:37,319
Token is, is a kernel structure, the same

57
00:02:34,920 --> 00:02:39,900
as everything else in the kernel, and it

58
00:02:37,319 --> 00:02:42,660
is managed by the Object Manager. And so

59
00:02:39,900 --> 00:02:44,879
here, the Token kernel structure will be

60
00:02:42,660 --> 00:02:47,480
associated with the _EPROCESS which

61
00:02:44,879 --> 00:02:50,220
address ends with

62
00:02:47,480 --> 00:02:52,739
701c0 as you can see at the very top of

63
00:02:50,220 --> 00:02:55,379
the output. And the Token structure just

64
00:02:52,739 --> 00:02:58,500
defines a bunch of privileges that

65
00:02:55,379 --> 00:03:01,019
processes can have. So you can see that a

66
00:02:58,500 --> 00:03:03,660
bunch of it is actually snipped in the

67
00:03:01,019 --> 00:03:07,379
output. But there are like 36 different

68
00:03:03,660 --> 00:03:10,019
special privileges that a Token can sort of

69
00:03:07,379 --> 00:03:12,180
have in the current Windows 10 version I

70
00:03:10,019 --> 00:03:14,700
am analyzing. And so the highest

71
00:03:12,180 --> 00:03:17,519
privileged Token is usually what we call

72
00:03:14,700 --> 00:03:20,519
the System Token. And generally, a System

73
00:03:17,519 --> 00:03:22,560
level Token either has all of these

74
00:03:20,519 --> 00:03:26,099
permissions, or it has a special

75
00:03:22,560 --> 00:03:29,040
permission that says it can adjust Token

76
00:03:26,099 --> 00:03:31,500
privileges. So even if your Token

77
00:03:29,040 --> 00:03:33,599
doesn't say that you can maybe debug

78
00:03:31,500 --> 00:03:37,379
another process, if you have the

79
00:03:33,599 --> 00:03:40,379
privilege that says you can add your own

80
00:03:37,379 --> 00:03:43,200
privileges, which System Token typically

81
00:03:40,379 --> 00:03:45,659
will, then it is just kind of game over

82
00:03:43,200 --> 00:03:48,360
anyways, because you can manipulate your

83
00:03:45,659 --> 00:03:50,700
own privileges in order to add whatever

84
00:03:48,360 --> 00:03:52,560
you need. And most of the time, from an

85
00:03:50,700 --> 00:03:55,739
attacker perspective, you don't really

86
00:03:52,560 --> 00:03:58,920
care what the privileges are. You just want

87
00:03:55,739 --> 00:04:01,440
to become system so that you can kind of

88
00:03:58,920 --> 00:04:03,959
do what you want, like stealing

89
00:04:01,440 --> 00:04:06,959
domain admin credentials or whatever. So

90
00:04:03,959 --> 00:04:09,840
the actual underlying details of all of

91
00:04:06,959 --> 00:04:12,480
the different privileges and other stuff

92
00:04:09,840 --> 00:04:14,879
are not super relevant. And to be honest,

93
00:04:12,480 --> 00:04:17,519
there was one point where I knew them in

94
00:04:14,879 --> 00:04:19,620
detail and now it's been so long since I

95
00:04:17,519 --> 00:04:22,139
cared that I don't even remember a lot

96
00:04:19,620 --> 00:04:23,940
of the details. That being said there is

97
00:04:22,139 --> 00:04:25,919
certain scenario where it really matters.

98
00:04:23,940 --> 00:04:28,860
Like if you are interested in userland

99
00:04:25,919 --> 00:04:31,500
bugs that someone like James Forshaw

100
00:04:28,860 --> 00:04:34,440
from Project Zero abuses. A lot of

101
00:04:31,500 --> 00:04:37,800
it has to do with the nuances of tokens

102
00:04:34,440 --> 00:04:40,500
and specific edge cases. But again, it's

103
00:04:37,800 --> 00:04:42,540
like what I have already said in this

104
00:04:40,500 --> 00:04:45,000
course, which is like a lot of the time,

105
00:04:42,540 --> 00:04:46,740
unless you know for sure you're going to

106
00:04:45,000 --> 00:04:49,199
have to deal with it, it's not

107
00:04:46,740 --> 00:04:51,900
necessarily worth spending all the time

108
00:04:49,199 --> 00:04:54,540
trying to memorize all the Token stuff,

109
00:04:51,900 --> 00:04:57,479
or anything like that. In general, you can

110
00:04:54,540 --> 00:04:59,460
use a tool like Process Explorer and you

111
00:04:57,479 --> 00:05:02,400
can just see the Token information from

112
00:04:59,460 --> 00:05:04,860
userland as well, which just goes back to

113
00:05:02,400 --> 00:05:07,080
what we said already, that a lot of what

114
00:05:04,860 --> 00:05:09,840
you see in userland is just like a

115
00:05:07,080 --> 00:05:11,580
shadow of the kernel structures exposed

116
00:05:09,840 --> 00:05:14,400
to userland. And so, here we are

117
00:05:11,580 --> 00:05:17,280
looking at the system process like lsass,

118
00:05:14,400 --> 00:05:20,120
and we can see its security identifier

119
00:05:17,280 --> 00:05:20,120
is

120
00:05:20,600 --> 00:05:26,460
S-1-5-18. And we see all the privileges

121
00:05:23,639 --> 00:05:29,100
it has, similarly to what we saw

122
00:05:26,460 --> 00:05:31,680
previously in WinDbg, but this time we

123
00:05:29,100 --> 00:05:34,020
see it from userland. And so, if we take

124
00:05:31,680 --> 00:05:38,699
again the _EPROCESS object we saw

125
00:05:34,020 --> 00:05:40,680
earlier that ends with 701c0, we know

126
00:05:38,699 --> 00:05:42,960
there is an OBJECT_HEADER structure

127
00:05:40,680 --> 00:05:45,900
before the actual _EPROCESS structure

128
00:05:42,960 --> 00:05:48,539
which is kind of why we subtract the 0x30

129
00:05:45,900 --> 00:05:51,539
bytes and then we print the

130
00:05:48,539 --> 00:05:55,639
SecurityDescriptor field, which is a pointer,

131
00:05:51,539 --> 00:05:59,100
and we see it ends with the

132
00:05:55,639 --> 00:06:01,919
0x06724 bytes. In this case, we can ignore

133
00:05:59,100 --> 00:06:04,199
the lower three bits of the shown value

134
00:06:01,919 --> 00:06:06,479
to get the actual SecurityDescriptor

135
00:06:04,199 --> 00:06:11,520
pointer. And so the pointer actually

136
00:06:06,479 --> 00:06:15,300
starts with 0x720 instead of 0x724.

137
00:06:11,520 --> 00:06:17,100
And so, we can use the !sd command to

138
00:06:15,300 --> 00:06:19,500
show the contents on the security

139
00:06:17,100 --> 00:06:22,680
descriptor associated with that object.

140
00:06:19,500 --> 00:06:26,160
It basically contains the owner security

141
00:06:22,680 --> 00:06:29,639
ID and group security ID that dictates

142
00:06:26,160 --> 00:06:32,520
who owns that actual object and a series

143
00:06:29,639 --> 00:06:34,800
of access controls that give additional

144
00:06:32,520 --> 00:06:39,000
permissions. And so the access control

145
00:06:34,800 --> 00:06:41,520
list is known as ACL, and it contains

146
00:06:39,000 --> 00:06:45,479
what we call access control entries,

147
00:06:41,520 --> 00:06:48,060
known as ACE. And we said previously that

148
00:06:45,479 --> 00:06:51,539
the security descriptor contains the

149
00:06:48,060 --> 00:06:55,020
owner security ID and Group security ID,

150
00:06:51,539 --> 00:06:57,780
and so you can imagine that each access

151
00:06:55,020 --> 00:07:01,080
control entry also contains a security

152
00:06:57,780 --> 00:07:03,180
ID to dictate what other threads or

153
00:07:01,080 --> 00:07:06,120
processes can do on that particular

154
00:07:03,180 --> 00:07:09,360
object, and it is indicated by an access

155
00:07:06,120 --> 00:07:12,240
mask that dictates if the access to the

156
00:07:09,360 --> 00:07:14,400
object is allowed or denied. You also

157
00:07:12,240 --> 00:07:16,680
have this concept of account level

158
00:07:14,400 --> 00:07:20,580
privileges. And so you typically have

159
00:07:16,680 --> 00:07:23,220
user privileges and Administrator privileges. And

160
00:07:20,580 --> 00:07:25,380
so, a given process can either be

161
00:07:23,220 --> 00:07:27,840
associated with a user or an

162
00:07:25,380 --> 00:07:29,400
Administrator, and it might be given

163
00:07:27,840 --> 00:07:32,220
different privileges based on that

164
00:07:29,400 --> 00:07:34,259
account type it is associated with. And

165
00:07:32,220 --> 00:07:37,319
so typically, it will be able to access

166
00:07:34,259 --> 00:07:40,080
certain objects or not, depending on that

167
00:07:37,319 --> 00:07:42,840
account. We've just seen that the privileges

168
00:07:40,080 --> 00:07:45,720
are stored into the Token structure and

169
00:07:42,840 --> 00:07:48,620
the Token says if the privileges are

170
00:07:45,720 --> 00:07:50,880
enabled or disabled. And so typically,

171
00:07:48,620 --> 00:07:53,160
privileges that are useful from an

172
00:07:50,880 --> 00:07:55,440
attacker's perspective are the ones to

173
00:07:53,160 --> 00:07:58,319
debug other processes,

174
00:07:55,440 --> 00:08:00,780
so you can actually inspect and adjust

175
00:07:58,319 --> 00:08:03,840
the memory of other processes. And you

176
00:08:00,780 --> 00:08:05,940
have one to actually create tokens which

177
00:08:03,840 --> 00:08:08,280
obviously is like an admin equivalent

178
00:08:05,940 --> 00:08:10,860
privilege, since you can then add your

179
00:08:08,280 --> 00:08:14,160
own tokens that gives you whatever you

180
00:08:10,860 --> 00:08:16,979
need. I guess one thing worth noticing is,

181
00:08:14,160 --> 00:08:19,860
a thread can also sort of impersonate

182
00:08:16,979 --> 00:08:22,740
another specific Token that might not be

183
00:08:19,860 --> 00:08:25,919
the same as the one it originally has, so

184
00:08:22,740 --> 00:08:28,680
that it can just temporarily access certain

185
00:08:25,919 --> 00:08:30,539
objects or whatever. And again, this is a

186
00:08:28,680 --> 00:08:33,240
high privilege. And this is just a

187
00:08:30,539 --> 00:08:35,640
summary of all the privileges that were

188
00:08:33,240 --> 00:08:38,159
available when we did this course but

189
00:08:35,640 --> 00:08:43,039
your mileage may vary, because Microsoft

190
00:08:38,159 --> 00:08:43,039
actually adds new privileges over time. 

