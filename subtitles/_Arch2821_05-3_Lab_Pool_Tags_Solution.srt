1
00:00:00,160 --> 00:00:04,319
So, as we can see the ExAllocatePoolWithTag

2
00:00:02,080 --> 00:00:06,000
function is used to allocate

3
00:00:04,319 --> 00:00:08,160
pool memory.

4
00:00:06,000 --> 00:00:10,559
And it returns a pointer to the

5
00:00:08,160 --> 00:00:12,719
allocated block.

6
00:00:10,559 --> 00:00:15,280
We can see the third argument

7
00:00:12,719 --> 00:00:18,240
is specifying the actual tag and the

8
00:00:15,280 --> 00:00:21,039
first argument is used to specify the

9
00:00:18,240 --> 00:00:24,240
actual type like non-paged pool or paged pool.

10
00:00:21,039 --> 00:00:26,000
Microsoft maintains a list of all

11
00:00:24,240 --> 00:00:28,080
the pool tags,

12
00:00:26,000 --> 00:00:29,840
this is used by WindDbg

13
00:00:28,080 --> 00:00:33,280
in order to know

14
00:00:29,840 --> 00:00:35,280
and show you what pool tag is defined.

15
00:00:33,280 --> 00:00:37,760
If you look for pooltag.txt

16
00:00:35,280 --> 00:00:40,640
after you've installed WinDbg,

17
00:00:37,760 --> 00:00:44,600
you'll see it. Or you can search for that

18
00:00:40,640 --> 00:00:44,600
specific filename online.

19
00:00:45,920 --> 00:00:50,320
You'll see the actual pool tag,

20
00:00:48,320 --> 00:00:52,640
what kernel module it is in, and the

21
00:00:50,320 --> 00:00:54,879
actual description for that pool tag.

22
00:00:52,640 --> 00:00:57,280
This is the syntax to actually set a

23
00:00:54,879 --> 00:01:00,719
breakpoint with a condition. So in the

24
00:00:57,280 --> 00:01:04,320
kernel case, we'll use "ba e 1" to set a

25
00:01:00,719 --> 00:01:06,479
breakpoint on execution instead of "bp".

26
00:01:04,320 --> 00:01:08,720
But then basically, you will use the ".if" syntax

27
00:01:06,479 --> 00:01:11,040
with like the condition,

28
00:01:08,720 --> 00:01:12,479
and then the actual

29
00:01:11,040 --> 00:01:14,560
command you want to execute if the

30
00:01:12,479 --> 00:01:16,880
condition is true.

31
00:01:14,560 --> 00:01:19,200
If you need to set several conditions,

32
00:01:16,880 --> 00:01:19,920
you can use the "&"

33
00:01:19,200 --> 00:01:23,360
to

34
00:01:19,920 --> 00:01:24,400
say it's an "and" condition or a vertical

35
00:01:23,360 --> 00:01:27,439
bar,

36
00:01:24,400 --> 00:01:28,799
like a "|" if you want to say it's an

37
00:01:27,439 --> 00:01:31,360
"or" condition.

38
00:01:28,799 --> 00:01:34,720
A single character is good enough when

39
00:01:31,360 --> 00:01:34,720
you deal with the actual registers.

40
00:01:36,240 --> 00:01:41,840
So, let's set a breakpoint on

41
00:01:38,079 --> 00:01:41,840
ExAllocatePoolWithTag.

42
00:01:44,560 --> 00:01:48,159
We see it hits right away, due to an

43
00:01:46,640 --> 00:01:52,119
allocation in the kernel.

44
00:01:48,159 --> 00:01:52,119
Let's look at the different register.

45
00:01:56,880 --> 00:02:03,280
As defined by the actual prototype

46
00:01:59,600 --> 00:02:03,280
the PoolType defines the actual

47
00:02:05,119 --> 00:02:11,680
type of the allocation.

48
00:02:06,799 --> 00:02:15,840
In this case 512, which is 0x200,

49
00:02:11,680 --> 00:02:15,840
indicates NonPagedPoolNx.

50
00:02:16,560 --> 00:02:19,840
Then, we have the actual NumberOfBytes

51
00:02:18,400 --> 00:02:23,840
for the allocation,

52
00:02:19,840 --> 00:02:23,840
and finally we have the actual pool Tag.

53
00:02:26,640 --> 00:02:30,239
We can see it's BNDN.

54
00:02:32,080 --> 00:02:34,840
Now, let's continue execution until the

55
00:02:33,840 --> 00:02:37,519
function

56
00:02:34,840 --> 00:02:41,360
returns. You can use "step out"

57
00:02:37,519 --> 00:02:41,360
or "gu" in the debugger.

58
00:02:42,239 --> 00:02:48,760
Now, let's analyze the actual allocation

59
00:02:44,800 --> 00:02:48,760
with the "!pool" command.

60
00:02:52,640 --> 00:02:57,760
If we look at the actual help for this

61
00:02:54,400 --> 00:03:00,879
"!pool" command, we can see it accepts flags

62
00:02:57,760 --> 00:03:03,440
after. And if you use "2", it's going to

63
00:03:00,879 --> 00:03:05,440
cause the display to just show the one

64
00:03:03,440 --> 00:03:07,120
you actually want that you specify with

65
00:03:05,440 --> 00:03:09,120
the address.

66
00:03:07,120 --> 00:03:11,680
If we actually use "!pool @rax",

67
00:03:09,120 --> 00:03:13,280
it's going to actually show

68
00:03:11,680 --> 00:03:14,800
different allocations around that

69
00:03:13,280 --> 00:03:17,599
allocation,

70
00:03:14,800 --> 00:03:19,120
which can be quite verbose.

71
00:03:17,599 --> 00:03:21,519
The one that corresponds to the actual

72
00:03:19,120 --> 00:03:25,200
allocation you're interested in is with

73
00:03:21,519 --> 00:03:27,040
a star at the very first.

74
00:03:25,200 --> 00:03:28,000
If you just use

75
00:03:27,040 --> 00:03:29,200
"2",

76
00:03:28,000 --> 00:03:32,680
it's just going to show the one you're

77
00:03:29,200 --> 00:03:32,680
interested in.

78
00:03:36,000 --> 00:03:39,840
So, we can see

79
00:03:38,000 --> 00:03:42,159
from the pool tag that

80
00:03:39,840 --> 00:03:44,840
"NDNB"

81
00:03:42,159 --> 00:03:49,200
is actually not documented into the pool

82
00:03:44,840 --> 00:03:50,959
tag, and that's why it doesn't say any

83
00:03:49,200 --> 00:03:53,599
pool tag because it says unknown, it

84
00:03:50,959 --> 00:03:53,599
doesn't know it.

85
00:03:58,400 --> 00:04:02,159
So, now let's modify our breakpoint to

86
00:04:00,239 --> 00:04:05,439
actually ignore

87
00:04:02,159 --> 00:04:05,439
this pool tag.

88
00:04:06,560 --> 00:04:10,159
So, we modify our breakpoint

89
00:04:08,799 --> 00:04:13,480
to actually

90
00:04:10,159 --> 00:04:13,480
have a condition.

91
00:04:16,479 --> 00:04:19,120
If this is the pool tag we've already

92
00:04:18,000 --> 00:04:22,919
seen,

93
00:04:19,120 --> 00:04:22,919
we just continue execution.

94
00:04:41,840 --> 00:04:47,280
So, we're hitting another breakpoint,

95
00:04:43,840 --> 00:04:47,280
let's continue execution.

96
00:04:53,199 --> 00:05:00,320
We can see in this case we

97
00:04:55,120 --> 00:05:00,320
reach a WFP NBL info container

98
00:05:01,120 --> 00:05:05,280
in the netio.sys.

99
00:05:08,400 --> 00:05:14,400
So, now let's ignore this other pool tag

100
00:05:11,840 --> 00:05:16,160
we've just seen.

101
00:05:14,400 --> 00:05:19,360
We're going to use the "|"

102
00:05:16,160 --> 00:05:22,520
to indicate the "or"

103
00:05:19,360 --> 00:05:22,520
r8.

104
00:05:32,080 --> 00:05:34,960
If you don't see any allocation right

105
00:05:34,000 --> 00:05:36,960
away,

106
00:05:34,960 --> 00:05:38,400
you can go on your target VM and just

107
00:05:36,960 --> 00:05:40,160
try to click.

108
00:05:38,400 --> 00:05:41,759
It should be quite slow and maybe not

109
00:05:40,160 --> 00:05:42,880
responding but

110
00:05:41,759 --> 00:05:45,440
at least

111
00:05:42,880 --> 00:05:47,199
it will try to force some allocations.

112
00:05:45,440 --> 00:05:49,039
The reason it's not responding is mainly

113
00:05:47,199 --> 00:05:50,720
because the debugger

114
00:05:49,039 --> 00:05:53,360
and the breakpoint

115
00:05:50,720 --> 00:05:56,240
is actually hitting a lot,

116
00:05:53,360 --> 00:05:56,240
so it's quite slow.

117
00:05:57,039 --> 00:06:00,960
Now, let's look at the actual r8

118
00:05:59,360 --> 00:06:03,280
tag

119
00:06:00,960 --> 00:06:06,280
and continue execution until it exits the

120
00:06:03,280 --> 00:06:06,280
function.

121
00:06:14,319 --> 00:06:19,440
We can see this one is related to an

122
00:06:16,880 --> 00:06:22,720
export driver for the Windows Filtering

123
00:06:19,440 --> 00:06:22,720
Platform component.

124
00:06:29,759 --> 00:06:35,080
Again, let's ignore that new rax

125
00:06:52,080 --> 00:06:55,400
with the "==".

126
00:07:04,080 --> 00:07:07,960
We're hitting again the breakpoint.

127
00:07:20,720 --> 00:07:25,759
Here, we see

128
00:07:21,919 --> 00:07:25,759
it's defining an Io Mdl.

129
00:07:27,919 --> 00:07:33,360
And it's actually calling the

130
00:07:30,560 --> 00:07:35,919
IoAllocateMdl, so probably it is allocating

131
00:07:33,360 --> 00:07:38,560
an MDL object.

132
00:07:35,919 --> 00:07:39,759
You could continue to actually

133
00:07:38,560 --> 00:07:42,800
analyze

134
00:07:39,759 --> 00:07:46,560
pool tags, also you could try to match

135
00:07:42,800 --> 00:07:46,560
them to the Vergilius objects.

136
00:07:47,680 --> 00:07:52,160
For instance, for MDL,

137
00:07:50,080 --> 00:07:54,639
if you look for MDL you'll find the

138
00:07:52,160 --> 00:07:58,680
actual object that is probably allocated

139
00:07:54,639 --> 00:07:58,680
by IoAllocateMdl.

