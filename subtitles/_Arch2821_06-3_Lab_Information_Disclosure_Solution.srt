﻿1
00:00:00,080 --> 00:00:08,560
So, we can see CVE-2021-31955 has been

2
00:00:04,560 --> 00:00:08,560
patched in June 2021.

3
00:00:08,880 --> 00:00:14,400
It's an information disclosure vulnerability

4
00:00:12,000 --> 00:00:17,519
that was found in-the-wild

5
00:00:14,400 --> 00:00:17,519
by Kaspersky Lab.

6
00:00:17,920 --> 00:00:22,160
This vulnerability is described into

7
00:00:20,000 --> 00:00:24,640
this securelist.com website from

8
00:00:22,160 --> 00:00:26,160
Kaspersky.

9
00:00:24,640 --> 00:00:28,560
We see it is described as an information

10
00:00:26,160 --> 00:00:30,640
disclosure vulnerability into ntoskrnl. And

11
00:00:28,560 --> 00:00:32,960
the vulnerability is in a component

12
00:00:30,640 --> 00:00:36,480
called SuperFetch. It was introduced in

13
00:00:32,960 --> 00:00:37,520
Windows Vista and you can access this

14
00:00:36,480 --> 00:00:39,760
feature

15
00:00:37,520 --> 00:00:41,040
using the NtQuerySystemInformation

16
00:00:39,760 --> 00:00:43,600
syscall.

17
00:00:41,040 --> 00:00:45,440
And the vulnerability lies in the fact that

18
00:00:43,600 --> 00:00:47,360
the returned information contains

19
00:00:45,440 --> 00:00:50,320
_EPROCESS pointers

20
00:00:47,360 --> 00:00:52,399
from kernel memory.

21
00:00:50,320 --> 00:00:53,280
We can see there are some code available

22
00:00:52,399 --> 00:00:56,000
since

23
00:00:53,280 --> 00:00:57,199
2017 that actually contained proof of the

24
00:00:56,000 --> 00:00:58,719
vulnerability.

25
00:00:57,199 --> 00:01:00,800
The syscall to actually trigger the

26
00:00:58,719 --> 00:01:03,280
vulnerability is NtQuerySystemInformation

27
00:01:00,800 --> 00:01:06,960
with a specific

28
00:01:03,280 --> 00:01:06,960
SystemInformationClass being specified.

29
00:01:07,600 --> 00:01:12,320
We can see

30
00:01:08,799 --> 00:01:16,080
in this blog that it's the

31
00:01:12,320 --> 00:01:21,320
SystemSuperfetchInformation,

32
00:01:16,080 --> 00:01:21,320
you can confirm that in the MemInfo.cpp.

33
00:01:23,040 --> 00:01:27,520
Okay, so now let's start the actual

34
00:01:24,880 --> 00:01:29,119
exploit on our target virtual machine.

35
00:01:27,520 --> 00:01:30,880
You can see we are running it from a

36
00:01:29,119 --> 00:01:33,520
normal cmd.exe, it doesn't have any

37
00:01:30,880 --> 00:01:33,520
privileges.

38
00:01:35,360 --> 00:01:39,119
As you can see,

39
00:01:37,680 --> 00:01:42,560
we are listing all the different

40
00:01:39,119 --> 00:01:45,920
processes, their PID,

41
00:01:42,560 --> 00:01:45,920
and some kernel address.

42
00:01:47,280 --> 00:01:50,880
There should be kernel addresses for

43
00:01:48,880 --> 00:01:53,119
_EPROCESSES, let's check that in the

44
00:01:50,880 --> 00:01:54,960
debugger.

45
00:01:53,119 --> 00:01:57,840
So, now let's analyze

46
00:01:54,960 --> 00:01:57,840
with our debugger.

47
00:02:04,880 --> 00:02:09,240
We are copying this address,

48
00:02:16,239 --> 00:02:20,640
and we are printing

49
00:02:18,000 --> 00:02:24,720
this address as an _EPROCESS

50
00:02:20,640 --> 00:02:24,720
and showing these specific fields.

51
00:02:25,840 --> 00:02:32,080
As you can see, the [Image]FileName is

52
00:02:29,640 --> 00:02:35,080
CVE-2021-31955.exe which corresponds to our

53
00:02:32,080 --> 00:02:35,080
executable.

54
00:02:41,840 --> 00:02:46,440
If we take another process like lsass,

55
00:02:54,400 --> 00:02:57,920
and dump it,

56
00:02:55,680 --> 00:03:00,480
we'll see we're actually dumping the

57
00:02:57,920 --> 00:03:03,480
other process. So yes, the leak is

58
00:03:00,480 --> 00:03:03,480
working.

