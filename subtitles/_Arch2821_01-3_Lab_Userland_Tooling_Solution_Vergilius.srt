1
00:00:00,240 --> 00:00:03,120
So, we're going to use the Vergilius Project

2
00:00:03,120 --> 00:00:05,120
to analyze a specific structure,

3
00:00:05,120 --> 00:00:12,360
and see the differences over time.

4
00:00:08,800 --> 00:00:12,360
So we're going to Vergilius.

5
00:00:17,840 --> 00:00:22,560
We're going to select Windows 10 | 1809,

6
00:00:22,720 --> 00:00:26,640
and we're going to look for the

7
00:00:23,680 --> 00:00:26,640
_EPROCESS structure.

8
00:00:29,119 --> 00:00:31,679
So, we can use the copy button to copy

9
00:00:30,960 --> 00:00:34,679
the

10
00:00:31,679 --> 00:00:34,679
structure.

11
00:00:50,320 --> 00:00:54,559
We're going to name that eprocess_

12
00:00:53,199 --> 00:00:57,559
win10_

13
00:00:54,559 --> 00:00:57,559
1809[.h].

14
00:01:02,879 --> 00:01:08,760
Now, we're going to go to the latest

15
00:01:05,040 --> 00:01:08,760
version of Windows 10.

16
00:01:16,560 --> 00:01:21,320
Actually, we're going to compare it with

17
00:01:17,920 --> 00:01:21,320
Windows 11.

18
00:01:38,560 --> 00:01:43,720
Now, let's compare the

19
00:01:40,479 --> 00:01:43,720
two versions.

20
00:01:57,680 --> 00:02:02,479
So, the first thing we can see is that

21
00:01:59,280 --> 00:02:02,479
they have different size.

22
00:02:03,200 --> 00:02:06,880
And actually,

23
00:02:04,479 --> 00:02:10,000
most of it seems to have changed but

24
00:02:06,880 --> 00:02:12,720
it's because the size of the actual PCB

25
00:02:10,000 --> 00:02:14,720
structure which is the _KPROCESS has a

26
00:02:12,720 --> 00:02:16,080
different size so all the fields have

27
00:02:14,720 --> 00:02:17,760
different offsets.

28
00:02:16,080 --> 00:02:20,560
So, the first thing we want to do is to

29
00:02:17,760 --> 00:02:23,840
actually remove the actual offsets

30
00:02:20,560 --> 00:02:23,840
from the files.

31
00:02:26,080 --> 00:02:30,239
So, we're going to basically replace the

32
00:02:28,480 --> 00:02:33,200
//0x

33
00:02:30,239 --> 00:02:36,440
something as a regular expression,

34
00:02:33,200 --> 00:02:36,440
with nothing.

35
00:02:45,360 --> 00:02:48,800
So, now that we have done that we can see

36
00:02:47,519 --> 00:02:51,040
that

37
00:02:48,800 --> 00:02:53,280
there were a lot of changes there are not

38
00:02:51,040 --> 00:02:56,160
real changes. So, let's look at the

39
00:02:53,280 --> 00:02:56,160
different changes.

40
00:02:56,239 --> 00:03:00,720
Here, we see a field that has been

41
00:02:58,239 --> 00:03:00,720
removed,

42
00:03:01,599 --> 00:03:06,720
here we see a DeviceMap that was changed

43
00:03:04,319 --> 00:03:09,440
from VOID* to

44
00:03:06,720 --> 00:03:11,599
_EX_FAST_REF so if we look up what _EX_FAST_REF

45
00:03:09,440 --> 00:03:14,640
is

46
00:03:11,599 --> 00:03:14,640
inside Vergilius,

47
00:03:17,680 --> 00:03:21,760
we can see it's actually a union of

48
00:03:19,920 --> 00:03:25,760
different types and one of them is a

49
00:03:21,760 --> 00:03:25,760
VOID* and it has a RefCnt.

50
00:03:26,799 --> 00:03:33,760
So, we continue looking at other changes.

51
00:03:30,799 --> 00:03:37,040
We see that there is a Flags3

52
00:03:33,760 --> 00:03:39,519
which is a union of different flags.

53
00:03:37,040 --> 00:03:41,599
And some new flags have been added,

54
00:03:39,519 --> 00:03:43,680
so for instance, if we look at

55
00:03:41,599 --> 00:03:46,920
VmProcessorHostTransition

56
00:03:43,680 --> 00:03:46,920
on Google,

57
00:03:48,560 --> 00:03:51,280
we see it's referenced

58
00:03:50,319 --> 00:03:52,640


59
00:03:51,280 --> 00:03:54,799
on this website

60
00:03:52,640 --> 00:03:58,879
and we see that actually this flag has

61
00:03:54,799 --> 00:04:01,040
been added in Windows 10 2004, so we need

62
00:03:58,879 --> 00:04:03,519
more investigation to figure out what it

63
00:04:01,040 --> 00:04:03,519
is.

64
00:04:04,720 --> 00:04:09,640
We can look up DisallowUserTerminate.

65
00:04:17,280 --> 00:04:20,639
So what it tells us, is that at a certain

66
00:04:19,680 --> 00:04:23,280
time

67
00:04:20,639 --> 00:04:26,080
Windows 10 this flag was introduced,

68
00:04:23,280 --> 00:04:28,160
which we've just seen, and what it does

69
00:04:26,080 --> 00:04:29,120
is, basically, any process that has this

70
00:04:28,160 --> 00:04:31,919
flag

71
00:04:29,120 --> 00:04:35,280
set cannot be terminated from user mode,

72
00:04:31,919 --> 00:04:35,280
DisallowUserTerminate.

73
00:04:35,360 --> 00:04:39,840
So, we continue looking at other changes.

74
00:04:38,320 --> 00:04:41,520
So we see that

75
00:04:39,840 --> 00:04:45,040
there is Spare0,

76
00:04:41,520 --> 00:04:46,080
but actually Spare0 used to be

77
00:04:45,040 --> 00:04:48,800
here.

78
00:04:46,080 --> 00:04:50,479
So, actually Spare0 was not removed,

79
00:04:48,800 --> 00:04:52,240
it was actually just

80
00:04:50,479 --> 00:04:54,320
moved to another place,

81
00:04:52,240 --> 00:04:55,520
close to the Machine which is the new

82
00:04:54,320 --> 00:04:57,199
one.

83
00:04:55,520 --> 00:04:59,680
So, we continue looking at the different

84
00:04:57,199 --> 00:04:59,680
changes.

85
00:04:59,919 --> 00:05:05,919
There are a bunch of flags that are new.

86
00:05:03,039 --> 00:05:08,400
We can see the union for the

87
00:05:05,919 --> 00:05:10,479
MitigationFlags2, which as you can imagine, are

88
00:05:08,400 --> 00:05:12,800
defining mitigations

89
00:05:10,479 --> 00:05:15,199
in place for that particular process. So

90
00:05:12,800 --> 00:05:18,320
the first thing we see is that the

91
00:05:15,199 --> 00:05:19,520
CetShadowStacks flag has been renamed into

92
00:05:18,320 --> 00:05:22,560


93
00:05:19,520 --> 00:05:27,160
CetUserShadowStacks. And if we look at

94
00:05:22,560 --> 00:05:27,160
the XtendedControlFlowGuard.

95
00:05:43,120 --> 00:05:49,600
So, we see that in Windows 10 [21H1], XFG

96
00:05:48,400 --> 00:05:54,479
was actually

97
00:05:49,600 --> 00:05:56,240
released but not introduced into 21H1.

98
00:05:54,479 --> 00:05:57,919
And there is some information

99
00:05:56,240 --> 00:06:01,880
that you could read to understand what

100
00:05:57,919 --> 00:06:01,880
XFG is about.

101
00:06:07,039 --> 00:06:12,720
And then we see, at the end,

102
00:06:09,520 --> 00:06:15,840
additional structures that were added

103
00:06:12,720 --> 00:06:19,400
and we need more investigation to know

104
00:06:15,840 --> 00:06:19,400
what this is about.

105
00:06:21,680 --> 00:06:26,080
So, basically

106
00:06:23,199 --> 00:06:29,039
this shows all the different changes

107
00:06:26,080 --> 00:06:30,240
between the two versions we have chosen,

108
00:06:29,039 --> 00:06:32,240
and also

109
00:06:30,240 --> 00:06:34,560
you could actually look into the

110
00:06:32,240 --> 00:06:36,400
_KPROCESS structure as an additional

111
00:06:34,560 --> 00:06:38,560
exercise to figure out what other

112
00:06:36,400 --> 00:06:40,319
changes have been made between these two

113
00:06:38,560 --> 00:06:42,240
versions.

114
00:06:40,319 --> 00:06:44,960
Because we know that the offset of the

115
00:06:42,240 --> 00:06:46,639
following field is different

116
00:06:44,960 --> 00:06:48,720
and all the fields have different

117
00:06:46,639 --> 00:06:52,639
offsets in the _EPROCESS structure due

118
00:06:48,720 --> 00:06:52,639
to the _KPROCESS changes.

