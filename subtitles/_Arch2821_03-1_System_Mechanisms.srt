1
00:00:00,240 --> 00:00:03,679
In this part, we're going to look at

2
00:00:01,520 --> 00:00:04,799
system mechanisms.

3
00:00:03,679 --> 00:00:06,560
We're going to

4
00:00:04,799 --> 00:00:07,919
look into different tables into the

5
00:00:06,560 --> 00:00:11,440
kernel,

6
00:00:07,919 --> 00:00:12,880
that give us information on where

7
00:00:11,440 --> 00:00:15,519
the system

8
00:00:12,880 --> 00:00:17,039
calls are implemented,

9
00:00:15,519 --> 00:00:18,880
on where

10
00:00:17,039 --> 00:00:20,320
the actual exceptions

11
00:00:18,880 --> 00:00:22,320
are handled,

12
00:00:20,320 --> 00:00:23,519
and where some memory regions are

13
00:00:22,320 --> 00:00:26,640
accessed.

14
00:00:23,519 --> 00:00:28,000
Then, we're going to look into APCs which

15
00:00:26,640 --> 00:00:31,599
are a way to

16
00:00:28,000 --> 00:00:34,960
execute code asynchronously into threads.

17
00:00:31,599 --> 00:00:37,360
And we're going to also look into IRQLs,

18
00:00:34,960 --> 00:00:39,680
which are defining

19
00:00:37,360 --> 00:00:41,600
what can be done or can't be done in the

20
00:00:39,680 --> 00:00:43,600
kernel from an exploitation point of

21
00:00:41,600 --> 00:00:45,360
view. Then, we're going to look into what

22
00:00:43,600 --> 00:00:46,719
the Object Manager

23
00:00:45,360 --> 00:00:48,399
typically does,

24
00:00:46,719 --> 00:00:50,640
and what are the different types of

25
00:00:48,399 --> 00:00:52,719
objects that are useful from

26
00:00:50,640 --> 00:00:55,120
exploitation point of view. Finally, we'll

27
00:00:52,719 --> 00:00:56,559
look into the different synchronization

28
00:00:55,120 --> 00:00:58,640
mechanisms

29
00:00:56,559 --> 00:01:02,079
that allow different threads

30
00:00:58,640 --> 00:01:05,280
to deal with data simultaneously without

31
00:01:02,079 --> 00:01:06,960
any problems. Okay, let's get started!

32
00:01:05,280 --> 00:01:09,760
So, sometimes, we say that this call

33
00:01:06,960 --> 00:01:13,040
instruction will trap into the kernel.

34
00:01:09,760 --> 00:01:15,520
And "traps" generally refer to either

35
00:01:13,040 --> 00:01:18,479
exceptions or interrupts. But, basically,

36
00:01:15,520 --> 00:01:21,439
the way they are classified is,

37
00:01:18,479 --> 00:01:23,520
like an interrupt, is asynchronous. So, it

38
00:01:21,439 --> 00:01:26,479
is not the process

39
00:01:23,520 --> 00:01:28,560
specifically of executing

40
00:01:26,479 --> 00:01:31,840
like an instruction, like for instance a

41
00:01:28,560 --> 00:01:32,720
syscall instruction, that triggers it but

42
00:01:31,840 --> 00:01:35,680


43
00:01:32,720 --> 00:01:36,479
the interrupt will be more something

44
00:01:35,680 --> 00:01:38,320
like

45
00:01:36,479 --> 00:01:40,799
when there is a timer

46
00:01:38,320 --> 00:01:44,240
that fires, for example due to the clock

47
00:01:40,799 --> 00:01:46,560
or some device on the PCI bus that says:

48
00:01:44,240 --> 00:01:49,840
"I'm ready for you to do something right

49
00:01:46,560 --> 00:01:52,079
now" while the kernel is actually doing

50
00:01:49,840 --> 00:01:54,159
something else. Contrary to that,

51
00:01:52,079 --> 00:01:55,759
an exception is basically

52
00:01:54,159 --> 00:01:57,840
classified as something that is

53
00:01:55,759 --> 00:02:00,320
synchronous, and so the main exception

54
00:01:57,840 --> 00:02:02,640
we'll be dealing with is the actual

55
00:02:00,320 --> 00:02:05,360
"syscall" instruction. Another one would be

56
00:02:02,640 --> 00:02:07,439
like a "breakpoint" which helps with

57
00:02:05,360 --> 00:02:10,080
debugging. But in general, all the

58
00:02:07,439 --> 00:02:13,040
exceptions we'll be dealing with are

59
00:02:10,080 --> 00:02:14,640
centered around system calls. So,

60
00:02:13,040 --> 00:02:17,760
if you're interested in

61
00:02:14,640 --> 00:02:19,840
local kernel exploitation,

62
00:02:17,760 --> 00:02:22,400
most of the time you actually

63
00:02:19,840 --> 00:02:24,800
trigger bugs by calling system calls, so

64
00:02:22,400 --> 00:02:26,879
it's useful to know the terminology. So,

65
00:02:24,800 --> 00:02:29,599
the first table we're interested in is

66
00:02:26,879 --> 00:02:30,560
the Interrupt Descriptor Table also

67
00:02:29,599 --> 00:02:33,360
known as

68
00:02:30,560 --> 00:02:35,680
IDT. And in general, it shows the

69
00:02:33,360 --> 00:02:38,400
different handlers for interrupts. And

70
00:02:35,680 --> 00:02:41,040
confusingly, even if the name is

71
00:02:38,400 --> 00:02:44,480
Interrupt Descriptor Table, and we just

72
00:02:41,040 --> 00:02:48,000
said interrupts are classified as

73
00:02:44,480 --> 00:02:50,959
asychronous, exception style traps that

74
00:02:48,000 --> 00:02:53,680
are triggered synchronously also are

75
00:02:50,959 --> 00:02:55,599
tracked by this table. However, this table

76
00:02:53,680 --> 00:02:58,239
does not track the actual syscalls'

77
00:02:55,599 --> 00:02:59,920
exceptions. Also another thing is there are

78
00:02:58,239 --> 00:03:02,400
unprivileged instructions that you can

79
00:02:59,920 --> 00:03:05,120
call from userland like SIDT. And,

80
00:03:02,400 --> 00:03:07,280
historically, you could use these

81
00:03:05,120 --> 00:03:09,760
instructions to get the address of that

82
00:03:07,280 --> 00:03:12,959
table [IDT] from userland. So,

83
00:03:09,760 --> 00:03:14,400
if you have an arbitrary read primitive, you

84
00:03:12,959 --> 00:03:16,239
could just provide the address of that

85
00:03:14,400 --> 00:03:19,840
table, and start reading other stuff in

86
00:03:16,239 --> 00:03:21,760
the kernel. So, another table is the

87
00:03:19,840 --> 00:03:24,080
Global Descriptor Table. So

88
00:03:21,760 --> 00:03:27,200
interestingly this table is not heavily

89
00:03:24,080 --> 00:03:29,360
used anymore, but in the old days when

90
00:03:27,200 --> 00:03:31,599
there was a linear memory region, things

91
00:03:29,360 --> 00:03:33,920
would be broken up into segments,

92
00:03:31,599 --> 00:03:36,239
and basically this table would dictate

93
00:03:33,920 --> 00:03:38,799
things where like things like when you

94
00:03:36,239 --> 00:03:41,599
execute an instruction to access data

95
00:03:38,799 --> 00:03:43,519
from a memory region indexed by one of

96
00:03:41,599 --> 00:03:46,959
these segment registers. So, in

97
00:03:43,519 --> 00:03:49,519
general all the segments are the same

98
00:03:46,959 --> 00:03:52,560
especially nowadays, but Windows

99
00:03:49,519 --> 00:03:54,560
uses the special register GS to point to

100
00:03:52,560 --> 00:03:56,480
a special structure. And it's used both

101
00:03:54,560 --> 00:03:58,720
in userland and in kernel, and we'll

102
00:03:56,480 --> 00:04:00,400
detail that later. But, basically it

103
00:03:58,720 --> 00:04:02,640
allows the kernel

104
00:04:00,400 --> 00:04:04,799
for quick access of things without

105
00:04:02,640 --> 00:04:06,640
having to hardcode a pointer everywhere into

106
00:04:04,799 --> 00:04:07,920
the kernel. So, in general just

107
00:04:06,640 --> 00:04:11,360
knowing that

108
00:04:07,920 --> 00:04:13,439
GS is used for that purpose

109
00:04:11,360 --> 00:04:16,639
is good enough, and you pretty much never

110
00:04:13,439 --> 00:04:19,359
have to use other segment registers

111
00:04:16,639 --> 00:04:22,160
or even look at the GDT. So, another table

112
00:04:19,359 --> 00:04:23,680
that is worth talking about is the

113
00:04:22,160 --> 00:04:26,639
SSDT.

114
00:04:23,680 --> 00:04:28,720
So, on Linux we say syscall table but on

115
00:04:26,639 --> 00:04:31,759
Windows we say System

116
00:04:28,720 --> 00:04:33,919
Service Descriptor Table for SSDT, but

117
00:04:31,759 --> 00:04:35,840
it's basically a table of relative

118
00:04:33,919 --> 00:04:38,080
offsets that point to the different

119
00:04:35,840 --> 00:04:40,160
syscalls' implementation

120
00:04:38,080 --> 00:04:42,320
and there is a ton of them, like compared

121
00:04:40,160 --> 00:04:44,160
to Linux, for instance, there are a lot

122
00:04:42,320 --> 00:04:46,479
more and so

123
00:04:44,160 --> 00:04:49,280
the symbol is called

124
00:04:46,479 --> 00:04:50,479
KiServiceTable and you

125
00:04:49,280 --> 00:04:51,360
can find it

126
00:04:50,479 --> 00:04:54,320
by

127
00:04:51,360 --> 00:04:55,840
looking up the KeServiceDescriptorTable.

128
00:04:54,320 --> 00:04:58,320
So, the way you can figure out

129
00:04:55,840 --> 00:04:59,600
where the corresponding system call is

130
00:04:58,320 --> 00:05:01,680
implemented

131
00:04:59,600 --> 00:05:03,360
is described in this slide and so if you

132
00:05:01,680 --> 00:05:04,800
see where the syscall is dispatched in

133
00:05:03,360 --> 00:05:06,240
userland,

134
00:05:04,800 --> 00:05:09,440
so for instance,

135
00:05:06,240 --> 00:05:10,720
in the ZwCreateFile function

136
00:05:09,440 --> 00:05:13,600


137
00:05:10,720 --> 00:05:14,880
you'll see that for this function it

138
00:05:13,600 --> 00:05:16,320
actually

139
00:05:14,880 --> 00:05:19,680
saves

140
00:05:16,320 --> 00:05:21,759
0x55 into eax before calling the syscall

141
00:05:19,680 --> 00:05:24,320
instruction,

142
00:05:21,759 --> 00:05:26,800
and so you can basically look up the

143
00:05:24,320 --> 00:05:28,160
55th entry

144
00:05:26,800 --> 00:05:30,320
in the table,

145
00:05:28,160 --> 00:05:32,160
and you'll get like a relative offset,

146
00:05:30,320 --> 00:05:34,160
and so using that relative offset you

147
00:05:32,160 --> 00:05:36,720
you can shift it and then you'll get the

148
00:05:34,160 --> 00:05:38,639
actual syscall implementation address in

149
00:05:36,720 --> 00:05:40,880
the kernel. So, in practice

150
00:05:38,639 --> 00:05:42,639
it's pretty rare you have to do this,

151
00:05:40,880 --> 00:05:45,360
because generally the name of the system

152
00:05:42,639 --> 00:05:47,039
call wrapper in ntdll is the same

153
00:05:45,360 --> 00:05:48,560
as the name of the syscall

154
00:05:47,039 --> 00:05:50,400
implementation

155
00:05:48,560 --> 00:05:54,000
in the kernel, except the module name

156
00:05:50,400 --> 00:05:56,240
like ntdll and userland and ntoskrnl

157
00:05:54,000 --> 00:05:57,759
in kernel. But, basically

158
00:05:56,240 --> 00:05:59,680
it's still interesting to know in case

159
00:05:57,759 --> 00:06:01,120
you want to analyze like a new

160
00:05:59,680 --> 00:06:03,840
version of Windows, and you want to see

161
00:06:01,120 --> 00:06:06,240
if they added any new system call. So,

162
00:06:03,840 --> 00:06:09,039
because system call numbers were

163
00:06:06,240 --> 00:06:10,479
changing over time, people started to

164
00:06:09,039 --> 00:06:13,360
publicly

165
00:06:10,479 --> 00:06:14,319
detail and like, document all the system

166
00:06:13,360 --> 00:06:17,280
calls.

167
00:06:14,319 --> 00:06:20,080
So, in particular j00ru from Project Zero

168
00:06:17,280 --> 00:06:22,160
has a website that lists all the system

169
00:06:20,080 --> 00:06:24,639
calls, and so you can search for specific

170
00:06:22,160 --> 00:06:26,560
system calls to get their numbers

171
00:06:24,639 --> 00:06:28,800
for all the different operating system

172
00:06:26,560 --> 00:06:30,319
versions, and also it's quite useful if

173
00:06:28,800 --> 00:06:32,160
you want to get some ideas of things you

174
00:06:30,319 --> 00:06:34,479
want to poke around, like for finding

175
00:06:32,160 --> 00:06:36,479
bugs, like new system calls, or more

176
00:06:34,479 --> 00:06:38,479
generally if you want to find system

177
00:06:36,479 --> 00:06:40,000
calls that contain a specific word, like

178
00:06:38,479 --> 00:06:42,000
for instance, if you are interested in

179
00:06:40,000 --> 00:06:44,960
the Kernel Transaction Manager, most of

180
00:06:42,000 --> 00:06:46,960
the system calls start with Tm

181
00:06:44,960 --> 00:06:48,800
like Transaction Manager, so you can find

182
00:06:46,960 --> 00:06:51,520
them all on this website. So, there is

183
00:06:48,800 --> 00:06:53,599
this concept of asynchronous procedure

184
00:06:51,520 --> 00:06:56,960
call, and I mentioned

185
00:06:53,599 --> 00:06:59,120
it previously that win32k

186
00:06:56,960 --> 00:07:00,960
can call into userland, so basically the

187
00:06:59,120 --> 00:07:02,400
way this works is sometimes the kernel

188
00:07:00,960 --> 00:07:03,919
wants to execute some thread in user

189
00:07:02,400 --> 00:07:06,240
land for like some special

190
00:07:03,919 --> 00:07:08,960
functionality that the userland process

191
00:07:06,240 --> 00:07:11,520
doesn't know itself should be run. And so

192
00:07:08,960 --> 00:07:13,840
this is basically done through a special

193
00:07:11,520 --> 00:07:16,720
kernel to userland function invocation

194
00:07:13,840 --> 00:07:19,120
which is called APC. And basically it

195
00:07:16,720 --> 00:07:20,400
relies on this idea that a thread can be

196
00:07:19,120 --> 00:07:22,720
alertable,

197
00:07:20,400 --> 00:07:24,400
which means that in a state that allows

198
00:07:22,720 --> 00:07:26,639
it to be scheduled,

199
00:07:24,400 --> 00:07:28,720
and typically

200
00:07:26,639 --> 00:07:30,639
the way it works is anytime the thread

201
00:07:28,720 --> 00:07:32,880
is busy waiting for something else to

202
00:07:30,639 --> 00:07:35,039
happen, before it can continue executing,

203
00:07:32,880 --> 00:07:36,400
for example waiting on a

204
00:07:35,039 --> 00:07:38,800
mutex,

205
00:07:36,400 --> 00:07:41,440
meaning the thread can't continue

206
00:07:38,800 --> 00:07:43,759
doing something useful, then

207
00:07:41,440 --> 00:07:45,520
basically, what's going to happen is an

208
00:07:43,759 --> 00:07:47,599
APC will be

209
00:07:45,520 --> 00:07:49,520
executed instead, by

210
00:07:47,599 --> 00:07:50,879
invoking like a new state of

211
00:07:49,520 --> 00:07:52,800
execution

212
00:07:50,879 --> 00:07:55,199
for that particular thread. So one

213
00:07:52,800 --> 00:07:57,599
example where this is relevant

214
00:07:55,199 --> 00:08:00,720
is, if you get code execution in the kernel,

215
00:07:57,599 --> 00:08:02,960
and you want to pivot to userland for

216
00:08:00,720 --> 00:08:07,039
instance inside of lsass to steal

217
00:08:02,960 --> 00:08:09,759
credentials or hashes. So what you'll do

218
00:08:07,039 --> 00:08:11,919
is you invoke APC

219
00:08:09,759 --> 00:08:15,039
and so you would allocate memory into

220
00:08:11,919 --> 00:08:16,479
the userland process and queue that APC,

221
00:08:15,039 --> 00:08:19,199
and so your

222
00:08:16,479 --> 00:08:21,440
your code will be executed as soon as an

223
00:08:19,199 --> 00:08:24,800
alertable thread is scheduled. So there is

224
00:08:21,440 --> 00:08:27,759
this concept of IRQL for Interrupt

225
00:08:24,800 --> 00:08:30,240
Request Level. And so

226
00:08:27,759 --> 00:08:31,599
basically when we are interested in

227
00:08:30,240 --> 00:08:34,560
Windows kernel

228
00:08:31,599 --> 00:08:38,880
exploitation in general, all we

229
00:08:34,560 --> 00:08:41,200
are interested in are IRQLs 0, 1 and 2. And so

230
00:08:38,880 --> 00:08:43,279
if your thread is just running normally,

231
00:08:41,200 --> 00:08:45,760
like a userland thread, and no interrupt

232
00:08:43,279 --> 00:08:48,399
occurred, and so your thread will be at

233
00:08:45,760 --> 00:08:50,080
passive level like 0. So, for instance,

234
00:08:48,399 --> 00:08:52,640
you run code in userland that

235
00:08:50,080 --> 00:08:54,320
calls into a system call so you end up

236
00:08:52,640 --> 00:08:57,519
in the kernel, it will still be at

237
00:08:54,320 --> 00:09:00,240
passive level, like 0, but for certain

238
00:08:57,519 --> 00:09:02,640
other functionality, like scheduling or

239
00:09:00,240 --> 00:09:04,480
other critical functionality, that can't

240
00:09:02,640 --> 00:09:07,440
block, the code will actually be running

241
00:09:04,480 --> 00:09:09,200
at higher IRQLs, and this aspect can be

242
00:09:07,440 --> 00:09:11,200
fairly important for exploitation. So

243
00:09:09,200 --> 00:09:13,279
basically, the way it works is there are

244
00:09:11,200 --> 00:09:15,279
certain scenarios where say, you access

245
00:09:13,279 --> 00:09:17,279
certain virtual memory, and there is a

246
00:09:15,279 --> 00:09:19,760
page fault, because the actual physical

247
00:09:17,279 --> 00:09:22,240
memory backing the actual virtual memory

248
00:09:19,760 --> 00:09:25,120
is not cached yet, the kernel will have to

249
00:09:22,240 --> 00:09:27,440
do a page table walk to populate the

250
00:09:25,120 --> 00:09:29,680
memory. So, basically the way that

251
00:09:27,440 --> 00:09:32,880
works is that, when the page fault

252
00:09:29,680 --> 00:09:36,080
handler gets called, the kernel says:

253
00:09:32,880 --> 00:09:38,959
"was the IRQL of the caller that triggered

254
00:09:36,080 --> 00:09:41,839
this interrupt one value that allows it

255
00:09:38,959 --> 00:09:43,519
to handle this page fault or not?" and if

256
00:09:41,839 --> 00:09:46,720
not, it will trigger a blue screen of

257
00:09:43,519 --> 00:09:49,200
death, like a BSOD. And so this concept of

258
00:09:46,720 --> 00:09:51,279
IRQLs, basically, allows a certain level

259
00:09:49,200 --> 00:09:53,360
of sanity in the kernel. So, one thing that

260
00:09:51,279 --> 00:09:56,240
can be confusing at first is that

261
00:09:53,360 --> 00:09:59,440
Windows refers to the level 0 as

262
00:09:56,240 --> 00:10:02,079
passive level, but WinDbg uses the term

263
00:09:59,440 --> 00:10:04,079
low level, but basically anything that is

264
00:10:02,079 --> 00:10:06,399
running in the kernel on behalf of user

265
00:10:04,079 --> 00:10:08,240
code that is not specifically adjusted

266
00:10:06,399 --> 00:10:11,040
will be running at passive level. And

267
00:10:08,240 --> 00:10:13,279
then, you have APIs like KeEnterCriticalRegion

268
00:10:11,040 --> 00:10:16,079
which basically will

269
00:10:13,279 --> 00:10:19,200
adjust the IRQL, so some other things

270
00:10:16,079 --> 00:10:21,600
like APCs can't happen when that thread

271
00:10:19,200 --> 00:10:23,760
executes. So, there are a few consequences

272
00:10:21,600 --> 00:10:26,079
from what I've just explained. So for

273
00:10:23,760 --> 00:10:28,640
instance, if this thread is at dispatch

274
00:10:26,079 --> 00:10:31,040
level, so like level 2, you won't be

275
00:10:28,640 --> 00:10:32,560
allowed to block on a mutex,

276
00:10:31,040 --> 00:10:35,200
or something like that, so the only

277
00:10:32,560 --> 00:10:36,880
solution, if it has to wait for

278
00:10:35,200 --> 00:10:38,560
certain thing to happen, is just to do a

279
00:10:36,880 --> 00:10:40,720
spinlock, and just to loop forever and

280
00:10:38,560 --> 00:10:42,880
check the value over time. Another thing

281
00:10:40,720 --> 00:10:46,480
is that the actual pager

282
00:10:42,880 --> 00:10:48,320
runs at the APC level, so level 1. So, a

283
00:10:46,480 --> 00:10:50,800
consequence of that is that, at dispatch

284
00:10:48,320 --> 00:10:53,839
level, like level 2, it's not

285
00:10:50,800 --> 00:10:56,079
allowed to access paged out memory.

286
00:10:53,839 --> 00:10:58,320
So, if a page fault is triggered, it

287
00:10:56,079 --> 00:11:00,480
will just be BSOD. So, from an exploitation

288
00:10:58,320 --> 00:11:01,839
perspective, the reason why we care about

289
00:11:00,480 --> 00:11:03,839
this kind of information is that,

290
00:11:01,839 --> 00:11:05,519
sometimes you are executing some

291
00:11:03,839 --> 00:11:07,600
code path in the kernel, that you are trying

292
00:11:05,519 --> 00:11:10,240
to abuse, so for instance, you are trying

293
00:11:07,600 --> 00:11:12,800
to either get code execution

294
00:11:10,240 --> 00:11:15,920
or you're trying to make the kernel

295
00:11:12,800 --> 00:11:18,320
access some data you control. And so,

296
00:11:15,920 --> 00:11:20,640
typically, you would want the code in the

297
00:11:18,320 --> 00:11:23,040
kernel to use pointers into userland

298
00:11:20,640 --> 00:11:25,680
that you control. But one problem is that

299
00:11:23,040 --> 00:11:28,079
userland memory can typically be paged

300
00:11:25,680 --> 00:11:29,920
out. So, when the kernel accesses your

301
00:11:28,079 --> 00:11:32,160
fake userland data

302
00:11:29,920 --> 00:11:34,959
it could actually lead to a page fault when the

303
00:11:32,160 --> 00:11:37,040
kernel tries to read the data. So if

304
00:11:34,959 --> 00:11:38,399
the code path in the kernel that you are

305
00:11:37,040 --> 00:11:40,880
trying to abuse

306
00:11:38,399 --> 00:11:42,720
is running at passive or APC level, like

307
00:11:40,880 --> 00:11:44,720
level

308
00:11:42,720 --> 00:11:46,959
0 or 1,

309
00:11:44,720 --> 00:11:49,040
the page fault will be automatically

310
00:11:46,959 --> 00:11:51,440
handled and the kernel code will use

311
00:11:49,040 --> 00:11:53,120
your userland data and it will just work,

312
00:11:51,440 --> 00:11:54,880
and you will be able to abuse the kernel

313
00:11:53,120 --> 00:11:56,959
to get better exploitation primitives.

314
00:11:54,880 --> 00:11:58,639
However, if the code path in the kernel

315
00:11:56,959 --> 00:12:00,959
you're trying to abuse is actually

316
00:11:58,639 --> 00:12:03,200
running at dispatch level, at level 2,

317
00:12:00,959 --> 00:12:06,079
you end up with a situation where the

318
00:12:03,200 --> 00:12:09,200
page faults can't be handled,

319
00:12:06,079 --> 00:12:11,760
and it's going to result into a BSOD.

320
00:12:09,200 --> 00:12:13,600
So it's worth knowing IRQLs in general,

321
00:12:11,760 --> 00:12:15,680
as they can drastically change the

322
00:12:13,600 --> 00:12:18,399
reliability of your exploit depending on

323
00:12:15,680 --> 00:12:20,000
how you decide to approach exploitation.

324
00:12:18,399 --> 00:12:22,720
However, in practice, you don't really

325
00:12:20,000 --> 00:12:24,079
need to know like the IRQL in advance,

326
00:12:22,720 --> 00:12:26,399
but when you're trying to exploit a

327
00:12:24,079 --> 00:12:28,399
bug, if you end up with like weird BSOD

328
00:12:26,399 --> 00:12:30,560
crashes when the kernel accesses your

329
00:12:28,399 --> 00:12:32,160
userland structures,

330
00:12:30,560 --> 00:12:35,200
then it's good to remember that it could

331
00:12:32,160 --> 00:12:37,440
be related to the IRQL. Also, for

332
00:12:35,200 --> 00:12:39,760
remote bugs like EternalBlue or

333
00:12:37,440 --> 00:12:41,839
EternalRomance, when you get code execution, you

334
00:12:39,760 --> 00:12:43,600
run at dispatch level, so if you

335
00:12:41,839 --> 00:12:47,360
want to do certain functionality, like

336
00:12:43,600 --> 00:12:50,000
parsing the ntoskrnl.exe binary

337
00:12:47,360 --> 00:12:51,440
to find certain functions, what they are

338
00:12:50,000 --> 00:12:54,720
called and stuff like that, you're going

339
00:12:51,440 --> 00:12:56,800
to want to walk these pages.

340
00:12:54,720 --> 00:12:59,120
And for doing so you need to be at passive

341
00:12:56,800 --> 00:13:00,959
level, otherwise you risk to trigger a

342
00:12:59,120 --> 00:13:03,200
page fault due to the page not being

343
00:13:00,959 --> 00:13:06,000
mapped, and that would trigger a BSOD.

344
00:13:03,200 --> 00:13:07,920
Ans so, in order to be at passive level first,

345
00:13:06,000 --> 00:13:09,760
you have to find a way to transition

346
00:13:07,920 --> 00:13:12,560
from dispatch level to

347
00:13:09,760 --> 00:13:14,160
passive level, first. But again, until you

348
00:13:12,560 --> 00:13:15,519
actually encounter a scenario where you

349
00:13:14,160 --> 00:13:17,600
have to deal with that,

350
00:13:15,519 --> 00:13:19,920
you probably don't need to worry about it.

351
00:13:17,600 --> 00:13:22,480
So, the Object Manager is, basically,

352
00:13:19,920 --> 00:13:24,560
all the functions related to managing

353
00:13:22,480 --> 00:13:27,200
the Objects in the kernel, that we talked

354
00:13:24,560 --> 00:13:29,279
about like _EPROCESS, _ETHREAD, all the

355
00:13:27,200 --> 00:13:30,240
structures. And so, the Object Manager

356
00:13:29,279 --> 00:13:32,160
doesn't

357
00:13:30,240 --> 00:13:33,920
have its own thread running like in the

358
00:13:32,160 --> 00:13:35,440
kernel. When we talk about the Object

359
00:13:33,920 --> 00:13:38,880
manager, we're just talking about the

360
00:13:35,440 --> 00:13:41,519
abstraction of all the APIs for dealing

361
00:13:38,880 --> 00:13:44,480
with Objects creation, deletion

362
00:13:41,519 --> 00:13:47,040
and tracking of them. So

363
00:13:44,480 --> 00:13:50,720
the Object Manager, basically, they are

364
00:13:47,040 --> 00:13:52,959
enforcing a specific naming scheme, a

365
00:13:50,720 --> 00:13:56,399
standardized reference counting

366
00:13:52,959 --> 00:13:58,639
mechanism. And so, one aspect where it's

367
00:13:56,399 --> 00:14:01,600
interesting to understand how the Object

368
00:13:58,639 --> 00:14:04,560
manager works is when you analyze some

369
00:14:01,600 --> 00:14:05,600
rootkit stuff. So, you often hear the term

370
00:14:04,560 --> 00:14:09,040
DKOM

371
00:14:05,600 --> 00:14:10,880
for Direct Kernel Object Manipulation.

372
00:14:09,040 --> 00:14:11,680
And the idea is that instead of calling

373
00:14:10,880 --> 00:14:14,320
the

374
00:14:11,680 --> 00:14:16,480
Object Manager APIs directly, the

375
00:14:14,320 --> 00:14:18,959
attacker is just going to modify the

376
00:14:16,480 --> 00:14:21,279
Objects in memory. And the reason for

377
00:14:18,959 --> 00:14:24,320
that is that, you're trying to avoid

378
00:14:21,279 --> 00:14:26,399
antivirus detection, because the AV

379
00:14:24,320 --> 00:14:28,480
the antivirus, will typically hook the

380
00:14:26,399 --> 00:14:30,480
Object Manager APIs to make sure you're

381
00:14:28,480 --> 00:14:33,199
not modifying certain kernel structures,

382
00:14:30,480 --> 00:14:35,600
or objects. So, I kind of mentioned this

383
00:14:33,199 --> 00:14:38,399
quickly before but basically structures'

384
00:14:35,600 --> 00:14:41,199
names have this convention where E

385
00:14:38,399 --> 00:14:44,399
stands for Executive and K stands for

386
00:14:41,199 --> 00:14:46,720
Kernel. So, usually the E structure

387
00:14:44,399 --> 00:14:48,639
will have a sub-structure which is a K

388
00:14:46,720 --> 00:14:50,399
structure and the core parts of the

389
00:14:48,639 --> 00:14:53,040
kernel like the scheduler will only

390
00:14:50,399 --> 00:14:55,360
access the K parts. But in practice you

391
00:14:53,040 --> 00:14:59,120
can think of them as exactly the same

392
00:14:55,360 --> 00:15:01,680
thing, and actually in a lot of people

393
00:14:59,120 --> 00:15:02,560
exploits, like in their own exploits,

394
00:15:01,680 --> 00:15:05,440
people

395
00:15:02,560 --> 00:15:08,079
refer to the K and E structures

396
00:15:05,440 --> 00:15:09,440
interchangeably even, which can be

397
00:15:08,079 --> 00:15:12,000
confusing at first if you're not

398
00:15:09,440 --> 00:15:13,760
familiar with this simple difference. So,

399
00:15:12,000 --> 00:15:16,240
common object that you'll typically run

400
00:15:13,760 --> 00:15:18,720
into. So we talked about _EPROCESS and

401
00:15:16,240 --> 00:15:21,279
_ETHREAD already, but a very important

402
00:15:18,720 --> 00:15:23,839
one is the actual Token, which you'll

403
00:15:21,279 --> 00:15:25,279
always be dealing with, when you'll be

404
00:15:23,839 --> 00:15:26,720
doing like local

405
00:15:25,279 --> 00:15:28,639
kernel exploitation,

406
00:15:26,720 --> 00:15:31,120
because you often want to find like a

407
00:15:28,639 --> 00:15:34,160
high privileged Token in memory and then

408
00:15:31,120 --> 00:15:37,199
patch a pointer into your own _EPROCESS

409
00:15:34,160 --> 00:15:39,600
structure to point to the same Token

410
00:15:37,199 --> 00:15:42,240
that you've just stolen. And that will

411
00:15:39,600 --> 00:15:43,759
allow you to elevate your own privilege

412
00:15:42,240 --> 00:15:45,839
for your own process.

413
00:15:43,759 --> 00:15:47,279
And then you have other Objects like

414
00:15:45,839 --> 00:15:48,959
Semaphore,

415
00:15:47,279 --> 00:15:51,040
Mutant, which are related to

416
00:15:48,959 --> 00:15:52,880
synchronization between different threads.

417
00:15:51,040 --> 00:15:56,000
Basically, a Mutant is the Windows

418
00:15:52,880 --> 00:15:57,920
terminology to represent a mutex. And

419
00:15:56,000 --> 00:15:59,199
again, for most of these,

420
00:15:57,920 --> 00:16:01,199
you don't really

421
00:15:59,199 --> 00:16:02,959
need to look into them in advance, or

422
00:16:01,199 --> 00:16:04,959
care about the contents,

423
00:16:02,959 --> 00:16:06,880
until you actually run into them. And

424
00:16:04,959 --> 00:16:08,639
when that is the case, you use

425
00:16:06,880 --> 00:16:11,680
something like Vergillius to learn about

426
00:16:08,639 --> 00:16:13,920
them, and abuse them. So,

427
00:16:11,680 --> 00:16:17,120
let's talk about synchronization. So the

428
00:16:13,920 --> 00:16:20,480
idea is that a lot of structures contain

429
00:16:17,120 --> 00:16:23,120
sensitive data, so the kernel only wants one

430
00:16:20,480 --> 00:16:25,920
thread of execution accessing things

431
00:16:23,120 --> 00:16:28,160
at a given time. So, we typically use

432
00:16:25,920 --> 00:16:30,399
mutual exclusion for this,

433
00:16:28,160 --> 00:16:33,199
which basically blocks other threads

434
00:16:30,399 --> 00:16:35,600
from accessing data that is currently

435
00:16:33,199 --> 00:16:36,399
accessed by another thread. And these

436
00:16:35,600 --> 00:16:38,480
will

437
00:16:36,399 --> 00:16:40,399
unblock when the

438
00:16:38,480 --> 00:16:42,079
the other thread has finished accessing

439
00:16:40,399 --> 00:16:44,480
it. And so it's heavily used in the

440
00:16:42,079 --> 00:16:46,880
kernel, and you'll run into this

441
00:16:44,480 --> 00:16:49,600
synchronization APIs all the time.

442
00:16:46,880 --> 00:16:51,680
It's especially relevant because it can

443
00:16:49,600 --> 00:16:53,440
lead to things like use-after-free or

444
00:16:51,680 --> 00:16:56,240
race condition problems.

445
00:16:53,440 --> 00:16:58,480
So, there are typically 4 main types

446
00:16:56,240 --> 00:17:00,000
of synchronization that we'll be

447
00:16:58,480 --> 00:17:02,160
describing now.

448
00:17:00,000 --> 00:17:05,280
So, an interlocked operation is basically

449
00:17:02,160 --> 00:17:08,000
just a wrapper around an atomic assembly

450
00:17:05,280 --> 00:17:10,480
instruction that ensures that the data

451
00:17:08,000 --> 00:17:12,959
that is accessed is not modified at the

452
00:17:10,480 --> 00:17:14,959
same time by another thread. And so, if

453
00:17:12,959 --> 00:17:17,439
you're looking at the IDA Pro decompiler,

454
00:17:14,959 --> 00:17:20,720
HexRays, for instance, you'll see these

455
00:17:17,439 --> 00:17:22,079
_interlockedbittestandset

456
00:17:20,720 --> 00:17:24,559
API

457
00:17:22,079 --> 00:17:27,439
when the actual assembly instruction

458
00:17:24,559 --> 00:17:30,080
corresponds to "lock bts" for instance.

459
00:17:27,439 --> 00:17:32,480
Basically, it just corresponds to a "lock"

460
00:17:30,080 --> 00:17:34,799
something assembly instruction. So

461
00:17:32,480 --> 00:17:36,240
these interlocked operations are

462
00:17:34,799 --> 00:17:37,919
typically used

463
00:17:36,240 --> 00:17:40,080
by spinlocks.

464
00:17:37,919 --> 00:17:42,080
A spinlock is something that does not

465
00:17:40,080 --> 00:17:45,840
actually block per se. It is not

466
00:17:42,080 --> 00:17:48,559
something that sets an alertable state

467
00:17:45,840 --> 00:17:49,760
that allows code to be rescheduled. It is

468
00:17:48,559 --> 00:17:51,760
just really

469
00:17:49,760 --> 00:17:54,240
a tight loop in C

470
00:17:51,760 --> 00:17:56,880
that is basically just spinning until it

471
00:17:54,240 --> 00:17:59,039
does this atomic operation successfully.

472
00:17:56,880 --> 00:18:00,799
So, when the result is true

473
00:17:59,039 --> 00:18:02,880
and it

474
00:18:00,799 --> 00:18:06,480
indicates the operation successfully

475
00:18:02,880 --> 00:18:08,160
happened, then it just exits this tight loop.

476
00:18:06,480 --> 00:18:10,559
So, the alternative, which is more

477
00:18:08,160 --> 00:18:13,280
complicated, is the mutex, which has a

478
00:18:10,559 --> 00:18:16,720
well-known documented API

479
00:18:13,280 --> 00:18:18,880
called KeWaitForSingleObject. And so,

480
00:18:16,720 --> 00:18:21,520
if you call that API, you're

481
00:18:18,880 --> 00:18:23,440
effectively waiting on that mutex.

482
00:18:21,520 --> 00:18:25,600
And so, if you're not allowed to obtain

483
00:18:23,440 --> 00:18:27,520
that mutex, because another thread

484
00:18:25,600 --> 00:18:29,919
obtained that mutex already, your thread

485
00:18:27,520 --> 00:18:32,320
will actually go into a blocking state,

486
00:18:29,919 --> 00:18:35,679
and it might become alertable.

487
00:18:32,320 --> 00:18:39,039
And it will be paused. And other threads

488
00:18:35,679 --> 00:18:41,520
can start to be scheduled by the kernel.

489
00:18:39,039 --> 00:18:43,440
So, in the critical section case, I

490
00:18:41,520 --> 00:18:45,440
guess one thing that is worth

491
00:18:43,440 --> 00:18:48,559
remembering is that, it will adjust the

492
00:18:45,440 --> 00:18:50,720
IRQL, which basically prevents certain

493
00:18:48,559 --> 00:18:53,360
interrupts to happen when your thread is

494
00:18:50,720 --> 00:18:55,440
actually being executed into that critical

495
00:18:53,360 --> 00:18:57,120
section. And so for instance your thread

496
00:18:55,440 --> 00:19:00,400
can't be preempted

497
00:18:57,120 --> 00:19:00,400
by another thread.

