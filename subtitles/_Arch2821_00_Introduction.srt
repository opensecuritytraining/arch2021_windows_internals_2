1
00:00:04,580 --> 00:00:10,340
Hi everyone. In this course, we are going

2
00:00:08,160 --> 00:00:13,320
to look at the Windows operating system

3
00:00:10,340 --> 00:00:17,039
and all its internals, to understand

4
00:00:13,320 --> 00:00:19,980
better how it works. The goal of this

5
00:00:17,039 --> 00:00:22,619
course is to understand really well all

6
00:00:19,980 --> 00:00:25,920
the core concepts of the Windows kernel.

7
00:00:22,619 --> 00:00:27,900
We will see how to abuse the kernel, for

8
00:00:25,920 --> 00:00:30,000
instance, to exploit it.

9
00:00:27,900 --> 00:00:32,399
Everything we learn for the Windows

10
00:00:30,000 --> 00:00:35,100
operating system will actually be

11
00:00:32,399 --> 00:00:36,960
applicable for other operating systems

12
00:00:35,100 --> 00:00:38,059
as well. The first thing we are going to

13
00:00:36,960 --> 00:00:42,360
look at

14
00:00:38,059 --> 00:00:46,200
is various base concepts like processes,

15
00:00:42,360 --> 00:00:49,440
threads, sessions, objects, and how we call

16
00:00:46,200 --> 00:00:52,260
Windows APIs. Then, we are going to look

17
00:00:49,440 --> 00:00:54,600
at how we can actually go from

18
00:00:52,260 --> 00:00:57,000
userland to kernel land, which is by

19
00:00:54,600 --> 00:01:01,680
calling syscalls. Then, we are going to

20
00:00:57,000 --> 00:01:04,140
look at how the processes and threads

21
00:01:01,680 --> 00:01:06,780
structures are implemented into the

22
00:01:04,140 --> 00:01:10,080
kernel. Then, we are going to look at what

23
00:01:06,780 --> 00:01:12,780
we call kernel pools, which is basically

24
00:01:10,080 --> 00:01:15,360
the heap implementation into the Windows

25
00:01:12,780 --> 00:01:17,640
kernel. We are going to see what are the

26
00:01:15,360 --> 00:01:19,799
different kernel pools implemented into the

27
00:01:17,640 --> 00:01:22,860
kernel. Then we are going to look at

28
00:01:19,799 --> 00:01:24,360
different kernel bugs that you can find

29
00:01:22,860 --> 00:01:26,880
such as memory corruption

30
00:01:24,360 --> 00:01:30,720
vulnerabilities, like use-after-frees,

31
00:01:26,880 --> 00:01:33,060
type confusion bugs, overflows, or race

32
00:01:30,720 --> 00:01:35,700
conditions. Then, we are going to look at

33
00:01:33,060 --> 00:01:38,159
different mitigations that have been

34
00:01:35,700 --> 00:01:41,040
added into the Windows kernel over time,

35
00:01:38,159 --> 00:01:44,759
such as avoiding that the NULL page is

36
00:01:41,040 --> 00:01:48,240
mapped, implementing mitigations like NX or

37
00:01:44,759 --> 00:01:51,600
ASLR to reduce the likelihood of 

38
00:01:48,240 --> 00:01:54,600
vulnerabilities being exploited or others like SMEP

39
00:01:51,600 --> 00:01:56,640
and we're going to talk about some

40
00:01:54,600 --> 00:01:59,119
mitigations that are actually not

41
00:01:56,640 --> 00:02:02,700
implemented into the Windows kernel yet

42
00:01:59,119 --> 00:02:06,060
such as SMAP. Then, we are going to look

43
00:02:02,700 --> 00:02:08,520
at how access checks are implemented for

44
00:02:06,060 --> 00:02:11,220
processes so these processes can only

45
00:02:08,520 --> 00:02:14,580
access what they are supposed to. Then,

46
00:02:11,220 --> 00:02:17,760
we will look at how kernel exploit

47
00:02:14,580 --> 00:02:21,480
payloads are able to execute code in

48
00:02:17,760 --> 00:02:24,300
userland either by migrating from kernel

49
00:02:21,480 --> 00:02:27,000
to userland or by doing what we call

50
00:02:24,300 --> 00:02:29,580
data-only attacks, which is basically a

51
00:02:27,000 --> 00:02:32,280
way to patch things in memory in order

52
00:02:29,580 --> 00:02:35,340
to elevate a given process so it

53
00:02:32,280 --> 00:02:38,599
executes with the System privileges. Okay,

54
00:02:35,340 --> 00:02:38,599
let's get started!

